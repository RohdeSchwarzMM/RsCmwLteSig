PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PIRPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PIRPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Pcc.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: