Pdcp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SRPRofiles
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:MRCSessions
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SNEXtension
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SRCContinue

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SRPRofiles
	SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:MRCSessions
	SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SNEXtension
	SENSe:LTE:SIGNaling<instance>:UECapability:PDCP:SRCContinue



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Pdcp.PdcpCls
	:members:
	:undoc-members:
	:noindex: