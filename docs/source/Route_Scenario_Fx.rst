Fx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FX[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FX[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fx.FxCls
	:members:
	:undoc-members:
	:noindex: