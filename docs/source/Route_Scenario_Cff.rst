Cff
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cff.CffCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.cff.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Cff_Flexible.rst