Pdcch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.pdcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_Pdcch_Alevel.rst
	Sense_Connection_Scc_Pdcch_Psymbols.rst