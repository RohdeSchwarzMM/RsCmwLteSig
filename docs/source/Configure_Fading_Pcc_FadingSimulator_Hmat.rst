Hmat
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:HMAT:MODE
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:HMAT

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:HMAT:MODE
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:HMAT



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Hmat.HmatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.fadingSimulator.hmat.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_FadingSimulator_Hmat_Row.rst
	Configure_Fading_Pcc_FadingSimulator_Hmat_Rst.rst