Crate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:FBURst:DL<Stream>:PEPSubframes:CRATe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:FBURst:DL<Stream>:PEPSubframes:CRATe



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.Laa.Fburst.Downlink.PepSubFrames.Crate.CrateCls
	:members:
	:undoc-members:
	:noindex: