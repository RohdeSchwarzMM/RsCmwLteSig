Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:RMC:RBPosition:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:RMC:RBPosition:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Rmc.RbPosition.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: