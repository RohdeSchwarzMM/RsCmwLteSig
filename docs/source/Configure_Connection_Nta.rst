Nta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:NTA:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:NTA:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Nta.NtaCls
	:members:
	:undoc-members:
	:noindex: