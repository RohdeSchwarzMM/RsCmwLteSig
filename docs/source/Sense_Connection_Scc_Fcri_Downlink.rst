Downlink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fcri.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.fcri.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_Fcri_Downlink_Mcs.rst
	Sense_Connection_Scc_Fcri_Downlink_McsTable.rst