Catalog
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:CATalog:DESTination

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:CATalog:DESTination



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.Catalog.CatalogCls
	:members:
	:undoc-members:
	:noindex: