Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:CSIRs:MODE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:CSIRs:MODE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Csirs.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: