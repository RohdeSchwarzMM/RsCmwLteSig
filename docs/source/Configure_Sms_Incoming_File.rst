File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SMS:INComing:FILE:INFO
	single: CONFigure:LTE:SIGNaling<instance>:SMS:INComing:FILE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SMS:INComing:FILE:INFO
	CONFigure:LTE:SIGNaling<instance>:SMS:INComing:FILE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Incoming.File.FileCls
	:members:
	:undoc-members:
	:noindex: