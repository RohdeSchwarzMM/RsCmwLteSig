Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: