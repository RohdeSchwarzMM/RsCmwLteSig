Enhanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:REPorted:ENHanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:REPorted:ENHanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.UeCategory.Reported.Enhanced.EnhancedCls
	:members:
	:undoc-members:
	:noindex: