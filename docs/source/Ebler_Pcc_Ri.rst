Ri
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:RI

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:RI



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Ri.RiCls
	:members:
	:undoc-members:
	:noindex: