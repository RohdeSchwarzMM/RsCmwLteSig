Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PHICh:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PHICh:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Phich.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: