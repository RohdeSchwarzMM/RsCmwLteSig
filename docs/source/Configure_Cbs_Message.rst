Message
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ID
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:IDTYpe
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:CGRoup
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:CATegory
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:SOURce
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DATA
	single: CONFigure:LTE:SIGNaling<Instance>:CBS:MESSage:UCODed
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:WAENable
	single: CONFigure:LTE:SIGNaling<Instance>:CBS:MESSage:WACoordinate
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:PERiod

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ENABle
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ID
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:IDTYpe
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:CGRoup
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:CATegory
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:SOURce
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DATA
	CONFigure:LTE:SIGNaling<Instance>:CBS:MESSage:UCODed
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:WAENable
	CONFigure:LTE:SIGNaling<Instance>:CBS:MESSage:WACoordinate
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:PERiod



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.MessageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cbs.message.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cbs_Message_DcScheme.rst
	Configure_Cbs_Message_Etws.rst
	Configure_Cbs_Message_File.rst
	Configure_Cbs_Message_Language.rst
	Configure_Cbs_Message_Serial.rst