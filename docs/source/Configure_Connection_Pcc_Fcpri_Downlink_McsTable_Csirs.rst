Csirs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:CSIRs:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:CSIRs:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcpri.Downlink.McsTable.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: