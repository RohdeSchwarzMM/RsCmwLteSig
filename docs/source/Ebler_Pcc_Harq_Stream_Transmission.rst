Transmission
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Harq.Stream.Transmission.TransmissionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.pcc.harq.stream.transmission.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Pcc_Harq_Stream_Transmission_Absolute.rst
	Ebler_Pcc_Harq_Stream_Transmission_Relative.rst