Utc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SIB<n>:TNFO<tnfo>:UTC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SIB<n>:TNFO<tnfo>:UTC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sib.Tnfo.Utc.UtcCls
	:members:
	:undoc-members:
	:noindex: