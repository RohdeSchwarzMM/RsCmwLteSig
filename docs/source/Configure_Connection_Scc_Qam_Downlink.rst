Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:QAM<ModOrder>:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:QAM<ModOrder>:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Qam.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: