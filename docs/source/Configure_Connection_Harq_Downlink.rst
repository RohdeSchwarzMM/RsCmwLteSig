Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:NHT
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:RVCSequence

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:ENABle
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:NHT
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:RVCSequence



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Harq.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.harq.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Harq_Downlink_UdSequence.rst