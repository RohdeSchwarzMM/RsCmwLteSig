Sense
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:RRCState

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:RRCState



.. autoclass:: RsCmwLteSig.Implementations.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection.rst
	Sense_CqiReporting.rst
	Sense_Downlink.rst
	Sense_EeLog.rst
	Sense_Elog.rst
	Sense_Fading.rst
	Sense_IqOut.rst
	Sense_Sib.rst
	Sense_Sms.rst
	Sense_UeCapability.rst
	Sense_UeReport.rst
	Sense_UesInfo.rst
	Sense_Uplink.rst