UdPattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:UDPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:UDPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pusch.Tpc.UdPattern.UdPatternCls
	:members:
	:undoc-members:
	:noindex: