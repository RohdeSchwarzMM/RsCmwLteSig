Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PNPusch:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PNPusch:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.Pnpusch.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: