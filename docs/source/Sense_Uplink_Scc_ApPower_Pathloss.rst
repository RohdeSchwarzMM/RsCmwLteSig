Pathloss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PATHloss

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PATHloss



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.Pathloss.PathlossCls
	:members:
	:undoc-members:
	:noindex: