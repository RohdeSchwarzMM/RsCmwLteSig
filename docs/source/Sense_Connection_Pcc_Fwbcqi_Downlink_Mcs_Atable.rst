Atable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCS:ATABle:LIST

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCS:ATABle:LIST



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fwbcqi.Downlink.Mcs.Atable.AtableCls
	:members:
	:undoc-members:
	:noindex: