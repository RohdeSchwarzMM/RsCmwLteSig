UlDl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:ULDL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:ULDL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.UlDl.UlDlCls
	:members:
	:undoc-members:
	:noindex: