FcttiBased
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.FcttiBased.FcttiBasedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.fcttiBased.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_FcttiBased_Downlink.rst