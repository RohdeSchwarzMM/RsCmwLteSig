ApPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PATHloss
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:EPPPower
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:EOPower

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PATHloss
	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:EPPPower
	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:EOPower



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.ApPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.setc.apPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Setc_ApPower_PcAlpha.rst
	Sense_Uplink_Setc_ApPower_PirPower.rst
	Sense_Uplink_Setc_ApPower_Pnpusch.rst
	Sense_Uplink_Setc_ApPower_RsPower.rst
	Sense_Uplink_Setc_ApPower_TprrcSetup.rst