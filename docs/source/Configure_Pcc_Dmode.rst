Dmode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:DMODe:UCSPecific
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:DMODe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:DMODe:UCSPecific
	CONFigure:LTE:SIGNaling<instance>[:PCC]:DMODe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Dmode.DmodeCls
	:members:
	:undoc-members:
	:noindex: