CoThreshold
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:COTHreshold

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:COTHreshold



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Rssi.CoThreshold.CoThresholdCls
	:members:
	:undoc-members:
	:noindex: