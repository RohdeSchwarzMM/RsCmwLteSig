TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:TPRRcsetup:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:TPRRcsetup:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: