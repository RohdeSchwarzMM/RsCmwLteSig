Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:B:SCC<Carrier>:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:B:SCC<Carrier>:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.B.Scc.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: