Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer[:PCC]:STReam<Stream>:RELative

.. code-block:: python

	FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer[:PCC]:STReam<Stream>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Pcc.Stream.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: