Psymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PDCCh:PSYMbols

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PDCCh:PSYMbols



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Pdcch.Psymbols.PsymbolsCls
	:members:
	:undoc-members:
	:noindex: