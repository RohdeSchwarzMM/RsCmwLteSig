Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer[:PCC]:RELative

.. code-block:: python

	FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer[:PCC]:RELative



.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Pcc.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: