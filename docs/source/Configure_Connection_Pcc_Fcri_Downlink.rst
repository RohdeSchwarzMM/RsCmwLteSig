Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:STTI
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:STTI
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcri.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.fcri.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Fcri_Downlink_McsTable.rst