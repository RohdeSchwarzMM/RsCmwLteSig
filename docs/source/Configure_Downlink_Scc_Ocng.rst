Ocng
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:OCNG

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:OCNG



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Ocng.OcngCls
	:members:
	:undoc-members:
	:noindex: