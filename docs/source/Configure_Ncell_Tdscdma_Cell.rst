Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Tdscdma.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: