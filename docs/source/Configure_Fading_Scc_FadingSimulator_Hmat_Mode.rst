Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT:MODE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT:MODE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Hmat.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: