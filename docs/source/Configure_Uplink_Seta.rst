Seta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:PMAX

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:PMAX



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.SetaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.seta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Seta_ApPower.rst
	Configure_Uplink_Seta_Pucch.rst
	Configure_Uplink_Seta_Pusch.rst