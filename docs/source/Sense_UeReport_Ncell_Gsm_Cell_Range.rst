Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:GSM:CELL<nr>:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:GSM:CELL<nr>:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Gsm.Cell.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: