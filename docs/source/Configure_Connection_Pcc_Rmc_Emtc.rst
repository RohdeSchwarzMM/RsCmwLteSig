Emtc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:SFPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:SFPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Rmc.Emtc.EmtcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.rmc.emtc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Rmc_Emtc_NbPosition.rst