Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PNPusch:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PNPusch:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Pcc.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: