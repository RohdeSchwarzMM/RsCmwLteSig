TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:TPRRcsetup:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:TPRRcsetup:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: