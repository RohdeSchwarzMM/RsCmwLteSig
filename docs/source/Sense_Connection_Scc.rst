Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.sense.connection.scc.repcap_secondaryCompCarrier_get()
	driver.sense.connection.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_Fcpri.rst
	Sense_Connection_Scc_Fcri.rst
	Sense_Connection_Scc_Fwbcqi.rst
	Sense_Connection_Scc_Hpusch.rst
	Sense_Connection_Scc_Pdcch.rst
	Sense_Connection_Scc_Tscheme.rst
	Sense_Connection_Scc_UdChannels.rst
	Sense_Connection_Scc_UdttiBased.rst