Awgn
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.scc.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Scc_Awgn_Bandwidth.rst
	Configure_Fading_Scc_Awgn_Enable.rst
	Configure_Fading_Scc_Awgn_Foffset.rst
	Configure_Fading_Scc_Awgn_Measurement.rst
	Configure_Fading_Scc_Awgn_SnRatio.rst