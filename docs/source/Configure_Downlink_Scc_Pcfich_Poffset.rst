Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PCFich:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PCFich:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pcfich.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: