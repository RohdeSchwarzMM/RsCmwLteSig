Gya
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GYA[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GYA[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gya.GyaCls
	:members:
	:undoc-members:
	:noindex: