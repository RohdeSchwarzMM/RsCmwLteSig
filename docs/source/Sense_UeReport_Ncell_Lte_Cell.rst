Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:LTE:CELL<nr>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:LTE:CELL<nr>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Lte.Cell.CellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.lte.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Lte_Cell_Range.rst