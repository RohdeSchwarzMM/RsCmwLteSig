Gr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GR[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GR[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gr.GrCls
	:members:
	:undoc-members:
	:noindex: