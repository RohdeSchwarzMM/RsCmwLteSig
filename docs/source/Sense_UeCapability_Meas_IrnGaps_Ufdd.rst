Ufdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:UFDD

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:UFDD



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.Ufdd.UfddCls
	:members:
	:undoc-members:
	:noindex: