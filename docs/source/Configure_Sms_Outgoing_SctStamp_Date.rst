Date
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:DATE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Outgoing.SctStamp.Date.DateCls
	:members:
	:undoc-members:
	:noindex: