Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:BCLass:DL

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:BCLass:DL



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.Bclass.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: