Standard
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:STANdard:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:STANdard:PROFile

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:STANdard:ENABle
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:STANdard:PROFile



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Standard.StandardCls
	:members:
	:undoc-members:
	:noindex: