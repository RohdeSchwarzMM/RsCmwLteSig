Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.intermediate.ebler.scc.repcap_secondaryCompCarrier_get()
	driver.intermediate.ebler.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ebler.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ebler_Scc_Absolute.rst
	Intermediate_Ebler_Scc_Relative.rst
	Intermediate_Ebler_Scc_Stream.rst