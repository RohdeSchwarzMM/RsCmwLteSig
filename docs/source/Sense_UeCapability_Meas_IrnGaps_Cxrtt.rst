Cxrtt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:CXRTt

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:CXRTt



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.Cxrtt.CxrttCls
	:members:
	:undoc-members:
	:noindex: