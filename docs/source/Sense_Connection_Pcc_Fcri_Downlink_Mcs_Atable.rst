Atable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCS:ATABle:LIST

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCS:ATABle:LIST



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcri.Downlink.Mcs.Atable.AtableCls
	:members:
	:undoc-members:
	:noindex: