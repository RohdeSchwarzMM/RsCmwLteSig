Advanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PIRPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PIRPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.PirPower.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex: