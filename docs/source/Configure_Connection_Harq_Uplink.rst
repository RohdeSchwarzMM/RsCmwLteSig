Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:MAXTx
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:NHT
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:DPHich

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:MAXTx
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:ENABle
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:NHT
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:UL:DPHich



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Harq.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: