Mnc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:MNC:DIGits
	single: CONFigure:LTE:SIGNaling<instance>:CELL:MNC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:MNC:DIGits
	CONFigure:LTE:SIGNaling<instance>:CELL:MNC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Mnc.MncCls
	:members:
	:undoc-members:
	:noindex: