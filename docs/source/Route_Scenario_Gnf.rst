Gnf
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gnf.GnfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.gnf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Gnf_Flexible.rst