State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:B:STATe

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:B:STATe



.. autoclass:: RsCmwLteSig.Implementations.B.State.StateCls
	:members:
	:undoc-members:
	:noindex: