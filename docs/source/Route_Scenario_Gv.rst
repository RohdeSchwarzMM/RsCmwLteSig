Gv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GV[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GV[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gv.GvCls
	:members:
	:undoc-members:
	:noindex: