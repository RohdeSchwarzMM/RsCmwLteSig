T
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:T<nr>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:T<nr>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Timeout.T.TCls
	:members:
	:undoc-members:
	:noindex: