Atable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCS:ATABle:LIST

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCS:ATABle:LIST



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcpri.Downlink.Mcs.Atable.AtableCls
	:members:
	:undoc-members:
	:noindex: