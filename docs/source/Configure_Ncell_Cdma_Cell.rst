Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Cdma.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: