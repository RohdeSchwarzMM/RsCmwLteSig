Tnfo
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Sib.Tnfo.TnfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sib.tnfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sib_Tnfo_Leap.rst
	Configure_Sib_Tnfo_Utc.rst