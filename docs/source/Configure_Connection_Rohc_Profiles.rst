Profiles
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:PROFiles

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:PROFiles



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Rohc.Profiles.ProfilesCls
	:members:
	:undoc-members:
	:noindex: