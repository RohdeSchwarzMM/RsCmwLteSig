Gg
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GG[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GG[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gg.GgCls
	:members:
	:undoc-members:
	:noindex: