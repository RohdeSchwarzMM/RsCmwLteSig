Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:HOFFset
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:HOFFset
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.hopping.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Hopping_Downlink_A.rst
	Configure_Pcc_Emtc_Hopping_Downlink_B.rst