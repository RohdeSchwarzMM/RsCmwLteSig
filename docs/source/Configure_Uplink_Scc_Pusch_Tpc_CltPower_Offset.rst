Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:CLTPower:OFFSet

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:CLTPower:OFFSet



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pusch.Tpc.CltPower.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: