Fading
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.FadingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc.rst
	Configure_Fading_Scc.rst