Awgn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:AWGN

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:AWGN



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex: