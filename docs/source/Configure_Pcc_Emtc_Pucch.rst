Pucch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.pucch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Pucch_A.rst
	Configure_Pcc_Emtc_Pucch_B.rst