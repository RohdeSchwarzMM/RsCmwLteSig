PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:PIRPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:PIRPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: