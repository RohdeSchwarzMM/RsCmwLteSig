DedBearer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:CONNection:DEDBearer:SEParate
	single: PREPare:LTE:SIGNaling<instance>:CONNection:DEDBearer

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:CONNection:DEDBearer:SEParate
	PREPare:LTE:SIGNaling<instance>:CONNection:DEDBearer



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Connection.DedBearer.DedBearerCls
	:members:
	:undoc-members:
	:noindex: