UeCapability
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:ASRelease
	single: SENSe:LTE:SIGNaling<instance>:UECapability:DCIulca
	single: SENSe:LTE:SIGNaling<instance>:UECapability:URTTimediff
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IDCindex
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PPINdex
	single: SENSe:LTE:SIGNaling<instance>:UECapability:DTYPe
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RREPort
	single: SENSe:LTE:SIGNaling<instance>:UECapability:ERLField
	single: SENSe:LTE:SIGNaling<instance>:UECapability:LMMeas

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:ASRelease
	SENSe:LTE:SIGNaling<instance>:UECapability:DCIulca
	SENSe:LTE:SIGNaling<instance>:UECapability:URTTimediff
	SENSe:LTE:SIGNaling<instance>:UECapability:IDCindex
	SENSe:LTE:SIGNaling<instance>:UECapability:PPINdex
	SENSe:LTE:SIGNaling<instance>:UECapability:DTYPe
	SENSe:LTE:SIGNaling<instance>:UECapability:RREPort
	SENSe:LTE:SIGNaling<instance>:UECapability:ERLField
	SENSe:LTE:SIGNaling<instance>:UECapability:LMMeas



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_CeParameters.rst
	Sense_UeCapability_CpIndication.rst
	Sense_UeCapability_DcParameters.rst
	Sense_UeCapability_FaueEutra.rst
	Sense_UeCapability_FgIndicators.rst
	Sense_UeCapability_InterRat.rst
	Sense_UeCapability_Laa.rst
	Sense_UeCapability_Mac.rst
	Sense_UeCapability_Mbms.rst
	Sense_UeCapability_Meas.rst
	Sense_UeCapability_Ncsacq.rst
	Sense_UeCapability_Pdcp.rst
	Sense_UeCapability_Player.rst
	Sense_UeCapability_Rf.rst
	Sense_UeCapability_Sl.rst
	Sense_UeCapability_TaueEutra.rst
	Sense_UeCapability_UbnpMeas.rst
	Sense_UeCapability_UeCategory.rst
	Sense_UeCapability_Wiw.rst