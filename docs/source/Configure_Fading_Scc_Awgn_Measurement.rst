Measurement
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:MEASurement

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:MEASurement



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.Measurement.MeasurementCls
	:members:
	:undoc-members:
	:noindex: