Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: