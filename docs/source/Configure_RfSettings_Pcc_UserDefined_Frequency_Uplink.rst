Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:UL:MINimum
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:UL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:UL:MINimum
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:UL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.Frequency.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: