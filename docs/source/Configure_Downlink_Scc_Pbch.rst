Pbch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pbch.PbchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.pbch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Pbch_Poffset.rst