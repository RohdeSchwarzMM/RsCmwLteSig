Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:NRPReambles
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:NIPRach
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PRSTep
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PFOFfset
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:LRSindex
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:ZCZConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:NRPReambles
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:NIPRach
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PRSTep
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PFOFfset
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:LRSindex
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:ZCZConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Prach_PcIndex.rst