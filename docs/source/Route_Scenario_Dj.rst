Dj
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DJ[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:DJ[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Dj.DjCls
	:members:
	:undoc-members:
	:noindex: