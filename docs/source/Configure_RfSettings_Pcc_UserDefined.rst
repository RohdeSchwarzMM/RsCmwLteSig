UserDefined
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:UDSeparation
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:BINDicator

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:UDSeparation
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:BINDicator



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_UserDefined_Channel.rst
	Configure_RfSettings_Pcc_UserDefined_Frequency.rst