Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DL[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:DL[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: