UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:SSUBframe:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:SSUBframe:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Downlink.McsTable.Ssubframe.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: