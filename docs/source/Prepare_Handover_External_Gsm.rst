Gsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:GSM

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:GSM



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Gsm.GsmCls
	:members:
	:undoc-members:
	:noindex: