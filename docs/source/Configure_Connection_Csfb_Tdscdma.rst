Tdscdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:TDSCdma

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:TDSCdma



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Csfb.Tdscdma.TdscdmaCls
	:members:
	:undoc-members:
	:noindex: