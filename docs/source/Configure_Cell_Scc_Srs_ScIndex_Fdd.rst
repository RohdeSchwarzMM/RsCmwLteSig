Fdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SCINdex:FDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SCINdex:FDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.ScIndex.Fdd.FddCls
	:members:
	:undoc-members:
	:noindex: