Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:CSIRs:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:CSIRs:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fwbcqi.Downlink.McsTable.Csirs.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: