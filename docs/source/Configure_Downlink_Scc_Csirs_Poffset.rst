Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:CSIRs:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:CSIRs:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Csirs.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: