Tia<TbsIndexAlt>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr2 .. Nr3
	rc = driver.configure.connection.pcc.tia.repcap_tbsIndexAlt_get()
	driver.configure.connection.pcc.tia.repcap_tbsIndexAlt_set(repcap.TbsIndexAlt.Nr2)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TIA<Nr>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TIA<Nr>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tia.TiaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.tia.clone()