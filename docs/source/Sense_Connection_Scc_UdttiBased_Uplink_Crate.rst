Crate
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdttiBased.Uplink.Crate.CrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.udttiBased.uplink.crate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_UdttiBased_Uplink_Crate_All.rst