Stti
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:STTI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:STTI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcri.Downlink.Stti.SttiCls
	:members:
	:undoc-members:
	:noindex: