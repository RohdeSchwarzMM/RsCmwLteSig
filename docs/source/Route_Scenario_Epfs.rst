Epfs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Epfs.EpfsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.epfs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Epfs_Flexible.rst