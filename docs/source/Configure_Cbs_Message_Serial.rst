Serial
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:SERial

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:SERial



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.Serial.SerialCls
	:members:
	:undoc-members:
	:noindex: