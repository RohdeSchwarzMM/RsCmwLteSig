Soffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:RMTC:SOFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:RMTC:SOFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Rssi.Rmtc.Soffset.SoffsetCls
	:members:
	:undoc-members:
	:noindex: