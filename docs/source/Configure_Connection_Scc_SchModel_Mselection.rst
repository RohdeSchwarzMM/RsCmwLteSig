Mselection
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SchModel.Mselection.MselectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.schModel.mselection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_SchModel_Mselection_Mimo.rst