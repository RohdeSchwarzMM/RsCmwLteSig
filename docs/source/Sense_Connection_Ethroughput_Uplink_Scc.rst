Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.sense.connection.ethroughput.uplink.scc.repcap_secondaryCompCarrier_get()
	driver.sense.connection.ethroughput.uplink.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL:SCC<Carrier>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL:SCC<Carrier>



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.Uplink.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.uplink.scc.clone()