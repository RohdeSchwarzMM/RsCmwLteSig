Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.ebler.scc.stream.repcap_stream_get()
	driver.ebler.scc.stream.repcap_stream_set(repcap.Stream.S1)





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.scc.stream.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Scc_Stream_Absolute.rst
	Ebler_Scc_Stream_Relative.rst