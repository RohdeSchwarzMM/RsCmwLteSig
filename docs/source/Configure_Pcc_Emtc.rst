Emtc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:ENABle
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MB<number>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:ENABle
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MB<number>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.EmtcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Ce.rst
	Configure_Pcc_Emtc_Hopping.rst
	Configure_Pcc_Emtc_Mpdcch.rst
	Configure_Pcc_Emtc_Pdsch.rst
	Configure_Pcc_Emtc_Pucch.rst
	Configure_Pcc_Emtc_Pusch.rst