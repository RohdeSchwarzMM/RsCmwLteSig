Ri<ReliabilityIndicatorNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: RIno1 .. RIno4
	rc = driver.ebler.scc.pmi.ri.repcap_reliabilityIndicatorNo_get()
	driver.ebler.scc.pmi.ri.repcap_reliabilityIndicatorNo_set(repcap.ReliabilityIndicatorNo.RIno1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:PMI:RI<no>

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:PMI:RI<no>



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Pmi.Ri.RiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.scc.pmi.ri.clone()