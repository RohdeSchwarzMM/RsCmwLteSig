DcSupport
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:DCSupport:ASYNchronous
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:DCSupport:SCGRouping

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:DCSupport:ASYNchronous
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:DCSupport:SCGRouping



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.DcSupport.DcSupportCls
	:members:
	:undoc-members:
	:noindex: