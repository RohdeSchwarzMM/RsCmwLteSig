Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:STReam<Stream>:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:STReam<Stream>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Stream.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: