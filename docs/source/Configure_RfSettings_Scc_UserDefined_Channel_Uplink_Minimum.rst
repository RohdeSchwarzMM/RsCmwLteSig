Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:UL:MINimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:UL:MINimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Channel.Uplink.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: