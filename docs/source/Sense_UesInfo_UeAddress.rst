UeAddress
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UesInfo.UeAddress.UeAddressCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.ueAddress.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_UeAddress_DedBearer.rst
	Sense_UesInfo_UeAddress_Ipv.rst