UdSeparation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:UDSeparation

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:UDSeparation



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.UdSeparation.UdSeparationCls
	:members:
	:undoc-members:
	:noindex: