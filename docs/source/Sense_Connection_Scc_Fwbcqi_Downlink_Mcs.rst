Mcs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fwbcqi.Downlink.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.fwbcqi.downlink.mcs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_Fwbcqi_Downlink_Mcs_Atable.rst