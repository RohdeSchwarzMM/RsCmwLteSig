RpAttempt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:RPATtempt

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:RPATtempt



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.Level.Prach.RpAttempt.RpAttemptCls
	:members:
	:undoc-members:
	:noindex: