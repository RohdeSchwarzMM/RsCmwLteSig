Pdsch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pdsch.PdschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.pdsch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Pdsch_A.rst
	Configure_Pcc_Emtc_Pdsch_B.rst