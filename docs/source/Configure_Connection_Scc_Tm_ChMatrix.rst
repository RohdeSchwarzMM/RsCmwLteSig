ChMatrix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<8>:CHMatrix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<8>:CHMatrix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.ChMatrix.ChMatrixCls
	:members:
	:undoc-members:
	:noindex: