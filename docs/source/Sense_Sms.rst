Sms
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Sms.SmsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sms.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sms_Incoming.rst
	Sense_Sms_Info.rst
	Sense_Sms_Outgoing.rst