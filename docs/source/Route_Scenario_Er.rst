Er
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:ER[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:ER[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Er.ErCls
	:members:
	:undoc-members:
	:noindex: