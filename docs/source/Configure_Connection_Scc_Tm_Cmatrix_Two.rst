Two<MatrixTwoLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.connection.scc.tm.cmatrix.two.repcap_matrixTwoLine_get()
	driver.configure.connection.scc.tm.cmatrix.two.repcap_matrixTwoLine_set(repcap.MatrixTwoLine.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:TWO<line>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:TWO<line>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Cmatrix.Two.TwoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.tm.cmatrix.two.clone()