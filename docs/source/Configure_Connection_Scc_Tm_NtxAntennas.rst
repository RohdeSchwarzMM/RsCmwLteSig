NtxAntennas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:NTXantennas

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:NTXantennas



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.NtxAntennas.NtxAntennasCls
	:members:
	:undoc-members:
	:noindex: