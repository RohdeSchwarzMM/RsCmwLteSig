Hycs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:HYCS<MIMO4x4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:HYCS<MIMO4x4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Hycs.HycsCls
	:members:
	:undoc-members:
	:noindex: