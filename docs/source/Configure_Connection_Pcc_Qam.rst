Qam<QAMmodulationOrderB>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: QAM256 .. QAM1024
	rc = driver.configure.connection.pcc.qam.repcap_qAMmodulationOrderB_get()
	driver.configure.connection.pcc.qam.repcap_qAMmodulationOrderB_set(repcap.QAMmodulationOrderB.QAM256)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Qam.QamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Qam_Downlink.rst