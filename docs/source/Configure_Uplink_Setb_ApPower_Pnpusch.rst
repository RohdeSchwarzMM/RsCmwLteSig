Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:PNPusch:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:PNPusch:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: