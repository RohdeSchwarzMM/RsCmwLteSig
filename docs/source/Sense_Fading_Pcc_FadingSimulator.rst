FadingSimulator
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Fading.Pcc.FadingSimulator.FadingSimulatorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.fading.pcc.fadingSimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Fading_Pcc_FadingSimulator_Iloss.rst