Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:TRANsmission:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:TRANsmission:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Transmission.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: