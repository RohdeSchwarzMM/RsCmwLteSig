CafrfOut
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CAFRfout:FLEXible

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CAFRfout:FLEXible



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.CafrfOut.CafrfOutCls
	:members:
	:undoc-members:
	:noindex: