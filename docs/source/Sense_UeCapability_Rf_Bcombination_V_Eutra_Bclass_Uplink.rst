Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:BCLass:UL

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:BCLass:UL



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.Bclass.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: