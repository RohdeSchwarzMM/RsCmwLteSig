Crate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:RBURst:DL<Stream>:FSUBframes:CRATe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:RBURst:DL<Stream>:FSUBframes:CRATe



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.Laa.Rburst.Downlink.FullSubFrames.Crate.CrateCls
	:members:
	:undoc-members:
	:noindex: