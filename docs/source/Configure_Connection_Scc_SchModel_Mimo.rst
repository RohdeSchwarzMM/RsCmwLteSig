Mimo<Mimo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: M42 .. M44
	rc = driver.configure.connection.scc.schModel.mimo.repcap_mimo_get()
	driver.configure.connection.scc.schModel.mimo.repcap_mimo_set(repcap.Mimo.M42)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:MIMO<Mimo>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:MIMO<Mimo>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SchModel.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.schModel.mimo.clone()