EnpMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:ENPMode

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:ENPMode



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.EnpMode.EnpModeCls
	:members:
	:undoc-members:
	:noindex: