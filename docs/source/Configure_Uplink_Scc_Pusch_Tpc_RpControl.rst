RpControl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:RPControl

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:RPControl



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pusch.Tpc.RpControl.RpControlCls
	:members:
	:undoc-members:
	:noindex: