Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.sense.connection.ethroughput.downlink.scc.repcap_secondaryCompCarrier_get()
	driver.sense.connection.ethroughput.downlink.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:SCC<Carrier>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:SCC<Carrier>



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.Downlink.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.downlink.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Ethroughput_Downlink_Scc_Stream.rst