TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:TPRRcsetup:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:TPRRcsetup:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Pcc.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: