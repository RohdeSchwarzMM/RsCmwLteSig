Csirs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.csirs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Csirs_Mode.rst
	Configure_Downlink_Scc_Csirs_Poffset.rst