Foffset
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.foffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_Foffset_Downlink.rst
	Configure_RfSettings_Pcc_Foffset_Uplink.rst