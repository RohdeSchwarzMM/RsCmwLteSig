Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:EATTenuation:INPut

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:EATTenuation:INPut



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.eattenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_Eattenuation_Output.rst