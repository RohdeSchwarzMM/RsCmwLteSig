Pcid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:PCID

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:PCID



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Pcid.PcidCls
	:members:
	:undoc-members:
	:noindex: