A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:A:CERepetition
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:A:MRCE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:A:CERepetition
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:A:MRCE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pdsch.A.ACls
	:members:
	:undoc-members:
	:noindex: