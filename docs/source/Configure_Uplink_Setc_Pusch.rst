Pusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:OLNPower

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:OLNPower



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.setc.pusch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Setc_Pusch_Tpc.rst