Throughput
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: STOP:LTE:SIGNaling<instance>:THRoughput
	single: ABORt:LTE:SIGNaling<instance>:THRoughput
	single: INITiate:LTE:SIGNaling<instance>:THRoughput
	single: FETCh:LTE:SIGNaling<instance>:THRoughput
	single: READ:LTE:SIGNaling<instance>:THRoughput

.. code-block:: python

	STOP:LTE:SIGNaling<instance>:THRoughput
	ABORt:LTE:SIGNaling<instance>:THRoughput
	INITiate:LTE:SIGNaling<instance>:THRoughput
	FETCh:LTE:SIGNaling<instance>:THRoughput
	READ:LTE:SIGNaling<instance>:THRoughput



.. autoclass:: RsCmwLteSig.Implementations.Throughput.ThroughputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State.rst
	Throughput_Trace.rst