Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:MCLuster:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:MCLuster:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Mcluster.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: