FcPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:DL:SCC<Carrier>:FCPower

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:DL:SCC<Carrier>:FCPower



.. autoclass:: RsCmwLteSig.Implementations.Sense.Downlink.Scc.FcPower.FcPowerCls
	:members:
	:undoc-members:
	:noindex: