Hx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:HX[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:HX[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Hx.HxCls
	:members:
	:undoc-members:
	:noindex: