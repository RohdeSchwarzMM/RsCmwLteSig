Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CFF[:FLEXible][:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CFF[:FLEXible]:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CFF[:FLEXible][:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:CFF[:FLEXible]:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cff.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: