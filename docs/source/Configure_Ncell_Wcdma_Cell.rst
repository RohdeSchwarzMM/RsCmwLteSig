Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Wcdma.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: