Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:UDCHannels:MCLuster:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:UDCHannels:MCLuster:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Mcluster.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: