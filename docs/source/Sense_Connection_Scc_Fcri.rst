Fcri
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fcri.FcriCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.fcri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_Fcri_Downlink.rst