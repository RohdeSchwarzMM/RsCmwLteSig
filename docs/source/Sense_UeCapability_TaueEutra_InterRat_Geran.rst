Geran
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:GERan:SUPPorted
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:GERan:PHGeran

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:GERan:SUPPorted
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:GERan:PHGeran



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.InterRat.Geran.GeranCls
	:members:
	:undoc-members:
	:noindex: