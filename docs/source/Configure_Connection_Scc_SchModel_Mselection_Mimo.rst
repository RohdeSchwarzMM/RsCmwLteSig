Mimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:MSELection:MIMO<Mimo>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:MSELection:MIMO<Mimo>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SchModel.Mselection.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex: