Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:FREQuency:DL:MINimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:FREQuency:DL:MINimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Frequency.Downlink.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: