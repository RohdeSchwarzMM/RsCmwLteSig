Tdscdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:TDSCdma

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:TDSCdma



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Tdscdma.TdscdmaCls
	:members:
	:undoc-members:
	:noindex: