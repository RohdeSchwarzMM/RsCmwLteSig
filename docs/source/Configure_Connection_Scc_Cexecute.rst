Cexecute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:CEXecute

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:CEXecute



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Cexecute.CexecuteCls
	:members:
	:undoc-members:
	:noindex: