Epf
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Epf.EpfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.epf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Epf_Flexible.rst