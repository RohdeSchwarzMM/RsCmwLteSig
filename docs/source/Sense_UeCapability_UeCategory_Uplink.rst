Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:UECategory:UL:ENHanced

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:UECategory:UL:ENHanced



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.UeCategory.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: