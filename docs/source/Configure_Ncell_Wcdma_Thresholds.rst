Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:WCDMa:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Wcdma.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: