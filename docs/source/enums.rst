Enums
=========

AcceptAttachCause
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcceptAttachCause.C18
	# All values (3x):
	C18 | OFF | ON

AccStratRelease
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AccStratRelease.REL10
	# Last value:
	value = enums.AccStratRelease.REL9
	# All values (9x):
	REL10 | REL11 | REL12 | REL13 | REL14 | REL15 | REL16 | REL8
	REL9

AddSpectrumEmission
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AddSpectrumEmission.NS01
	# Last value:
	value = enums.AddSpectrumEmission.NS99
	# All values (288x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS100 | NS101 | NS102 | NS103 | NS104 | NS105
	NS106 | NS107 | NS108 | NS109 | NS11 | NS110 | NS111 | NS112
	NS113 | NS114 | NS115 | NS116 | NS117 | NS118 | NS119 | NS12
	NS120 | NS121 | NS122 | NS123 | NS124 | NS125 | NS126 | NS127
	NS128 | NS129 | NS13 | NS130 | NS131 | NS132 | NS133 | NS134
	NS135 | NS136 | NS137 | NS138 | NS139 | NS14 | NS140 | NS141
	NS142 | NS143 | NS144 | NS145 | NS146 | NS147 | NS148 | NS149
	NS15 | NS150 | NS151 | NS152 | NS153 | NS154 | NS155 | NS156
	NS157 | NS158 | NS159 | NS16 | NS160 | NS161 | NS162 | NS163
	NS164 | NS165 | NS166 | NS167 | NS168 | NS169 | NS17 | NS170
	NS171 | NS172 | NS173 | NS174 | NS175 | NS176 | NS177 | NS178
	NS179 | NS18 | NS180 | NS181 | NS182 | NS183 | NS184 | NS185
	NS186 | NS187 | NS188 | NS189 | NS19 | NS190 | NS191 | NS192
	NS193 | NS194 | NS195 | NS196 | NS197 | NS198 | NS199 | NS20
	NS200 | NS201 | NS202 | NS203 | NS204 | NS205 | NS206 | NS207
	NS208 | NS209 | NS21 | NS210 | NS211 | NS212 | NS213 | NS214
	NS215 | NS216 | NS217 | NS218 | NS219 | NS22 | NS220 | NS221
	NS222 | NS223 | NS224 | NS225 | NS226 | NS227 | NS228 | NS229
	NS23 | NS230 | NS231 | NS232 | NS233 | NS234 | NS235 | NS236
	NS237 | NS238 | NS239 | NS24 | NS240 | NS241 | NS242 | NS243
	NS244 | NS245 | NS246 | NS247 | NS248 | NS249 | NS25 | NS250
	NS251 | NS252 | NS253 | NS254 | NS255 | NS256 | NS257 | NS258
	NS259 | NS26 | NS260 | NS261 | NS262 | NS263 | NS264 | NS265
	NS266 | NS267 | NS268 | NS269 | NS27 | NS270 | NS271 | NS272
	NS273 | NS274 | NS275 | NS276 | NS277 | NS278 | NS279 | NS28
	NS280 | NS281 | NS282 | NS283 | NS284 | NS285 | NS286 | NS287
	NS288 | NS29 | NS30 | NS31 | NS32 | NS33 | NS34 | NS35
	NS36 | NS37 | NS38 | NS39 | NS40 | NS41 | NS42 | NS43
	NS44 | NS45 | NS46 | NS47 | NS48 | NS49 | NS50 | NS51
	NS52 | NS53 | NS54 | NS55 | NS56 | NS57 | NS58 | NS59
	NS60 | NS61 | NS62 | NS63 | NS64 | NS65 | NS66 | NS67
	NS68 | NS69 | NS70 | NS71 | NS72 | NS73 | NS74 | NS75
	NS76 | NS77 | NS78 | NS79 | NS80 | NS81 | NS82 | NS83
	NS84 | NS85 | NS86 | NS87 | NS88 | NS89 | NS90 | NS91
	NS92 | NS93 | NS94 | NS95 | NS96 | NS97 | NS98 | NS99

Aggregationlevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Aggregationlevel.AUTO
	# All values (6x):
	AUTO | D1U1 | D4U2 | D4U4 | D8U4 | D8U8

AntennaPorts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntennaPorts.NONE
	# All values (5x):
	NONE | P15 | P1516 | P1518 | P1522

AntennasTxA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntennasTxA.FOUR
	# All values (3x):
	FOUR | ONE | TWO

AntennasTxB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AntennasTxB.EIGHt
	# All values (3x):
	EIGHt | FOUR | TWO

AutoManualModeExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualModeExt.AUTO
	# All values (3x):
	AUTO | MANual | SEMiauto

AwgnMeasurement
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AwgnMeasurement.NOISe
	# All values (3x):
	NOISe | OFF | SIGNal

BandClass
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BandClass.AWS
	# Last value:
	value = enums.BandClass.USPC
	# All values (21x):
	AWS | B18M | IEXT | IM2K | JTAC | KCEL | KPCS | LO7C
	N45T | NA7C | NA8S | NA9C | NAPC | PA4M | PA8M | PS7C
	TACS | U25B | U25F | USC | USPC

BandIndicator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandIndicator.G18
	# All values (2x):
	G18 | G19

Bandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bandwidth.B014
	# All values (6x):
	B014 | B030 | B050 | B100 | B150 | B200

BasebandBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BasebandBoard.BBR1
	# Last value:
	value = enums.BasebandBoard.SUW44
	# All values (140x):
	BBR1 | BBR11 | BBR12 | BBR13 | BBR14 | BBR2 | BBR21 | BBR22
	BBR23 | BBR24 | BBR3 | BBR31 | BBR32 | BBR33 | BBR34 | BBR4
	BBR41 | BBR42 | BBR43 | BBR44 | BBT1 | BBT11 | BBT12 | BBT13
	BBT14 | BBT2 | BBT21 | BBT22 | BBT23 | BBT24 | BBT3 | BBT31
	BBT32 | BBT33 | BBT34 | BBT4 | BBT41 | BBT42 | BBT43 | BBT44
	SUA012 | SUA034 | SUA056 | SUA078 | SUA1 | SUA11 | SUA112 | SUA12
	SUA13 | SUA134 | SUA14 | SUA15 | SUA156 | SUA16 | SUA17 | SUA178
	SUA18 | SUA2 | SUA21 | SUA212 | SUA22 | SUA23 | SUA234 | SUA24
	SUA25 | SUA256 | SUA26 | SUA27 | SUA278 | SUA28 | SUA3 | SUA31
	SUA312 | SUA32 | SUA33 | SUA334 | SUA34 | SUA35 | SUA356 | SUA36
	SUA37 | SUA378 | SUA38 | SUA4 | SUA41 | SUA412 | SUA42 | SUA43
	SUA434 | SUA44 | SUA45 | SUA456 | SUA46 | SUA47 | SUA478 | SUA48
	SUA5 | SUA6 | SUA7 | SUA8 | SUU1 | SUU11 | SUU12 | SUU13
	SUU14 | SUU2 | SUU21 | SUU22 | SUU23 | SUU24 | SUU3 | SUU31
	SUU32 | SUU33 | SUU34 | SUU4 | SUU41 | SUU42 | SUU43 | SUU44
	SUW1 | SUW11 | SUW12 | SUW13 | SUW14 | SUW2 | SUW21 | SUW22
	SUW23 | SUW24 | SUW3 | SUW31 | SUW32 | SUW33 | SUW34 | SUW4
	SUW41 | SUW42 | SUW43 | SUW44

BeamformingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BeamformingMode.OFF
	# All values (4x):
	OFF | ON | PMAT | TSBF

BeamformingNoOfLayers
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BeamformingNoOfLayers.L1
	# All values (3x):
	L1 | L1I | L2

BlerAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlerAlgorithm.ERC1
	# All values (4x):
	ERC1 | ERC2 | ERC3 | ERC4

BlerStopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlerStopCondition.AC1St
	# All values (5x):
	AC1St | ACWait | PCC | SCC1 | SCC2

Bursts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Bursts.FBURst
	# All values (2x):
	FBURst | RBURst

CarrAggregationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrAggregationMode.INTRaband
	# All values (2x):
	INTRaband | OFF

Cdma2kBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Cdma2kBand.BC0
	# Last value:
	value = enums.Cdma2kBand.BC9
	# All values (18x):
	BC0 | BC1 | BC10 | BC11 | BC12 | BC13 | BC14 | BC15
	BC16 | BC17 | BC2 | BC3 | BC4 | BC5 | BC6 | BC7
	BC8 | BC9

CePucchRepsA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CePucchRepsA.R1
	# All values (4x):
	R1 | R2 | R4 | R8

CePucchRepsB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CePucchRepsB.R128
	# All values (6x):
	R128 | R16 | R32 | R4 | R64 | R8

CeRepetitionsA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CeRepetitionsA.R1
	# All values (6x):
	R1 | R16 | R2 | R32 | R4 | R8

CeRepetitionsB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CeRepetitionsB.R1
	# Last value:
	value = enums.CeRepetitionsB.R8
	# All values (15x):
	R1 | R1024 | R128 | R1536 | R16 | R192 | R2048 | R256
	R32 | R384 | R4 | R512 | R64 | R768 | R8

Confidence
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Confidence.EFAil
	# All values (6x):
	EFAil | EPASs | FAIL | PASS | RUNNing | UNDecided

ConnectionType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionType.DAPPlication
	# All values (2x):
	DAPPlication | TESTmode

CoverageEnhMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CoverageEnhMode.A
	# All values (2x):
	A | B

CqiMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CqiMode.FCPRi
	# All values (6x):
	FCPRi | FCRI | FPMI | FPRI | FWB | TTIBased

CsbfDestination
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CsbfDestination.CDMA
	# All values (5x):
	CDMA | GSM | NONE | TDSCdma | WCDMa

CsiReportingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CsiReportingMode.S1
	# All values (2x):
	S1 | S2

CsirsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CsirsMode.ACSirs
	# All values (2x):
	ACSirs | MANual

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

DciFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DciFormat.D1
	# All values (8x):
	D1 | D1A | D1B | D2 | D2A | D2B | D2C | D61

DedBearerProfile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DedBearerProfile.DRAM
	# All values (4x):
	DRAM | DRUM | VIDeo | VOICe

DeviceType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeviceType.NBFBcopt
	# All values (1x):
	NBFBcopt

DownlinkNarrowBandPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DownlinkNarrowBandPosition.GPP3
	# All values (4x):
	GPP3 | HIGH | LOW | MID

DownlinkRsrcBlockPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DownlinkRsrcBlockPosition.HIGH
	# All values (7x):
	HIGH | LOW | P10 | P23 | P35 | P48 | P5

DpCycle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DpCycle.P032
	# All values (4x):
	P032 | P064 | P128 | P256

DsTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DsTime.OFF
	# All values (4x):
	OFF | ON | P1H | P2H

DuplexMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DuplexMode.FDD
	# All values (3x):
	FDD | FTDD | TDD

EblerStopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EblerStopCondition.CLEVel
	# All values (2x):
	CLEVel | NONE

EmtcRmcPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EmtcRmcPattern.P1
	# All values (5x):
	P1 | P2 | P3 | P4 | P5

EnableCqiReport
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnableCqiReport.OFF
	# All values (2x):
	OFF | PERiodic

EnableDrx
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnableDrx.DRXL
	# All values (5x):
	DRXL | DRXS | OFF | ON | UDEFined

EnablePreambles
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnablePreambles.NIPReambles
	# All values (3x):
	NIPReambles | OFF | ON

FadingBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingBoard.FAD012
	# Last value:
	value = enums.FadingBoard.FAD8
	# All values (60x):
	FAD012 | FAD034 | FAD056 | FAD078 | FAD1 | FAD11 | FAD112 | FAD12
	FAD13 | FAD134 | FAD14 | FAD15 | FAD156 | FAD16 | FAD17 | FAD178
	FAD18 | FAD2 | FAD21 | FAD212 | FAD22 | FAD23 | FAD234 | FAD24
	FAD25 | FAD256 | FAD26 | FAD27 | FAD278 | FAD28 | FAD3 | FAD31
	FAD312 | FAD32 | FAD33 | FAD334 | FAD34 | FAD35 | FAD356 | FAD36
	FAD37 | FAD378 | FAD38 | FAD4 | FAD41 | FAD412 | FAD42 | FAD43
	FAD434 | FAD44 | FAD45 | FAD456 | FAD46 | FAD47 | FAD478 | FAD48
	FAD5 | FAD6 | FAD7 | FAD8

FadingMatrixMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingMatrixMode.KRONecker
	# All values (3x):
	KRONecker | NORMal | SCWI

FadingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FadingMode.NORMal
	# All values (2x):
	NORMal | USER

FadingProfile
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.FadingProfile.CTESt
	# Last value:
	value = enums.FadingProfile.USER
	# All values (41x):
	CTESt | EP5High | EP5Low | EP5Medium | ET3High | ET3Low | ET3Medium | ET7High
	ET7Low | ET7Medium | ETH30 | ETL30 | ETM30 | EV5High | EV5Low | EV5Medium
	EV7High | EV7Low | EV7Medium | EVH200 | EVL200 | EVM200 | HST | HST2
	HSTRain | IILS | IINL | IRALos | IRANlos | ISALos | ISANlos | IUALos
	IUANlos | IULS | IUNLos1 | IUNLos2 | UMA3 | UMA30 | UMI3 | UMI30
	USER

FilterCoefficient
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterCoefficient.FC4
	# All values (2x):
	FC4 | FC8

FilterRsrpqCoefficient
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterRsrpqCoefficient.FC0
	# All values (2x):
	FC0 | FC4

FrameStructure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameStructure.T1
	# All values (3x):
	T1 | T2 | T3

GeoScope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GeoScope.CIMMediate
	# All values (4x):
	CIMMediate | CNORmal | LOCation | PLMN

GeranBband
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.GeranBband.G045
	# Last value:
	value = enums.GeranBband.G19
	# All values (11x):
	G045 | G048 | G071 | G075 | G081 | G085 | G09E | G09P
	G09R | G18 | G19

GsmBand
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GsmBand.G04
	# All values (6x):
	G04 | G085 | G09 | G18 | G19 | GT081

HandoverDestination
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HandoverDestination.CDMA
	# All values (6x):
	CDMA | EVDO | GSM | LTE | TDSCdma | WCDMa

HandoverMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HandoverMode.HANDover
	# All values (3x):
	HANDover | MTCSfallback | REDirection

HeaderCompression
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HeaderCompression.ADB
	# All values (2x):
	ADB | VVB

IdleDrxLength
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IdleDrxLength.L1024
	# Last value:
	value = enums.IdleDrxLength.L8192
	# All values (14x):
	L1024 | L10240 | L12288 | L131072 | L14336 | L16384 | L2048 | L262144
	L32768 | L4096 | L512 | L6144 | L65536 | L8192

IdleLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IdleLevel.LEV0
	# All values (5x):
	LEV0 | LEV1 | LEV2 | LEV3 | UE

InactivityTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.InactivityTimer.PSF1
	# Last value:
	value = enums.InactivityTimer.PSF80
	# All values (22x):
	PSF1 | PSF10 | PSF100 | PSF1280 | PSF1920 | PSF2 | PSF20 | PSF200
	PSF2560 | PSF3 | PSF30 | PSF300 | PSF4 | PSF40 | PSF5 | PSF50
	PSF500 | PSF6 | PSF60 | PSF750 | PSF8 | PSF80

InsertLossMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InsertLossMode.LACP
	# All values (3x):
	LACP | NORMal | USER

InterBandHandoverMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InterBandHandoverMode.BHANdover
	# All values (2x):
	BHANdover | REDirection

IntervalA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IntervalA.I1
	# All values (4x):
	I1 | I2 | I4 | I8

IntervalB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IntervalB.I16
	# All values (4x):
	I16 | I2 | I4 | I8

IntervalC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IntervalC.S10
	# Last value:
	value = enums.IntervalC.S80
	# All values (10x):
	S10 | S128 | S160 | S20 | S32 | S320 | S40 | S64
	S640 | S80

IpAddress
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddress.IP1
	# All values (3x):
	IP1 | IP2 | IP3

IpVersion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpVersion.IPV4
	# All values (3x):
	IPV4 | IPV46 | IPV6

IqOutSampleRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutSampleRate.M1
	# All values (8x):
	M1 | M100 | M15 | M19 | M3 | M30 | M7 | M9

KeepConstant
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.KeepConstant.DSHift
	# All values (2x):
	DSHift | SPEed

LaaPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LaaPeriod.MS160
	# All values (3x):
	MS160 | MS40 | MS80

LaaUePeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LaaUePeriod.MS160
	# All values (5x):
	MS160 | MS320 | MS40 | MS640 | MS80

LastMessageSent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LastMessageSent.FAILed
	# All values (4x):
	FAILed | OFF | ON | SUCCessful

LdCycle
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LdCycle.SF10
	# Last value:
	value = enums.LdCycle.SF80
	# All values (20x):
	SF10 | SF1024 | SF10240 | SF128 | SF1280 | SF160 | SF20 | SF2048
	SF256 | SF2560 | SF32 | SF320 | SF40 | SF512 | SF5120 | SF60
	SF64 | SF640 | SF70 | SF80

LdsPeriod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LdsPeriod.M160
	# All values (3x):
	M160 | M40 | M80

LimitErrRation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LimitErrRation.P001
	# All values (3x):
	P001 | P010 | P050

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

LogCategory2
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory2.CONTinue
	# All values (5x):
	CONTinue | ERRor | HIDDen | INFO | WARNing

LongSmsHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LongSmsHandling.MSMS
	# All values (2x):
	MSMS | TRUNcate

MainState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MainState.OFF
	# All values (3x):
	OFF | ON | RFHandover

MaxNuRohcConSes
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MaxNuRohcConSes.CS1024
	# Last value:
	value = enums.MaxNuRohcConSes.CS8
	# All values (14x):
	CS1024 | CS12 | CS128 | CS16 | CS16384 | CS2 | CS24 | CS256
	CS32 | CS4 | CS48 | CS512 | CS64 | CS8

MeasCellCycle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasCellCycle.OFF
	# All values (8x):
	OFF | SF1024 | SF1280 | SF160 | SF256 | SF320 | SF512 | SF640

MessageClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageClass.CL0
	# All values (5x):
	CL0 | CL1 | CL2 | CL3 | NONE

MessageHandling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageHandling.FILE
	# All values (3x):
	FILE | INTernal | UCODed

MessageHandlingB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MessageHandlingB.FILE
	# All values (2x):
	FILE | INTernal

MessageType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MessageType.AAMBer
	# Last value:
	value = enums.MessageType.UDETws
	# All values (12x):
	AAMBer | AEXTreme | APResidentia | ASEVere | EARThquake | ETWarning | ETWTest | GFENcing
	TSUNami | UDCMas | UDEFined | UDETws

MimoMatrixSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MimoMatrixSelection.CM3Gpp
	# All values (4x):
	CM3Gpp | HADamard | IDENtity | UDEFined

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.Q1024
	# All values (5x):
	Q1024 | Q16 | Q256 | Q64 | QPSK

MpdcchRepetitions
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MpdcchRepetitions.MR1
	# Last value:
	value = enums.MpdcchRepetitions.MR8
	# All values (9x):
	MR1 | MR128 | MR16 | MR2 | MR256 | MR32 | MR4 | MR64
	MR8

MprachRepetitions
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MprachRepetitions.R1
	# Last value:
	value = enums.MprachRepetitions.R8
	# All values (9x):
	R1 | R128 | R16 | R2 | R256 | R32 | R4 | R64
	R8

MpschArepetitions
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MpschArepetitions.MR16
	# All values (3x):
	MR16 | MR32 | NCON

MpschBrepetitions
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.MpschBrepetitions.MR1024
	# Last value:
	value = enums.MpschBrepetitions.NCON
	# All values (9x):
	MR1024 | MR1536 | MR192 | MR2048 | MR256 | MR384 | MR512 | MR768
	NCON

MultiClusterDlTable
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MultiClusterDlTable.DETermined
	# All values (2x):
	DETermined | UDEFined

NbValue
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NbValue.NB2T
	# Last value:
	value = enums.NbValue.NBT8
	# All values (11x):
	NB2T | NB4T | NBT | NBT128 | NBT16 | NBT2 | NBT256 | NBT32
	NBT4 | NBT64 | NBT8

NetworkSegment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkSegment.A
	# All values (3x):
	A | B | C

NominalPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NominalPowerMode.AUToranging
	# All values (3x):
	AUToranging | MANual | ULPC

NoOfDigits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoOfDigits.THRee
	# All values (2x):
	THRee | TWO

NoOfLayers
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoOfLayers.L2
	# All values (2x):
	L2 | L4

NumberRb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NumberRb.N1
	# Last value:
	value = enums.NumberRb.ZERO
	# All values (41x):
	N1 | N10 | N100 | N12 | N15 | N16 | N17 | N18
	N2 | N20 | N21 | N24 | N25 | N27 | N3 | N30
	N32 | N36 | N4 | N40 | N42 | N45 | N48 | N5
	N50 | N54 | N6 | N60 | N64 | N7 | N72 | N75
	N8 | N80 | N81 | N83 | N9 | N90 | N92 | N96
	ZERO

NumberRb2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NumberRb2.N1
	# Last value:
	value = enums.NumberRb2.ZERO
	# All values (13x):
	N1 | N12 | N15 | N18 | N2 | N21 | N24 | N3
	N4 | N5 | N6 | N9 | ZERO

OccOfdmSymbols
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OccOfdmSymbols.SYM0
	# Last value:
	value = enums.OccOfdmSymbols.SYM9
	# All values (15x):
	SYM0 | SYM1 | SYM10 | SYM11 | SYM12 | SYM13 | SYM14 | SYM2
	SYM3 | SYM4 | SYM5 | SYM6 | SYM7 | SYM8 | SYM9

OnDurationTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OnDurationTimer.PSF1
	# Last value:
	value = enums.OnDurationTimer.PSF800
	# All values (24x):
	PSF1 | PSF10 | PSF100 | PSF1000 | PSF1200 | PSF1600 | PSF2 | PSF20
	PSF200 | PSF3 | PSF30 | PSF300 | PSF4 | PSF40 | PSF400 | PSF5
	PSF50 | PSF500 | PSF6 | PSF60 | PSF600 | PSF8 | PSF80 | PSF800

OperatingBandA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperatingBandA.OB1
	# All values (3x):
	OB1 | OB2 | OB3

OperatingBandB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperatingBandB.OB1
	# Last value:
	value = enums.OperatingBandB.OBS3
	# All values (24x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB19 | OB2
	OB20 | OB21 | OB22 | OB25 | OB26 | OB3 | OB4 | OB5
	OB6 | OB7 | OB8 | OB9 | OBL1 | OBS1 | OBS2 | OBS3

OperatingBandC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperatingBandC.OB1
	# Last value:
	value = enums.OperatingBandC.UDEFined
	# All values (71x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB250 | OB252 | OB255 | OB26 | OB27 | OB28
	OB29 | OB3 | OB30 | OB31 | OB32 | OB33 | OB34 | OB35
	OB36 | OB37 | OB38 | OB39 | OB4 | OB40 | OB41 | OB42
	OB43 | OB44 | OB45 | OB46 | OB48 | OB49 | OB5 | OB50
	OB51 | OB52 | OB53 | OB6 | OB65 | OB66 | OB67 | OB68
	OB69 | OB7 | OB70 | OB71 | OB72 | OB73 | OB74 | OB75
	OB76 | OB8 | OB85 | OB87 | OB88 | OB9 | UDEFined

OperatingBandD
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.OperatingBandD.OB1
	# Last value:
	value = enums.OperatingBandD.OB9
	# All values (32x):
	OB1 | OB10 | OB11 | OB12 | OB13 | OB14 | OB15 | OB16
	OB17 | OB18 | OB19 | OB2 | OB20 | OB21 | OB22 | OB23
	OB24 | OB25 | OB26 | OB27 | OB28 | OB29 | OB3 | OB30
	OB31 | OB32 | OB4 | OB5 | OB6 | OB7 | OB8 | OB9

PallocConfig
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PallocConfig.BOTH
	# All values (4x):
	BOTH | END | INIT | NO

PathCompAlpha
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathCompAlpha.DOT4
	# All values (8x):
	DOT4 | DOT5 | DOT6 | DOT7 | DOT8 | DOT9 | ONE | ZERO

PdcchSymbolsCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PdcchSymbolsCount.AUTO
	# All values (5x):
	AUTO | P1 | P2 | P3 | P4

PortsMapping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PortsMapping.R1
	# All values (2x):
	R1 | R1R2

PowerOffset
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerOffset.N3DB
	# All values (3x):
	N3DB | N6DB | ZERO

PreambleTransmReps
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PreambleTransmReps.R1
	# All values (8x):
	R1 | R128 | R16 | R2 | R32 | R4 | R64 | R8

PrecodingMatrixMode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PrecodingMatrixMode.PMI0
	# Last value:
	value = enums.PrecodingMatrixMode.RANDom_pmi
	# All values (17x):
	PMI0 | PMI1 | PMI10 | PMI11 | PMI12 | PMI13 | PMI14 | PMI15
	PMI2 | PMI3 | PMI4 | PMI5 | PMI6 | PMI7 | PMI8 | PMI9
	RANDom_pmi

Priority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Priority.BACKground
	# All values (3x):
	BACKground | HIGH | NORMal

PrStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrStep.P2DB
	# All values (4x):
	P2DB | P4DB | P6DB | ZERO

PswAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PswAction.CONNect
	# All values (7x):
	CONNect | DETach | DISConnect | HANDover | OFF | ON | SMS

PswState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PswState.ATTached
	# Last value:
	value = enums.PswState.SMESsage
	# All values (12x):
	ATTached | CESTablished | CONNecting | DISConnect | IHANdover | OFF | OHANdover | ON
	PAGing | RMESsage | SIGNaling | SMESsage

PucchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PucchFormat.F1BCs
	# All values (4x):
	F1BCs | F3 | F4 | F5

Qoffset
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Qoffset.N1
	# Last value:
	value = enums.Qoffset.ZERO
	# All values (31x):
	N1 | N10 | N12 | N14 | N16 | N18 | N2 | N20
	N22 | N24 | N3 | N4 | N5 | N6 | N8 | P1
	P10 | P12 | P14 | P16 | P18 | P2 | P20 | P22
	P24 | P3 | P4 | P5 | P6 | P8 | ZERO

RandomValueMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RandomValueMode.EVEN
	# All values (2x):
	EVEN | ODD

RbPosition
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RbPosition.FULL
	# Last value:
	value = enums.RbPosition.P99
	# All values (56x):
	FULL | HIGH | LOW | MID | P0 | P1 | P10 | P11
	P12 | P13 | P14 | P15 | P16 | P19 | P2 | P20
	P21 | P22 | P24 | P25 | P28 | P3 | P30 | P31
	P33 | P36 | P37 | P39 | P4 | P40 | P43 | P44
	P45 | P48 | P49 | P50 | P51 | P52 | P54 | P56
	P57 | P58 | P6 | P62 | P63 | P66 | P68 | P7
	P70 | P74 | P75 | P8 | P83 | P9 | P96 | P99

RedundancyVerSequence
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RedundancyVerSequence.TS1
	# All values (3x):
	TS1 | TS4 | UDEFined

RejectAttachCause
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RejectAttachCause.C10
	# Last value:
	value = enums.RejectAttachCause.TANA12
	# All values (38x):
	C10 | C100 | C101 | C111 | C13 | C14 | C15 | C16
	C17 | C18 | C19 | C2 | C20 | C21 | C23 | C24
	C25 | C26 | C35 | C39 | C40 | C42 | C5 | C6
	C8 | C9 | C95 | C96 | C97 | C98 | C99 | CONG22
	EPS7 | IUE3 | OFF | ON | PLMN11 | TANA12

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

RepetitionLevel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RepetitionLevel.RL1
	# All values (4x):
	RL1 | RL2 | RL3 | RL4

ReportInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ReportInterval.I1024
	# All values (8x):
	I1024 | I10240 | I120 | I2048 | I240 | I480 | I5120 | I640

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

RestartMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RestartMode.AUTO
	# All values (3x):
	AUTO | MANual | TRIGger

RetransmissionTimer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RetransmissionTimer.PSF0
	# Last value:
	value = enums.RetransmissionTimer.PSF96
	# All values (17x):
	PSF0 | PSF1 | PSF112 | PSF128 | PSF16 | PSF160 | PSF2 | PSF24
	PSF320 | PSF33 | PSF4 | PSF40 | PSF6 | PSF64 | PSF8 | PSF80
	PSF96

RlcMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RlcMode.AM
	# All values (2x):
	AM | UM

RpControlPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RpControlPattern.RDA
	# All values (6x):
	RDA | RDB | RDC | RUA | RUB | RUC

RrcState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RrcState.CONNected
	# All values (2x):
	CONNected | IDLE

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

SccAction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SccAction.MACactivate
	# All values (6x):
	MACactivate | MACDeactivat | OFF | ON | RRCadd | RRCDelete

Scenario
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Scenario.AD
	# Last value:
	value = enums.Scenario.TROF
	# All values (106x):
	AD | ADF | BF | BFF | BFSM4 | BH | BHF | CAFF
	CAFR | CATF | CATR | CC | CCMP | CCMS1 | CF | CFF
	CH | CHF | CHSM4 | CJ | CJF | CJFS4 | CJSM4 | CL
	DD | DH | DHF | DJ | DJSM4 | DL | DLSM4 | DN
	DNSM4 | DP | DPF | EE | EJ | EJF | EL | ELSM4
	EN | ENSM4 | EP | EPF | EPFS4 | EPSM4 | ER | ERSM4
	ET | FF | FL | FLF | FN | FNSM4 | FP | FPF
	FPFS4 | FPSM4 | FR | FRSM4 | FT | FTSM4 | FV | FVSM4
	FX | GG | GN | GNF | GP | GPF | GPFS4 | GPSM4
	GR | GRSM4 | GT | GTSM4 | GV | GVSM4 | GX | GXSM4
	GYA | GYAS4 | GYC | HH | HP | HPF | HR | HRSM4
	HT | HTSM4 | HV | HVSM4 | HX | HXSM4 | HYA | HYAS4
	HYC | HYCS4 | HYE | HYES4 | HYG | NAV | SCEL | SCF
	TRO | TROF

SchedulingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SchedulingType.CQI
	# All values (7x):
	CQI | EMAMode | EMCSched | RMC | SPS | UDCHannels | UDTTibased

SdCycle
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SdCycle.SF10
	# Last value:
	value = enums.SdCycle.SF80
	# All values (17x):
	SF10 | SF128 | SF16 | SF160 | SF2 | SF20 | SF256 | SF32
	SF320 | SF4 | SF40 | SF5 | SF512 | SF64 | SF640 | SF8
	SF80

SearchSpace
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SearchSpace.COMM
	# All values (2x):
	COMM | UESP

SecurityAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SecurityAlgorithm.NULL
	# All values (2x):
	NULL | S3G

SemissionValue
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SemissionValue.NS01
	# Last value:
	value = enums.SemissionValue.NS32
	# All values (32x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS11 | NS12 | NS13 | NS14 | NS15 | NS16
	NS17 | NS18 | NS19 | NS20 | NS21 | NS22 | NS23 | NS24
	NS25 | NS26 | NS27 | NS28 | NS29 | NS30 | NS31 | NS32

SetPosition
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SetPosition.INV
	# Last value:
	value = enums.SetPosition.SCC7
	# All values (9x):
	INV | PCC | SCC1 | SCC2 | SCC3 | SCC4 | SCC5 | SCC6
	SCC7

SetType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SetType.ALT0
	# Last value:
	value = enums.SetType.UDSingle
	# All values (10x):
	ALT0 | CLOop | CONStant | FULPower | MAXPower | MINPower | RPControl | SINGle
	UDContinuous | UDSingle

SignalingGeneratorState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalingGeneratorState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

SmsCodingGroup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsCodingGroup.DCMClass
	# All values (2x):
	DCMClass | GDCoding

SmsDataCoding
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmsDataCoding.BIT7
	# All values (2x):
	BIT7 | BIT8

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SourceTime
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceTime.CMWTime
	# All values (2x):
	CMWTime | DATE

SpsInteval
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SpsInteval.S1
	# Last value:
	value = enums.SpsInteval.SADL
	# All values (16x):
	S1 | S10 | S128 | S160 | S2 | S20 | S3 | S32
	S320 | S4 | S40 | S5 | S64 | S640 | S80 | SADL

StartingPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StartingPosition.OFDM0
	# All values (2x):
	OFDM0 | OFDM7

SubFramePattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubFramePattern.HAB10
	# All values (3x):
	HAB10 | HAB8 | STANdard

SupportedExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SupportedExt.NINFormation
	# All values (3x):
	NINFormation | NSUPported | SUPPorted

SupportedLong
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SupportedLong.NSUPported
	# All values (2x):
	NSUPported | SUPPorted

Symbols
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Symbols.S0
	# Last value:
	value = enums.Symbols.S9
	# All values (15x):
	S0 | S1 | S10 | S11 | S12 | S13 | S14 | S2
	S3 | S4 | S5 | S6 | S7 | S8 | S9

SymbolsDuration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SymbolsDuration.S1
	# All values (5x):
	S1 | S14 | S28 | S42 | S70

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.MACactivated
	# All values (4x):
	MACactivated | OFF | ON | RRCadded

SyncZone
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncZone.NONE
	# All values (2x):
	NONE | Z1

Table
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Table.ANY
	# All values (7x):
	ANY | CW1 | CW2 | OTLC1 | OTLC2 | TFLC1 | TFLC2

TimeResolution
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeResolution.HRES
	# All values (1x):
	HRES

TransBlockSizeIdx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TransBlockSizeIdx.T1
	# Last value:
	value = enums.TransBlockSizeIdx.ZERO
	# All values (38x):
	T1 | T10 | T11 | T12 | T13 | T14 | T15 | T16
	T17 | T18 | T19 | T2 | T20 | T21 | T22 | T23
	T24 | T25 | T26 | T27 | T28 | T29 | T3 | T30
	T31 | T32 | T33 | T34 | T35 | T36 | T37 | T4
	T5 | T6 | T7 | T8 | T9 | ZERO

TransGap
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransGap.G040
	# All values (2x):
	G040 | G080

TransmissionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransmissionMode.TM1
	# All values (8x):
	TM1 | TM2 | TM3 | TM4 | TM6 | TM7 | TM8 | TM9

TransmitAntenaSelection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransmitAntenaSelection.OFF
	# All values (2x):
	OFF | OLOop

TransmitAttempts
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TransmitAttempts.A10
	# All values (7x):
	A10 | A3 | A4 | A5 | A6 | A7 | A8

TransmScheme
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TransmScheme.CLSingle
	# Last value:
	value = enums.TransmScheme.UNDefined
	# All values (13x):
	CLSingle | CLSMultiplex | DBF78 | FBF710 | OLSMultiplex | S7I8 | SBF5 | SBF8
	SIMO | SISO | TBF79 | TXDiversity | UNDefined

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (86x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IFO1 | IFO2 | IFO3 | IFO4 | IFO5 | IFO6 | IQ2O | IQ4O
	IQ6O | IQ8O | R10D | R118 | R1183 | R1184 | R11C | R11D
	R11O | R11O3 | R11O4 | R12C | R12D | R13C | R13O | R14C
	R214 | R218 | R21C | R21O | R22C | R23C | R23O | R24C
	R258 | R318 | R31C | R31O | R32C | R33C | R33O | R34C
	R418 | R41C | R41O | R42C | R43C | R43O | R44C | RA18
	RB14 | RB18 | RC18 | RD18 | RE18 | RF18 | RF1C | RF1O
	RF2C | RF3C | RF3O | RF4C | RF5C | RF6C | RF7C | RF8C
	RF9C | RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

TxRxConfiguration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxRxConfiguration.DUAL
	# All values (2x):
	DUAL | SINGle

UeCatManual
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UeCatManual._0
	# Last value:
	value = enums.UeCatManual.M2
	# All values (15x):
	_0 | _1 | _10 | _11 | _12 | _2 | _3 | _4
	_5 | _6 | _7 | _8 | _9 | M1 | M2

UeChangesType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeChangesType.RRCReconfig
	# All values (2x):
	RRCReconfig | SIBPaging

UeProcessesCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeProcessesCount.N1
	# All values (3x):
	N1 | N3 | N4

UeSidelinkProcessesCount
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeSidelinkProcessesCount.N400
	# All values (2x):
	N400 | N50

UeUsage
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UeUsage.DCENtric
	# All values (2x):
	DCENtric | VCENtric

UlHarqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UlHarqMode.D0ONly
	# All values (5x):
	D0ONly | D0PHich | PHIChonly | PNACk | PND0

UlPwrMaster
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UlPwrMaster.PCC
	# All values (8x):
	PCC | SCC1 | SCC2 | SCC3 | SCC4 | SCC5 | SCC6 | SCC7

UpDownDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpDownDirection.DOWN
	# All values (2x):
	DOWN | UP

UplinkNarrowBandPosition
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.UplinkNarrowBandPosition.HIGH
	# Last value:
	value = enums.UplinkNarrowBandPosition.NB9
	# All values (16x):
	HIGH | LOW | NB1 | NB10 | NB11 | NB12 | NB13 | NB14
	NB2 | NB3 | NB4 | NB5 | NB6 | NB7 | NB8 | NB9

VdPreference
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VdPreference.CVONly
	# All values (4x):
	CVONly | CVPRefered | IPVonly | IPVPrefered

VolteHandoverType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VolteHandoverType.PSData
	# All values (2x):
	PSData | PSVolte

Window
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Window.W10240
	# Last value:
	value = enums.Window.W8960
	# All values (16x):
	W10240 | W11520 | W1280 | W12800 | W14080 | W15360 | W16640 | W17920
	W19200 | W20480 | W2560 | W3840 | W5120 | W6400 | W7680 | W8960

WmQuantity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.WmQuantity.ECNO
	# All values (2x):
	ECNO | RSCP

