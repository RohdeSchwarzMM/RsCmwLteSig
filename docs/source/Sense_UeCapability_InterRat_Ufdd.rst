Ufdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UFDD:SUPPorted

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UFDD:SUPPorted



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Ufdd.UfddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.interRat.ufdd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_InterRat_Ufdd_Eredirection.rst