PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PIRPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PIRPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: