Stti
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPRI:DL:STTI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPRI:DL:STTI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fpri.Downlink.Stti.SttiCls
	:members:
	:undoc-members:
	:noindex: