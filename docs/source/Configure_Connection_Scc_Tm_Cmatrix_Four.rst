Four<MatrixFourLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.connection.scc.tm.cmatrix.four.repcap_matrixFourLine_get()
	driver.configure.connection.scc.tm.cmatrix.four.repcap_matrixFourLine_set(repcap.MatrixFourLine.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:FOUR<line>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:FOUR<line>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Cmatrix.Four.FourCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.tm.cmatrix.four.clone()