RepCaps
=========



























Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

Anb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Anb.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

CellNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CellNo.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

ClippingCounter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ClippingCounter.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

EutraBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.EutraBand.Band1
	# Values (4x):
	Band1 | Band2 | Band3 | Band4

HMatrixColumn
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.HMatrixColumn.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

HMatrixRow
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.HMatrixRow.Row1
	# Range:
	Row1 .. Row8
	# All values (8x):
	Row1 | Row2 | Row3 | Row4 | Row5 | Row6 | Row7 | Row8

IPversion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IPversion.IPv4
	# Values (2x):
	IPv4 | IPv6

MatrixEightLine
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MatrixEightLine.Nr1
	# Values (2x):
	Nr1 | Nr2

MatrixFourLine
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MatrixFourLine.Nr1
	# Values (2x):
	Nr1 | Nr2

MatrixLine
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MatrixLine.Line1
	# Values (4x):
	Line1 | Line2 | Line3 | Line4

MatrixTwoLine
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MatrixTwoLine.Nr1
	# Values (2x):
	Nr1 | Nr2

Mimo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Mimo.M42
	# Values (2x):
	M42 | M44

Output
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Output.Out1
	# Values (4x):
	Out1 | Out2 | Out3 | Out4

Path
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Path.Path1
	# Values (2x):
	Path1 | Path2

QAMmodulationOrder
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QAMmodulationOrder.QAM64
	# Values (2x):
	QAM64 | QAM256

QAMmodulationOrderB
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QAMmodulationOrderB.QAM256
	# Values (2x):
	QAM256 | QAM1024

ReliabilityIndicatorNo
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ReliabilityIndicatorNo.RIno1
	# Values (4x):
	RIno1 | RIno2 | RIno3 | RIno4

SecondaryCompCarrier
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondaryCompCarrier.CC1
	# Range:
	CC1 .. CC7
	# All values (7x):
	CC1 | CC2 | CC3 | CC4 | CC5 | CC6 | CC7

Stream
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Stream.S1
	# Values (2x):
	S1 | S2

SystemInfoBlock
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SystemInfoBlock.Sib8
	# Values (2x):
	Sib8 | Sib16

TbsIndexAlt
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TbsIndexAlt.Nr2
	# Values (2x):
	Nr2 | Nr3

Text
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Text.T3324
	# Values (3x):
	T3324 | T3402 | T3412

UeReport
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UeReport.V1020
	# Values (2x):
	V1020 | V1090

ULqam
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ULqam.QAM64
	# Values (1x):
	QAM64

UTddFreq
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UTddFreq.Freq128
	# Values (3x):
	Freq128 | Freq384 | Freq768

