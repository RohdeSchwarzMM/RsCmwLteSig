UeIdentity
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:UEIDentity:IMSI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:UEIDentity:IMSI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.UeIdentity.UeIdentityCls
	:members:
	:undoc-members:
	:noindex: