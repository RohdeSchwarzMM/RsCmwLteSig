Easy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:EASY:BFBW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:EASY:BFBW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Easy.EasyCls
	:members:
	:undoc-members:
	:noindex: