UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:MCSTable:CSIRs:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:MCSTable:CSIRs:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcpri.Downlink.McsTable.Csirs.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: