Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:FREQuency:INTRa
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:FREQuency:INTer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:FREQuency:INTRa
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:FREQuency:INTer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.Ncsacq.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: