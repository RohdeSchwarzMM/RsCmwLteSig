FullSubFrames
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Laa.Fburst.FullSubFrames.FullSubFramesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udChannels.laa.fburst.fullSubFrames.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdChannels_Laa_Fburst_FullSubFrames_Downlink.rst
	Configure_Connection_Scc_UdChannels_Laa_Fburst_FullSubFrames_Mcluster.rst