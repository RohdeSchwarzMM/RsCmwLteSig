Gvsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GVSM<MIMO4x4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GVSM<MIMO4x4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gvsm.GvsmCls
	:members:
	:undoc-members:
	:noindex: