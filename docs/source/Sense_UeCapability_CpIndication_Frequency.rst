Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:FREQuency:INTRa
	single: SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:FREQuency:INTer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:FREQuency:INTRa
	SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:FREQuency:INTer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.CpIndication.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: