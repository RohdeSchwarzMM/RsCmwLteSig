Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:HPUSch:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:HPUSch:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Hpusch.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: