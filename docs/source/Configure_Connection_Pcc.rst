Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:HDUPlex
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TTIBundling
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:DLEQual
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TRANsmission
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:DCIFormat
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:NENBantennas
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:NOLayers
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PMATrix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:HDUPlex
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TTIBundling
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:DLEQual
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TRANsmission
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:DCIFormat
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:NENBantennas
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:NOLayers
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PMATrix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Beamforming.rst
	Configure_Connection_Pcc_Cscheduling.rst
	Configure_Connection_Pcc_Emamode.rst
	Configure_Connection_Pcc_Fcpri.rst
	Configure_Connection_Pcc_Fcri.rst
	Configure_Connection_Pcc_FcttiBased.rst
	Configure_Connection_Pcc_Fpmi.rst
	Configure_Connection_Pcc_Fpri.rst
	Configure_Connection_Pcc_Fwbcqi.rst
	Configure_Connection_Pcc_Hpusch.rst
	Configure_Connection_Pcc_Mcluster.rst
	Configure_Connection_Pcc_Pdcch.rst
	Configure_Connection_Pcc_Pucch.rst
	Configure_Connection_Pcc_Pzero.rst
	Configure_Connection_Pcc_Qam.rst
	Configure_Connection_Pcc_Rmc.rst
	Configure_Connection_Pcc_SchModel.rst
	Configure_Connection_Pcc_Sps.rst
	Configure_Connection_Pcc_Stype.rst
	Configure_Connection_Pcc_Tia.rst
	Configure_Connection_Pcc_Tm.rst
	Configure_Connection_Pcc_UdChannels.rst
	Configure_Connection_Pcc_UdttiBased.rst