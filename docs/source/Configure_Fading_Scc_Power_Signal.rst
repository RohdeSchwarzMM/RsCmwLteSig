Signal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:SIGNal

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:SIGNal



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Power.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex: