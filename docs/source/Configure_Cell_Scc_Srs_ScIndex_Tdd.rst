Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SCINdex:TDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SCINdex:TDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.ScIndex.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: