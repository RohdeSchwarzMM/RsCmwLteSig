Single
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:SINGle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:SINGle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.Pusch.Tpc.Single.SingleCls
	:members:
	:undoc-members:
	:noindex: