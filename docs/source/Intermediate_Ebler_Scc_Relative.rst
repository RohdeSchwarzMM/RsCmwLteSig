Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RELative

.. code-block:: python

	FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Scc.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: