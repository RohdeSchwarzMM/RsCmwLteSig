Confidence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:CONFidence

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:CONFidence



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Confidence.ConfidenceCls
	:members:
	:undoc-members:
	:noindex: