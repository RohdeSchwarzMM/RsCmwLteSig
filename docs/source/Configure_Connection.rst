Connection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:DEDBearer
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:RLCMode
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:IPVersion
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:APN
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:QCI
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UDSCheduling
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:IUGNrb
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:IUGMcsidx
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UETSelection
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SRPRindex
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SRCindex
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:TAControl
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:IDCHsindic
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SIBReconfig
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:GHOPping
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:PSMallowed
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:IEMergency
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:EOISupport
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SDNSpco
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:DPCYcle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:PCNB
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CTYPe
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:KRRC
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:RITimer
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:FCOefficient
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:TMODe
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:DLEinsertion
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:DLPadding
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ASEMission
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SUITx
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:OBCHange
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:FCHange
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:AMDBearer

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:DEDBearer
	CONFigure:LTE:SIGNaling<instance>:CONNection:RLCMode
	CONFigure:LTE:SIGNaling<instance>:CONNection:IPVersion
	CONFigure:LTE:SIGNaling<instance>:CONNection:APN
	CONFigure:LTE:SIGNaling<instance>:CONNection:QCI
	CONFigure:LTE:SIGNaling<instance>:CONNection:UDSCheduling
	CONFigure:LTE:SIGNaling<instance>:CONNection:IUGNrb
	CONFigure:LTE:SIGNaling<instance>:CONNection:IUGMcsidx
	CONFigure:LTE:SIGNaling<instance>:CONNection:UETSelection
	CONFigure:LTE:SIGNaling<instance>:CONNection:SRPRindex
	CONFigure:LTE:SIGNaling<instance>:CONNection:SRCindex
	CONFigure:LTE:SIGNaling<instance>:CONNection:TAControl
	CONFigure:LTE:SIGNaling<instance>:CONNection:IDCHsindic
	CONFigure:LTE:SIGNaling<instance>:CONNection:SIBReconfig
	CONFigure:LTE:SIGNaling<instance>:CONNection:GHOPping
	CONFigure:LTE:SIGNaling<instance>:CONNection:PSMallowed
	CONFigure:LTE:SIGNaling<instance>:CONNection:IEMergency
	CONFigure:LTE:SIGNaling<instance>:CONNection:EOISupport
	CONFigure:LTE:SIGNaling<instance>:CONNection:SDNSpco
	CONFigure:LTE:SIGNaling<instance>:CONNection:DPCYcle
	CONFigure:LTE:SIGNaling<instance>:CONNection:PCNB
	CONFigure:LTE:SIGNaling<instance>:CONNection:CTYPe
	CONFigure:LTE:SIGNaling<instance>:CONNection:KRRC
	CONFigure:LTE:SIGNaling<instance>:CONNection:RITimer
	CONFigure:LTE:SIGNaling<instance>:CONNection:FCOefficient
	CONFigure:LTE:SIGNaling<instance>:CONNection:TMODe
	CONFigure:LTE:SIGNaling<instance>:CONNection:DLEinsertion
	CONFigure:LTE:SIGNaling<instance>:CONNection:DLPadding
	CONFigure:LTE:SIGNaling<instance>:CONNection:ASEMission
	CONFigure:LTE:SIGNaling<instance>:CONNection:SUITx
	CONFigure:LTE:SIGNaling<instance>:CONNection:OBCHange
	CONFigure:LTE:SIGNaling<instance>:CONNection:FCHange
	CONFigure:LTE:SIGNaling<instance>:CONNection:AMDBearer



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cdrx.rst
	Configure_Connection_Csfb.rst
	Configure_Connection_Easy.rst
	Configure_Connection_Edau.rst
	Configure_Connection_Harq.rst
	Configure_Connection_Nta.rst
	Configure_Connection_Pcc.rst
	Configure_Connection_Rohc.rst
	Configure_Connection_Scc.rst
	Configure_Connection_SipHandling.rst
	Configure_Connection_TdBearer.rst
	Configure_Connection_UeCategory.rst
	Configure_Connection_UePosition.rst