Uplink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Uplink_Pdu.rst