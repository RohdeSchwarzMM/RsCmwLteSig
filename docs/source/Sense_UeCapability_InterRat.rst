InterRat
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.InterRatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.interRat.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_InterRat_Cdma.rst
	Sense_UeCapability_InterRat_Chrpd.rst
	Sense_UeCapability_InterRat_Cxrtt.rst
	Sense_UeCapability_InterRat_Geran.rst
	Sense_UeCapability_InterRat_Ufdd.rst
	Sense_UeCapability_InterRat_Utdd.rst