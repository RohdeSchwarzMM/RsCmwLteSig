Gx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GX[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GX[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gx.GxCls
	:members:
	:undoc-members:
	:noindex: