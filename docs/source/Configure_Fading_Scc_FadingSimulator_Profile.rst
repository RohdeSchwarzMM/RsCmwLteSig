Profile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:PROFile

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:PROFile



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Profile.ProfileCls
	:members:
	:undoc-members:
	:noindex: