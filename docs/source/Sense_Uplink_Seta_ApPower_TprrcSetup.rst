TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:TPRRcsetup:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:TPRRcsetup:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: