RfBands
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.UeCapability.RfBands.RfBandsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueCapability.rfBands.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeCapability_RfBands_All.rst