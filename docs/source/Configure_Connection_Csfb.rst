Csfb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:DESTination

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:DESTination



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Csfb.CsfbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.csfb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Csfb_Gsm.rst
	Configure_Connection_Csfb_Tdscdma.rst
	Configure_Connection_Csfb_Wcdma.rst