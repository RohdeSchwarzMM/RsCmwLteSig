Utdd<UTddFreq>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Freq128 .. Freq768
	rc = driver.sense.ueCapability.interRat.utdd.repcap_uTddFreq_get()
	driver.sense.ueCapability.interRat.utdd.repcap_uTddFreq_set(repcap.UTddFreq.Freq128)





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Utdd.UtddCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.interRat.utdd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_InterRat_Utdd_Eredirection.rst
	Sense_UeCapability_InterRat_Utdd_Supported.rst