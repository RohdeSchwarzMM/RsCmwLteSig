Trace
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Trace_CqiReporting.rst
	Ebler_Trace_Throughput.rst