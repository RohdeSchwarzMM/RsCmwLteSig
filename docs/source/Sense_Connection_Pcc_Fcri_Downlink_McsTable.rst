McsTable
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcri.Downlink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.pcc.fcri.downlink.mcsTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Pcc_Fcri_Downlink_McsTable_Determined.rst
	Sense_Connection_Pcc_Fcri_Downlink_McsTable_Ssubframe.rst