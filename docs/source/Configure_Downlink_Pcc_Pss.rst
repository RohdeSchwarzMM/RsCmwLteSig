Pss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PSS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PSS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Pss.PssCls
	:members:
	:undoc-members:
	:noindex: