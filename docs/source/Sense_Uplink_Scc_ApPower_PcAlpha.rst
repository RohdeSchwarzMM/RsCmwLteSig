PcAlpha
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.scc.apPower.pcAlpha.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Scc_ApPower_PcAlpha_Basic.rst