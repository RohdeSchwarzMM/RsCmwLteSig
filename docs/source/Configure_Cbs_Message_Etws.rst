Etws
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ETWS:ALERt
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ETWS:POPup

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ETWS:ALERt
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:ETWS:POPup



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.Etws.EtwsCls
	:members:
	:undoc-members:
	:noindex: