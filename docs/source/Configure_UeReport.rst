UeReport
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:WMQuantity
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:MGENable
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:MGPeriod
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:RINTerval
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:MCSCell
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:AINTerrupt

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:ENABle
	CONFigure:LTE:SIGNaling<instance>:UEReport:WMQuantity
	CONFigure:LTE:SIGNaling<instance>:UEReport:MGENable
	CONFigure:LTE:SIGNaling<instance>:UEReport:MGPeriod
	CONFigure:LTE:SIGNaling<instance>:UEReport:RINTerval
	CONFigure:LTE:SIGNaling<instance>:UEReport:MCSCell
	CONFigure:LTE:SIGNaling<instance>:UEReport:AINTerrupt



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.UeReportCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeReport_Fcoefficient.rst
	Configure_UeReport_Scc.rst