Advanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PCALpha:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PCALpha:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.PcAlpha.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex: