Manual
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:MANual:ENHanced
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:MANual

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:MANual:ENHanced
	CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:MANual



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.UeCategory.Manual.ManualCls
	:members:
	:undoc-members:
	:noindex: