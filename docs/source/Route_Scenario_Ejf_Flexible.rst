Flexible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:EJF[:FLEXible]:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:EJF[:FLEXible]:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ejf.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: