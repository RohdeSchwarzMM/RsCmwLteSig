Pdcch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.pdcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Pdcch_Poffset.rst