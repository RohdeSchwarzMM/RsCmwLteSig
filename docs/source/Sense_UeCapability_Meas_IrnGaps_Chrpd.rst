Chrpd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:CHRPd

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:CHRPd



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.Chrpd.ChrpdCls
	:members:
	:undoc-members:
	:noindex: