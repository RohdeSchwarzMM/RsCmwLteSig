Ncell<CellNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.sense.ueReport.ncell.repcap_cellNo_get()
	driver.sense.ueReport.ncell.repcap_cellNo_set(repcap.CellNo.Nr1)





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.NcellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Cdma.rst
	Sense_UeReport_Ncell_Evdo.rst
	Sense_UeReport_Ncell_Gsm.rst
	Sense_UeReport_Ncell_Lte.rst
	Sense_UeReport_Ncell_Tdscdma.rst
	Sense_UeReport_Ncell_Wcdma.rst