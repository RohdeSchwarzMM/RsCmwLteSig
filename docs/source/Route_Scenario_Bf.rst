Bf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:BF[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:BF[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Bf.BfCls
	:members:
	:undoc-members:
	:noindex: