Player
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:UTASupported
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:USRSsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:TAPPsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:TWEFsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:PDSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:CCSSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:SPPSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:MCPCsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:NURClist

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:UTASupported
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:USRSsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:TAPPsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:TWEFsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:PDSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:CCSSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:SPPSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:MCPCsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:PLAYer:NURClist



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.Player.PlayerCls
	:members:
	:undoc-members:
	:noindex: