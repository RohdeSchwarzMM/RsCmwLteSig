Tro
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:TRO:FLEXible

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:TRO:FLEXible



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Tro.TroCls
	:members:
	:undoc-members:
	:noindex: