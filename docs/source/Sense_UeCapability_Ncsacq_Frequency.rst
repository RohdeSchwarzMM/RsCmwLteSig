Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:NCSacq:FREQuency:INTRa
	single: SENSe:LTE:SIGNaling<instance>:UECapability:NCSacq:FREQuency:INTer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:NCSacq:FREQuency:INTRa
	SENSe:LTE:SIGNaling<instance>:UECapability:NCSacq:FREQuency:INTer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Ncsacq.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: