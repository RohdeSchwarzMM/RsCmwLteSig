Call
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Call.CallCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_A.rst
	Call_B.rst
	Call_Pswitched.rst
	Call_Scc.rst