DcParameters
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:DCParameters:DTSCg
	single: SENSe:LTE:SIGNaling<instance>:UECapability:DCParameters:DTSPlit

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:DCParameters:DTSCg
	SENSe:LTE:SIGNaling<instance>:UECapability:DCParameters:DTSPlit



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.DcParameters.DcParametersCls
	:members:
	:undoc-members:
	:noindex: