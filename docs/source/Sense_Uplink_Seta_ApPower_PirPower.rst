PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PIRPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PIRPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: