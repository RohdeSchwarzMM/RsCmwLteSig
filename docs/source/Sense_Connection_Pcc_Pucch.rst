Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:PUCCh:FFCA

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:PUCCh:FFCA



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex: