Csirs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:CSIRs:MODE
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:CSIRs:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:CSIRs:MODE
	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:CSIRs:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: