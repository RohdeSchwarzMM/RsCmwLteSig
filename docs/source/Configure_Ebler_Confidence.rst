Confidence
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:OASCondition
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:MTTime
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:LERate

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:OASCondition
	CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:MTTime
	CONFigure:LTE:SIGNaling<instance>:EBLer:CONFidence:LERate



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ebler.Confidence.ConfidenceCls
	:members:
	:undoc-members:
	:noindex: