Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:CDMA:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Cdma.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: