PriReporting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:PRIReporting:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting:PRIReporting:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.PriReporting.PriReportingCls
	:members:
	:undoc-members:
	:noindex: