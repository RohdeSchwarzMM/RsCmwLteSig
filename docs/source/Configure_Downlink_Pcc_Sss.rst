Sss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:SSS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:SSS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Sss.SssCls
	:members:
	:undoc-members:
	:noindex: