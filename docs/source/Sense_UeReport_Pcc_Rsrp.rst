Rsrp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:RSRP:RANGe
	single: SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:RSRP

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:RSRP:RANGe
	SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:RSRP



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Pcc.Rsrp.RsrpCls
	:members:
	:undoc-members:
	:noindex: