Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.cell.scc.repcap_secondaryCompCarrier_get()
	driver.configure.cell.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Scc_Cid.rst
	Configure_Cell_Scc_Csat.rst
	Configure_Cell_Scc_Dbandwidth.rst
	Configure_Cell_Scc_Pcid.rst
	Configure_Cell_Scc_ScMuting.rst
	Configure_Cell_Scc_Srs.rst
	Configure_Cell_Scc_Ssubframe.rst
	Configure_Cell_Scc_Sync.rst
	Configure_Cell_Scc_UlDl.rst
	Configure_Cell_Scc_UlSupport.rst