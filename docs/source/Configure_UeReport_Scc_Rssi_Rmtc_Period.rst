Period
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:RMTC:PERiod

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:RMTC:PERiod



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Rssi.Rmtc.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex: