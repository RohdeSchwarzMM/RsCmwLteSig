Csirs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.tm.csirs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Tm_Csirs_Aports.rst
	Configure_Connection_Scc_Tm_Csirs_Power.rst
	Configure_Connection_Scc_Tm_Csirs_Resource.rst
	Configure_Connection_Scc_Tm_Csirs_Subframe.rst