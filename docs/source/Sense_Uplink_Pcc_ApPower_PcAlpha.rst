PcAlpha
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PCALpha:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PCALpha:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Pcc.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex: