Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:SCELl:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:SCELl:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.Scell.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: