Period
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:DMTC:PERiod

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:DMTC:PERiod



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Dmtc.Period.PeriodCls
	:members:
	:undoc-members:
	:noindex: