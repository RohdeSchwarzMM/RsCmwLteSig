Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:TROFading:FLEXible[:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:TROFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:TROFading:FLEXible[:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:TROFading:FLEXible:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.TroFading.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: