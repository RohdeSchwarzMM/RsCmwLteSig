Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CAFF:FLEXible[:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CAFF:FLEXible:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CAFF:FLEXible[:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:CAFF:FLEXible:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Caff.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: