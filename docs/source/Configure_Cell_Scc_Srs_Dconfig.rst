Dconfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:DCONfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:DCONfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.Dconfig.DconfigCls
	:members:
	:undoc-members:
	:noindex: