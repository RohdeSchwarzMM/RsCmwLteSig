Awgn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:FOFFset
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:SNRatio
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:MEASurement

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:ENABle
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:FOFFset
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:SNRatio
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:MEASurement



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_Awgn_Bandwidth.rst