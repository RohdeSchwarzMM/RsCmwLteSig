SpfSubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:SPFSubframe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:SPFSubframe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Fburst.SpfSubframe.SpfSubframeCls
	:members:
	:undoc-members:
	:noindex: