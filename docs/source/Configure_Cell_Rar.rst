Rar
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Rar.RarCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.rar.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Rar_Cmcs.rst