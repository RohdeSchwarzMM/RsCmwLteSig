Cmatrix
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.Cmatrix.CmatrixCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.tm.cmatrix.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Tm_Cmatrix_Eight.rst
	Configure_Connection_Pcc_Tm_Cmatrix_Four.rst
	Configure_Connection_Pcc_Tm_Cmatrix_Mimo.rst
	Configure_Connection_Pcc_Tm_Cmatrix_Two.rst