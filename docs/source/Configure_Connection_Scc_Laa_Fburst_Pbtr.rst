Pbtr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:PBTR

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:PBTR



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Fburst.Pbtr.PbtrCls
	:members:
	:undoc-members:
	:noindex: