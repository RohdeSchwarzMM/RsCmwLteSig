Adf
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Adf.AdfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.adf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Adf_Flexible.rst