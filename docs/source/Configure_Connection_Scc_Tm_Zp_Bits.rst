Bits
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:ZP:BITS

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:ZP:BITS



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Zp.Bits.BitsCls
	:members:
	:undoc-members:
	:noindex: