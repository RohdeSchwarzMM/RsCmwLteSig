A
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUCCh:A:CERepetition

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUCCh:A:CERepetition



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pucch.A.ACls
	:members:
	:undoc-members:
	:noindex: