Text<Text>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: T3324 .. T3412
	rc = driver.configure.cell.timeout.text.repcap_text_get()
	driver.configure.cell.timeout.text.repcap_text_set(repcap.Text.T3324)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:TEXT<nr>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:TEXT<nr>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Timeout.Text.TextCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.timeout.text.clone()