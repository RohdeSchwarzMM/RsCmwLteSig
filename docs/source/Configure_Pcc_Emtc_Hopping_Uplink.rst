Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:HOFFset
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:HOFFset
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.hopping.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Hopping_Uplink_A.rst
	Configure_Pcc_Emtc_Hopping_Uplink_B.rst