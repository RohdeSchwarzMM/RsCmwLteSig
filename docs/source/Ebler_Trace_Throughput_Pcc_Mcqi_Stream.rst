Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.ebler.trace.throughput.pcc.mcqi.stream.repcap_stream_get()
	driver.ebler.trace.throughput.pcc.mcqi.stream.repcap_stream_set(repcap.Stream.S1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput[:PCC]:MCQI:STReam<Stream>

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput[:PCC]:MCQI:STReam<Stream>



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.Throughput.Pcc.Mcqi.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.throughput.pcc.mcqi.stream.clone()