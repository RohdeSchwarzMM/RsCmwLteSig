Cell
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:CPRefix
	single: CONFigure:LTE:SIGNaling<instance>:CELL:MCC
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TAC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:CPRefix
	CONFigure:LTE:SIGNaling<instance>:CELL:MCC
	CONFigure:LTE:SIGNaling<instance>:CELL:TAC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.CellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Acause.rst
	Configure_Cell_Bandwidth.rst
	Configure_Cell_Mnc.rst
	Configure_Cell_Nas.rst
	Configure_Cell_Pcc.rst
	Configure_Cell_Prach.rst
	Configure_Cell_Rar.rst
	Configure_Cell_Rcause.rst
	Configure_Cell_ReSelection.rst
	Configure_Cell_Scc.rst
	Configure_Cell_Security.rst
	Configure_Cell_Tdd.rst
	Configure_Cell_Time.rst
	Configure_Cell_Timeout.rst
	Configure_Cell_UeIdentity.rst