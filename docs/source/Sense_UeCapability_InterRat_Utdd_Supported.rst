Supported
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UTDD<frequency>:SUPPorted

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UTDD<frequency>:SUPPorted



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Utdd.Supported.SupportedCls
	:members:
	:undoc-members:
	:noindex: