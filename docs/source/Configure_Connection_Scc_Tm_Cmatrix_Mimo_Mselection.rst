Mselection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TM<nr>:CMATrix:MIMO<Mimo>:MSELection

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TM<nr>:CMATrix:MIMO<Mimo>:MSELection



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Cmatrix.Mimo.Mselection.MselectionCls
	:members:
	:undoc-members:
	:noindex: