Ssubframe
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcpri.Downlink.McsTable.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.pcc.fcpri.downlink.mcsTable.ssubframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Pcc_Fcpri_Downlink_McsTable_Ssubframe_Determined.rst