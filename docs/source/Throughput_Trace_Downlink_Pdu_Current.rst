Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:CURRent
	single: READ:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:CURRent

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:CURRent
	READ:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:CURRent



.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Downlink.Pdu.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: