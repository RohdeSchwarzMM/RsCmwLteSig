ReSelection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:RESelection:TSLow

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:RESelection:TSLow



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.ReSelection.ReSelectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.reSelection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_ReSelection_Quality.rst
	Configure_Cell_ReSelection_Search.rst