Dp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DP[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:DP[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Dp.DpCls
	:members:
	:undoc-members:
	:noindex: