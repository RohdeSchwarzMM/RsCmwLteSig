Rindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PDSCh:RINDex

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PDSCh:RINDex



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pdsch.Rindex.RindexCls
	:members:
	:undoc-members:
	:noindex: