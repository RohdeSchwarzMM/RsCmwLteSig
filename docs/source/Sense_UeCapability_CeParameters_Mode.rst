Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:CEParameters:MODE:A
	single: SENSe:LTE:SIGNaling<instance>:UECapability:CEParameters:MODE:B

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:CEParameters:MODE:A
	SENSe:LTE:SIGNaling<instance>:UECapability:CEParameters:MODE:B



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.CeParameters.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: