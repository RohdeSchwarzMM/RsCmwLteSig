IpSubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:IPSubframe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:IPSubframe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.IpSubframe.IpSubframeCls
	:members:
	:undoc-members:
	:noindex: