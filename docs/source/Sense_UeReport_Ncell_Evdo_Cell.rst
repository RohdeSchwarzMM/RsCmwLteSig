Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:EVDO:CELL<nr>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:EVDO:CELL<nr>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Evdo.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: