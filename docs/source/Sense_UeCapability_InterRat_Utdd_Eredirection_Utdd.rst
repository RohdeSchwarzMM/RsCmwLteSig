Utdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UTDD<frequency>:EREDirection:UTDD

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UTDD<frequency>:EREDirection:UTDD



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Utdd.Eredirection.Utdd.UtddCls
	:members:
	:undoc-members:
	:noindex: