SfConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SFConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:SFConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.SfConfig.SfConfigCls
	:members:
	:undoc-members:
	:noindex: