Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:SET
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:RPControl
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:TPOWer
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:UDPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:SET
	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:RPControl
	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:TPOWer
	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUSCh:TPC:UDPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.Pusch.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.setc.pusch.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Setc_Pusch_Tpc_CltPower.rst
	Configure_Uplink_Setc_Pusch_Tpc_Pexecute.rst
	Configure_Uplink_Setc_Pusch_Tpc_Single.rst