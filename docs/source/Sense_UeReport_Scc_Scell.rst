Scell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:SCELl

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:SCELl



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.scc.scell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Scc_Scell_Range.rst