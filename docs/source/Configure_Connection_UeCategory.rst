UeCategory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:CZALlowed

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:CZALlowed



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.UeCategory.UeCategoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.ueCategory.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_UeCategory_Manual.rst
	Configure_Connection_UeCategory_Reported.rst