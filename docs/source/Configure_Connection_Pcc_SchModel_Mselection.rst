Mselection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:MSELection:MIMO<Mimo>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:MSELection:MIMO<Mimo>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.SchModel.Mselection.MselectionCls
	:members:
	:undoc-members:
	:noindex: