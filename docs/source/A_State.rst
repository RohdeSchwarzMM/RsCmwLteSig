State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:A:STATe

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:A:STATe



.. autoclass:: RsCmwLteSig.Implementations.A.State.StateCls
	:members:
	:undoc-members:
	:noindex: