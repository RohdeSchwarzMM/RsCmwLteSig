Ftsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FTSM<MIMO4x4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FTSM<MIMO4x4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ftsm.FtsmCls
	:members:
	:undoc-members:
	:noindex: