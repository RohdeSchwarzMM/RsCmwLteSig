Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Evdo.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: