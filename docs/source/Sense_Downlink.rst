Downlink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Downlink_Pcc.rst
	Sense_Downlink_Scc.rst