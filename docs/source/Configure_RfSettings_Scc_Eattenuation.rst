Eattenuation
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.scc.eattenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Scc_Eattenuation_InputPy.rst
	Configure_RfSettings_Scc_Eattenuation_Output.rst