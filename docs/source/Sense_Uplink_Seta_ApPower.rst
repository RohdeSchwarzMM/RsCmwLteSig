ApPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PATHloss
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:EPPPower
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:EOPower

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PATHloss
	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:EPPPower
	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:EOPower



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.ApPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.seta.apPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Seta_ApPower_PcAlpha.rst
	Sense_Uplink_Seta_ApPower_PirPower.rst
	Sense_Uplink_Seta_ApPower_Pnpusch.rst
	Sense_Uplink_Seta_ApPower_RsPower.rst
	Sense_Uplink_Seta_ApPower_TprrcSetup.rst