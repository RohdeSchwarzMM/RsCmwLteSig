Subframe
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Subframe.SubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.scc.harq.stream.subframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Scc_Harq_Stream_Subframe_Absolute.rst
	Ebler_Scc_Harq_Stream_Subframe_Relative.rst