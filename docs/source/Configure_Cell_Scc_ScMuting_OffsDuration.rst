OffsDuration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:OFFSduration

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:OFFSduration



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.ScMuting.OffsDuration.OffsDurationCls
	:members:
	:undoc-members:
	:noindex: