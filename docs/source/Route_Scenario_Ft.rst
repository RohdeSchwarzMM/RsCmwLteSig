Ft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FT[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FT[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ft.FtCls
	:members:
	:undoc-members:
	:noindex: