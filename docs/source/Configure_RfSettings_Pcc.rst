Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:MLOFfset
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDSeparation
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:ENPower
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:ENPMode
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UMARgin

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:MLOFfset
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDSeparation
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:ENPower
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:ENPMode
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UMARgin



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_AfBands.rst
	Configure_RfSettings_Pcc_Channel.rst
	Configure_RfSettings_Pcc_Eattenuation.rst
	Configure_RfSettings_Pcc_Foffset.rst
	Configure_RfSettings_Pcc_UserDefined.rst