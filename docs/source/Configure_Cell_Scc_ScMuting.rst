ScMuting
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.ScMuting.ScMutingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.scc.scMuting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Scc_ScMuting_OffsDuration.rst
	Configure_Cell_Scc_ScMuting_OnsDuration.rst
	Configure_Cell_Scc_ScMuting_Pmac.rst