MpAttempts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:MPATtempts

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:MPATtempts



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.Level.Prach.MpAttempts.MpAttemptsCls
	:members:
	:undoc-members:
	:noindex: