UdSeparation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDSeparation

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDSeparation



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UdSeparation.UdSeparationCls
	:members:
	:undoc-members:
	:noindex: