Fstructure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:FSTRucture

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:FSTRucture



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Fstructure.FstructureCls
	:members:
	:undoc-members:
	:noindex: