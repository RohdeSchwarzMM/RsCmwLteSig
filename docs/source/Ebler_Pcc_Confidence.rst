Confidence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:CONFidence

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:CONFidence



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Confidence.ConfidenceCls
	:members:
	:undoc-members:
	:noindex: