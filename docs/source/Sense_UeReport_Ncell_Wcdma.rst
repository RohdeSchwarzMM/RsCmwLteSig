Wcdma
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.wcdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Wcdma_Cell.rst