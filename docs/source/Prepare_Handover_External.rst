External
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:DESTination

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:DESTination



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_External_Cdma.rst
	Prepare_Handover_External_Evdo.rst
	Prepare_Handover_External_Gsm.rst
	Prepare_Handover_External_Lte.rst
	Prepare_Handover_External_Tdscdma.rst
	Prepare_Handover_External_Wcdma.rst