Eredirection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UFDD:EREDirection:UTRA

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:UFDD:EREDirection:UTRA



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Ufdd.Eredirection.EredirectionCls
	:members:
	:undoc-members:
	:noindex: