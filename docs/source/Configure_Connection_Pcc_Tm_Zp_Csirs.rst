Csirs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:ZP:CSIRs:SUBFrame

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:ZP:CSIRs:SUBFrame



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.Zp.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: