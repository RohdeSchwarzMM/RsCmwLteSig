Tm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<8>:CHMatrix
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:PMATrix
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CODewords
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:NTXantennas

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<8>:CHMatrix
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:PMATrix
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CODewords
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:NTXantennas



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.TmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.tm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Tm_Cmatrix.rst
	Configure_Connection_Pcc_Tm_Csirs.rst
	Configure_Connection_Pcc_Tm_Zp.rst