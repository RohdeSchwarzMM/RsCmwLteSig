Cocc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:COCC

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:COCC



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.Cocc.CoccCls
	:members:
	:undoc-members:
	:noindex: