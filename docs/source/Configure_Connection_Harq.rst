Harq
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Harq.HarqCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.harq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Harq_Downlink.rst
	Configure_Connection_Harq_Uplink.rst