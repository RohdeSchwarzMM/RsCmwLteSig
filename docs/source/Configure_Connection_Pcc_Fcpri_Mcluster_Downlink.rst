Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcpri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: