Ssubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SSUBframe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SSUBframe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex: