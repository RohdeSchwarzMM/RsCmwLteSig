Eredirection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:EREDirection:UTRA
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:EREDirection:UTDD

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:EREDirection:UTRA
	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:IRAT:EREDirection:UTDD



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.InterRat.Eredirection.EredirectionCls
	:members:
	:undoc-members:
	:noindex: