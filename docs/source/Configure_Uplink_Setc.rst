Setc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PMAX

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PMAX



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.SetcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.setc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Setc_ApPower.rst
	Configure_Uplink_Setc_Pucch.rst
	Configure_Uplink_Setc_Pusch.rst