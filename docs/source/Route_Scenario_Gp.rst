Gp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GP[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GP[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gp.GpCls
	:members:
	:undoc-members:
	:noindex: