State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:PSWitched:STATe

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:PSWitched:STATe



.. autoclass:: RsCmwLteSig.Implementations.Pswitched.State.StateCls
	:members:
	:undoc-members:
	:noindex: