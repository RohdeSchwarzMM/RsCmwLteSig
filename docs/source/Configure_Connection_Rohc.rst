Rohc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:EFOR
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:EFOR
	CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Rohc.RohcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.rohc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Rohc_Profiles.rst
	Configure_Connection_Rohc_UlOnly.rst