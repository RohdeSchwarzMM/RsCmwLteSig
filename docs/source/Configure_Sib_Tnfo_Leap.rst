Leap
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SIB<n>:TNFO<tnfo>:LEAP

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SIB<n>:TNFO<tnfo>:LEAP



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sib.Tnfo.Leap.LeapCls
	:members:
	:undoc-members:
	:noindex: