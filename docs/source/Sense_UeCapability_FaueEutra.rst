FaueEutra
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.FaueEutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.faueEutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_FaueEutra_FgIndicators.rst
	Sense_UeCapability_FaueEutra_InterRat.rst
	Sense_UeCapability_FaueEutra_Ncsacq.rst
	Sense_UeCapability_FaueEutra_Player.rst