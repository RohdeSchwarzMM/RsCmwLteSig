El
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:EL[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:EL[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.El.ElCls
	:members:
	:undoc-members:
	:noindex: