Pmcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PMCC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PMCC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pmcc.PmccCls
	:members:
	:undoc-members:
	:noindex: