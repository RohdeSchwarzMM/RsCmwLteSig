Mimo
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Cmatrix.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.tm.cmatrix.mimo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Tm_Cmatrix_Mimo_Line.rst
	Configure_Connection_Scc_Tm_Cmatrix_Mimo_Mselection.rst