Seta
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.SetaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.seta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Seta_ApPower.rst