Dn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DN[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:DN[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Dn.DnCls
	:members:
	:undoc-members:
	:noindex: