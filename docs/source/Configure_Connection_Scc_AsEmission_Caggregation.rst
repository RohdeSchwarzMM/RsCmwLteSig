Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:ASEMission:CAGGregation

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:ASEMission:CAGGregation



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.AsEmission.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: