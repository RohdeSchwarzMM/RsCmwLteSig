ScIndex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:SCINdex:FDD
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:SCINdex:TDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:SCINdex:FDD
	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:SCINdex:TDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Pcc.Srs.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: