Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: