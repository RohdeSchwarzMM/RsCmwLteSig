TroFading
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.TroFading.TroFadingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.troFading.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_TroFading_Flexible.rst