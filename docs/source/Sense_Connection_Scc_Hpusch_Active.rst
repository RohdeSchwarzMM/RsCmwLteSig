Active
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:HPUSch:ACTive

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:HPUSch:ACTive



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Hpusch.Active.ActiveCls
	:members:
	:undoc-members:
	:noindex: