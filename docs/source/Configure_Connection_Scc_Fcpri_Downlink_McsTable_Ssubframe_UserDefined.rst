UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:MCSTable:SSUBframe:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:MCSTable:SSUBframe:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcpri.Downlink.McsTable.Ssubframe.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: