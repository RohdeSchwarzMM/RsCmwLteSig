Ratio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:BWIDth:RATio

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:BWIDth:RATio



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.Bandwidth.Ratio.RatioCls
	:members:
	:undoc-members:
	:noindex: