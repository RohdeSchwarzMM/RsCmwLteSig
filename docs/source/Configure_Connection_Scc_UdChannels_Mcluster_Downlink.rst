Downlink<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.configure.connection.scc.udChannels.mcluster.downlink.repcap_stream_get()
	driver.configure.connection.scc.udChannels.mcluster.downlink.repcap_stream_set(repcap.Stream.S1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:MCLuster:DL<Stream>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:MCLuster:DL<Stream>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udChannels.mcluster.downlink.clone()