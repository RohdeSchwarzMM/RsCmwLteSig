SchModel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SchModel.SchModelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.schModel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_SchModel_Enable.rst
	Configure_Connection_Scc_SchModel_Mimo.rst
	Configure_Connection_Scc_SchModel_Mselection.rst