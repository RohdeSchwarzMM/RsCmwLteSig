Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:SET
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:RPControl
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:CLTPower
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:TPOWer
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:UDPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:SET
	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:RPControl
	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:CLTPower
	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:TPOWer
	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:UDPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Pcc.Pusch.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.pcc.pusch.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Pcc_Pusch_Tpc_Pexecute.rst
	Configure_Uplink_Pcc_Pusch_Tpc_Single.rst