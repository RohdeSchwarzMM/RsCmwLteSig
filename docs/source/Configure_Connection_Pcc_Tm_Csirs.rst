Csirs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:APORts
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:SUBFrame
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:RESource
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:POWer

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:APORts
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:SUBFrame
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:RESource
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CSIRs:POWer



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: