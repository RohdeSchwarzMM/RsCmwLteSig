InterFreqNgaps
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IFNGaps

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IFNGaps



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.InterFreqNgaps.InterFreqNgapsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.meas.interFreqNgaps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Meas_InterFreqNgaps_V.rst