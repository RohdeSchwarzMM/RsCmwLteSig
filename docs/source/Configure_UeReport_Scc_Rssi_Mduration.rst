Mduration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:MDURation

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSSI:MDURation



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Rssi.Mduration.MdurationCls
	:members:
	:undoc-members:
	:noindex: