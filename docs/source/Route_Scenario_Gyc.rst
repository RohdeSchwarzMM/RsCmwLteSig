Gyc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GYC[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GYC[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gyc.GycCls
	:members:
	:undoc-members:
	:noindex: