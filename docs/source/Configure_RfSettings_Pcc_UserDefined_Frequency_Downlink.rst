Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:DL:MINimum
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:DL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:DL:MINimum
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:FREQuency:DL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.Frequency.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: