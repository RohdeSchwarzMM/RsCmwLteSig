Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:UL<qam>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:UL<qam>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: