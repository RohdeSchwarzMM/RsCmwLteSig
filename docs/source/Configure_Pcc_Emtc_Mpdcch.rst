Mpdcch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:SSPace
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:RLEVel
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:MREPetitions
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:MRPaging

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:SSPace
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:RLEVel
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:MREPetitions
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:MPDCch:MRPaging



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Mpdcch.MpdcchCls
	:members:
	:undoc-members:
	:noindex: