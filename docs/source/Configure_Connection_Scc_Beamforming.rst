Beamforming
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Beamforming.BeamformingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.beamforming.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Beamforming_Matrix.rst
	Configure_Connection_Scc_Beamforming_Mode.rst
	Configure_Connection_Scc_Beamforming_NoLayers.rst