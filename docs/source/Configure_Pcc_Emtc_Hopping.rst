Hopping
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.HoppingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.hopping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Hopping_Downlink.rst
	Configure_Pcc_Emtc_Hopping_Uplink.rst