Flexible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GPFS<MIMO4x4>[:FLEXible]:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GPFS<MIMO4x4>[:FLEXible]:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gpfs.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: