Eutran
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:CID:EUTRan

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:CID:EUTRan



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Cid.Eutran.EutranCls
	:members:
	:undoc-members:
	:noindex: