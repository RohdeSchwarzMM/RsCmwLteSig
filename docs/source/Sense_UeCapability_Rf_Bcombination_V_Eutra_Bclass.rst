Bclass
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.Bclass.BclassCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rf.bcombination.v.eutra.bclass.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Rf_Bcombination_V_Eutra_Bclass_Downlink.rst
	Sense_UeCapability_Rf_Bcombination_V_Eutra_Bclass_Uplink.rst