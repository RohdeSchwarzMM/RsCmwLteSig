Meas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:RMWideband
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:BFINterrupt
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:RCOReporting

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:RMWideband
	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:BFINterrupt
	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:RCOReporting



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.MeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.meas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Meas_InterFreqNgaps.rst
	Sense_UeCapability_Meas_IrnGaps.rst