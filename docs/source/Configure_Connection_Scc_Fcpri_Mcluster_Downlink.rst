Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcpri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: