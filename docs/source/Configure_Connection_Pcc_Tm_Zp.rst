Zp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:ZP:BITS

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:ZP:BITS



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.Zp.ZpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.tm.zp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Tm_Zp_Csirs.rst