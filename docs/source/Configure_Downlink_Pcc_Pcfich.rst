Pcfich
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PCFich:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PCFich:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Pcfich.PcfichCls
	:members:
	:undoc-members:
	:noindex: