Time
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:TSOurce
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:DSTime
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:LTZoffset
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SATTach
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SNName

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:TSOurce
	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:DSTime
	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:LTZoffset
	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SATTach
	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SNName



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Time_Date.rst
	Configure_Cell_Time_Snow.rst
	Configure_Cell_Time_Time.rst