Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:PNPusch:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:PNPusch:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: