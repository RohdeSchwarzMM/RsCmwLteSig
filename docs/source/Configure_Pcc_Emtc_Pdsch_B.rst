B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:B:CERepetition
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:B:MRCE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:B:CERepetition
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PDSCh:B:MRCE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pdsch.B.BCls
	:members:
	:undoc-members:
	:noindex: