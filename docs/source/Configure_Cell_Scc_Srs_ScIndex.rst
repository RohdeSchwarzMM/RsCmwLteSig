ScIndex
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.scc.srs.scIndex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Scc_Srs_ScIndex_Fdd.rst
	Configure_Cell_Scc_Srs_ScIndex_Tdd.rst