A
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:A:INTerval

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:A:INTerval



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Downlink.A.ACls
	:members:
	:undoc-members:
	:noindex: