Foffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:FOFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:FOFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex: