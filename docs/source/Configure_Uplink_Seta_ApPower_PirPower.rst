PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:PIRPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:PIRPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: