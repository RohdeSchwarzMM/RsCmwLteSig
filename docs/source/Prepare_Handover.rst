Handover
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover
	single: PREPare:LTE:SIGNaling<instance>:HANDover:DESTination
	single: PREPare:LTE:SIGNaling<instance>:HANDover:MMODe
	single: PREPare:LTE:SIGNaling<instance>:HANDover:CTYPe

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover
	PREPare:LTE:SIGNaling<instance>:HANDover:DESTination
	PREPare:LTE:SIGNaling<instance>:HANDover:MMODe
	PREPare:LTE:SIGNaling<instance>:HANDover:CTYPe



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.HandoverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.handover.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Handover_Catalog.rst
	Prepare_Handover_Enhanced.rst
	Prepare_Handover_External.rst