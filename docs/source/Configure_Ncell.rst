Ncell<CellNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.configure.ncell.repcap_cellNo_get()
	driver.configure.ncell.repcap_cellNo_set(repcap.CellNo.Nr1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.NcellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_All.rst
	Configure_Ncell_Cdma.rst
	Configure_Ncell_Evdo.rst
	Configure_Ncell_Gsm.rst
	Configure_Ncell_Lte.rst
	Configure_Ncell_Tdscdma.rst
	Configure_Ncell_Wcdma.rst