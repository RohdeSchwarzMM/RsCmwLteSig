Language
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:LANGuage

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:LANGuage



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.Language.LanguageCls
	:members:
	:undoc-members:
	:noindex: