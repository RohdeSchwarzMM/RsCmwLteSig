Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PNPusch:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PNPusch:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: