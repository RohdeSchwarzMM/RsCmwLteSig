Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:UL:UCSPecific
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:UL:UCSPecific
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Foffset.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: