CpIndication
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:UTRan

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:CPINdication:UTRan



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.CpIndication.CpIndicationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.cpIndication.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_CpIndication_Frequency.rst