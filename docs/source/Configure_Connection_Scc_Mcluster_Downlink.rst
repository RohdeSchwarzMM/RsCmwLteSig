Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: