Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Lte.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: