Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PUCCh:FFCA

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PUCCh:FFCA



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex: