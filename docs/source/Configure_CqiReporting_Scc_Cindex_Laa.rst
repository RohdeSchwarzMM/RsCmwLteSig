Laa
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex:LAA

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex:LAA



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.Scc.Cindex.Laa.LaaCls
	:members:
	:undoc-members:
	:noindex: