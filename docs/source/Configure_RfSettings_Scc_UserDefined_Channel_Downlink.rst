Downlink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Channel.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.scc.userDefined.channel.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Scc_UserDefined_Channel_Downlink_Maximum.rst
	Configure_RfSettings_Scc_UserDefined_Channel_Downlink_Minimum.rst