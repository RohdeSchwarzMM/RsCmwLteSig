Tprobability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:TPRobability

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:TPRobability



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.Tprobability.TprobabilityCls
	:members:
	:undoc-members:
	:noindex: