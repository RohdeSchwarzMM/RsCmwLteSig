Chsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CHSM<MIMO44>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CHSM<MIMO44>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Chsm.ChsmCls
	:members:
	:undoc-members:
	:noindex: