Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Gsm.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: