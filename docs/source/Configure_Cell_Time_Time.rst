Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:TIME

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:TIME



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Time.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: