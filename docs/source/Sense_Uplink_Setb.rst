Setb
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.SetbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.setb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Setb_ApPower.rst