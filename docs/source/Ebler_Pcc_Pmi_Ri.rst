Ri<ReliabilityIndicatorNo>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: RIno1 .. RIno4
	rc = driver.ebler.pcc.pmi.ri.repcap_reliabilityIndicatorNo_get()
	driver.ebler.pcc.pmi.ri.repcap_reliabilityIndicatorNo_set(repcap.ReliabilityIndicatorNo.RIno1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:PMI:RI<no>

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:PMI:RI<no>



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Pmi.Ri.RiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.pcc.pmi.ri.clone()