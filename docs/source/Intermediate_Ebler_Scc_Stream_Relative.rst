Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:STReam<Stream>:RELative

.. code-block:: python

	FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:STReam<Stream>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Scc.Stream.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: