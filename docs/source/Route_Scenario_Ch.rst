Ch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CH[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CH[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ch.ChCls
	:members:
	:undoc-members:
	:noindex: