Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fpri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: