Connection
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Prepare.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Connection_DedBearer.rst