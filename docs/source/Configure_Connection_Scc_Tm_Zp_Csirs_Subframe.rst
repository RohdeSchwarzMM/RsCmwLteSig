Subframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:ZP:CSIRs:SUBFrame

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:ZP:CSIRs:SUBFrame



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Zp.Csirs.Subframe.SubframeCls
	:members:
	:undoc-members:
	:noindex: