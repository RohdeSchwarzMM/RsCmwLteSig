InputPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:EATTenuation:INPut

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:EATTenuation:INPut



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Eattenuation.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: