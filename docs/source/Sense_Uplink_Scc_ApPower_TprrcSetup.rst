TprrcSetup
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.scc.apPower.tprrcSetup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Scc_ApPower_TprrcSetup_Basic.rst