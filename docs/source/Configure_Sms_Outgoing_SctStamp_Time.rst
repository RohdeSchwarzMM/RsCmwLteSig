Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TIME



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Outgoing.SctStamp.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: