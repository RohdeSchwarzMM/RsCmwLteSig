Cindex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting[:PCC]:CINDex[:FDD]
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting[:PCC]:CINDex:TDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting[:PCC]:CINDex[:FDD]
	CONFigure:LTE:SIGNaling<instance>:CQIReporting[:PCC]:CINDex:TDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.Pcc.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: