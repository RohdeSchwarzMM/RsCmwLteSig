Frequency
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.userDefined.frequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_UserDefined_Frequency_Downlink.rst
	Configure_RfSettings_Pcc_UserDefined_Frequency_Uplink.rst