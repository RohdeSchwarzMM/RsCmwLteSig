Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Sps.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: