Wcdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:WCDMa

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:WCDMa



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex: