Ncsacq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:UTRan

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:UTRan



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.Ncsacq.NcsacqCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.faueEutra.ncsacq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_FaueEutra_Ncsacq_Frequency.rst