Sexecute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SEXecute

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SEXecute



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Sexecute.SexecuteCls
	:members:
	:undoc-members:
	:noindex: