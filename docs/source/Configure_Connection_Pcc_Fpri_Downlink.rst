Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:DL:STTI
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:DL:STTI
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPRI:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fpri.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: