Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fwbcqi.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: