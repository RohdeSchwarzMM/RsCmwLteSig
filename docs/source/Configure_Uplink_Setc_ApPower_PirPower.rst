PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:PIRPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:PIRPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: