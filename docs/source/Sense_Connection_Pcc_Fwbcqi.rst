Fwbcqi
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fwbcqi.FwbcqiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.pcc.fwbcqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Pcc_Fwbcqi_Downlink.rst