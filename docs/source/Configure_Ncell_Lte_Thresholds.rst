Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:LTE:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Lte.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: