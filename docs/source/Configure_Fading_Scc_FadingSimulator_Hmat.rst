Hmat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Hmat.HmatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.scc.fadingSimulator.hmat.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Scc_FadingSimulator_Hmat_Mode.rst
	Configure_Fading_Scc_FadingSimulator_Hmat_Row.rst
	Configure_Fading_Scc_FadingSimulator_Hmat_Rst.rst