DmtcPeriod
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:CSAT:DMTCperiod

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:CSAT:DMTCperiod



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Csat.DmtcPeriod.DmtcPeriodCls
	:members:
	:undoc-members:
	:noindex: