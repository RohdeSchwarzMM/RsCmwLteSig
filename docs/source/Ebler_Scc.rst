Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.ebler.scc.repcap_secondaryCompCarrier_get()
	driver.ebler.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Scc_Absolute.rst
	Ebler_Scc_Confidence.rst
	Ebler_Scc_CqiReporting.rst
	Ebler_Scc_Harq.rst
	Ebler_Scc_Pmi.rst
	Ebler_Scc_Relative.rst
	Ebler_Scc_Ri.rst
	Ebler_Scc_Stream.rst
	Ebler_Scc_Uplink.rst