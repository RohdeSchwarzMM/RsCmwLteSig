Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:CHANnel:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:CHANnel:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Channel.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: