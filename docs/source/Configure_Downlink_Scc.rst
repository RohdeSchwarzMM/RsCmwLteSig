Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.downlink.scc.repcap_secondaryCompCarrier_get()
	driver.configure.downlink.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Awgn.rst
	Configure_Downlink_Scc_Csirs.rst
	Configure_Downlink_Scc_Ocng.rst
	Configure_Downlink_Scc_Pbch.rst
	Configure_Downlink_Scc_Pcfich.rst
	Configure_Downlink_Scc_Pdcch.rst
	Configure_Downlink_Scc_Pdsch.rst
	Configure_Downlink_Scc_Phich.rst
	Configure_Downlink_Scc_Power.rst
	Configure_Downlink_Scc_Pss.rst
	Configure_Downlink_Scc_Rsepre.rst
	Configure_Downlink_Scc_Sss.rst