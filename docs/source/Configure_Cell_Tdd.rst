Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TDD:SPECific

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TDD:SPECific



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: