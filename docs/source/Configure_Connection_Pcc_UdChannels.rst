UdChannels
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.UdChannelsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.udChannels.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_UdChannels_Downlink.rst
	Configure_Connection_Pcc_UdChannels_Emtc.rst
	Configure_Connection_Pcc_UdChannels_Mcluster.rst
	Configure_Connection_Pcc_UdChannels_Uplink.rst