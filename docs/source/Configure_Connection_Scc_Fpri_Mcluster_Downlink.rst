Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPRI:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPRI:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fpri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: