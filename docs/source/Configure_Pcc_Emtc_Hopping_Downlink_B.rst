B
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:B:INTerval

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:DL:B:INTerval



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Downlink.B.BCls
	:members:
	:undoc-members:
	:noindex: