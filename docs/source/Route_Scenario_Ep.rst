Ep
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:EP[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:EP[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ep.EpCls
	:members:
	:undoc-members:
	:noindex: