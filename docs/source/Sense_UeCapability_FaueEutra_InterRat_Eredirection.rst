Eredirection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:EREDirection:UTRA
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:EREDirection:UTDD

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:EREDirection:UTRA
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:EREDirection:UTDD



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.InterRat.Eredirection.EredirectionCls
	:members:
	:undoc-members:
	:noindex: