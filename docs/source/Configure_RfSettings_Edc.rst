Edc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:EDC:OUTPut
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:EDC:INPut

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:EDC:OUTPut
	CONFigure:LTE:SIGNaling<instance>:RFSettings:EDC:INPut



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Edc.EdcCls
	:members:
	:undoc-members:
	:noindex: