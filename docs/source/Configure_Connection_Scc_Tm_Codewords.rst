Codewords
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CODewords

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CODewords



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Codewords.CodewordsCls
	:members:
	:undoc-members:
	:noindex: