Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL[:PCC]
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL:ALL

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL[:PCC]
	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:UL:ALL



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Ethroughput_Uplink_Scc.rst