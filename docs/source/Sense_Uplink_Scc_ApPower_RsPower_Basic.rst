Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:RSPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:RSPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.RsPower.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: