Sinterval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:SINTerval[:DL]
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:SINTerval:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:SINTerval[:DL]
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:SINTerval:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Sps.Sinterval.SintervalCls
	:members:
	:undoc-members:
	:noindex: