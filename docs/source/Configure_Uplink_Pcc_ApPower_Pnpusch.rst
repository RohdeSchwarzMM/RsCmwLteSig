Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PNPusch:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PNPusch:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Pcc.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: