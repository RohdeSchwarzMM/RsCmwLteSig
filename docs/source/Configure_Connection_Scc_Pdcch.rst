Pdcch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.pdcch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Pdcch_Alevel.rst
	Configure_Connection_Scc_Pdcch_Symbol.rst