Flexible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:SCELl:FLEXible

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:SCELl:FLEXible



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Scell.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: