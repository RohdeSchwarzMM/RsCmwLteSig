Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:POWer

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:POWer



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Csirs.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: