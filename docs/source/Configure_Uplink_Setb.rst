Setb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:PMAX

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:PMAX



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.SetbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.setb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Setb_ApPower.rst
	Configure_Uplink_Setb_Pucch.rst
	Configure_Uplink_Setb_Pusch.rst