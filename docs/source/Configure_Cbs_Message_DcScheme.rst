DcScheme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DCSCheme
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DCSCheme:UCODed

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DCSCheme
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:DCSCheme:UCODed



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.DcScheme.DcSchemeCls
	:members:
	:undoc-members:
	:noindex: