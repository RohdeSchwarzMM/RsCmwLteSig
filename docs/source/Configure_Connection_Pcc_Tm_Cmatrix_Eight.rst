Eight<MatrixEightLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.connection.pcc.tm.cmatrix.eight.repcap_matrixEightLine_get()
	driver.configure.connection.pcc.tm.cmatrix.eight.repcap_matrixEightLine_set(repcap.MatrixEightLine.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CMATrix:EIGHt<line>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:TM<nr>:CMATrix:EIGHt<line>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Tm.Cmatrix.Eight.EightCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.tm.cmatrix.eight.clone()