Utdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:UTDD<n>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:UTDD<n>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.Utdd.UtddCls
	:members:
	:undoc-members:
	:noindex: