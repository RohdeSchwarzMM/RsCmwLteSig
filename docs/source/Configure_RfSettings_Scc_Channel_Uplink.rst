Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:CHANnel:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:CHANnel:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Channel.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: