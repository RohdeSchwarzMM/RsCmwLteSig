Gn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GN[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GN[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gn.GnCls
	:members:
	:undoc-members:
	:noindex: