Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:TDSCdma:CELL<nr>:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:TDSCdma:CELL<nr>:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Tdscdma.Cell.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: