Ej
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:EJ[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:EJ[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ej.EjCls
	:members:
	:undoc-members:
	:noindex: