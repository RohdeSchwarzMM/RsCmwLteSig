Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.scc.repcap_secondaryCompCarrier_get()
	driver.configure.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SCC:AMODe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SCC:AMODe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Scc_Band.rst
	Configure_Scc_Caggregation.rst
	Configure_Scc_Dmode.rst
	Configure_Scc_Fstructure.rst
	Configure_Scc_Uul.rst