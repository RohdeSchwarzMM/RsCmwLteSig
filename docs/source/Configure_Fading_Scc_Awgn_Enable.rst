Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: