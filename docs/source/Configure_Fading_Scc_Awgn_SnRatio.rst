SnRatio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:SNRatio

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:SNRatio



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.SnRatio.SnRatioCls
	:members:
	:undoc-members:
	:noindex: