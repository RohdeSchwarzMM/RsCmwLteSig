Hpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:HPUSch:ACTive

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:HPUSch:ACTive



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Hpusch.HpuschCls
	:members:
	:undoc-members:
	:noindex: