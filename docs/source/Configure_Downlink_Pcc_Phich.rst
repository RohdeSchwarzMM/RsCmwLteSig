Phich
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PHICh:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PHICh:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Phich.PhichCls
	:members:
	:undoc-members:
	:noindex: