OslSubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:OSLSubframe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:OSLSubframe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Fburst.OslSubframe.OslSubframeCls
	:members:
	:undoc-members:
	:noindex: