Snow
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SNOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:SNOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Time.Snow.SnowCls
	:members:
	:undoc-members:
	:noindex: