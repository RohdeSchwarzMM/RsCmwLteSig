Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:SUBFrame:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:SUBFrame:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Subframe.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: