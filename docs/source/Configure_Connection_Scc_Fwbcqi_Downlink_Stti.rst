Stti
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:STTI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:STTI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Downlink.Stti.SttiCls
	:members:
	:undoc-members:
	:noindex: