Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.intermediate.ebler.pcc.stream.repcap_stream_get()
	driver.intermediate.ebler.pcc.stream.repcap_stream_set(repcap.Stream.S1)





.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.Pcc.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.intermediate.ebler.pcc.stream.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Intermediate_Ebler_Pcc_Stream_Absolute.rst
	Intermediate_Ebler_Pcc_Stream_Relative.rst