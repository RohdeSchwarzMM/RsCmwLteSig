Col<HMatrixColumn>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.configure.fading.pcc.fadingSimulator.hmat.row.col.repcap_hMatrixColumn_get()
	driver.configure.fading.pcc.fadingSimulator.hmat.row.col.repcap_hMatrixColumn_set(repcap.HMatrixColumn.Nr1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Hmat.Row.Col.ColCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.fadingSimulator.hmat.row.col.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_FadingSimulator_Hmat_Row_Col_Imag.rst
	Configure_Fading_Pcc_FadingSimulator_Hmat_Row_Col_Real.rst