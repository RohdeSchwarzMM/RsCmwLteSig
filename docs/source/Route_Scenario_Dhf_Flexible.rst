Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DHF[:FLEXible][:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:DHF[:FLEXible]:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:DHF[:FLEXible][:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:DHF[:FLEXible]:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Dhf.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: