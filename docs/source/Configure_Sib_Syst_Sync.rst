Sync
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SIB<n>:SYST:SYNC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SIB<n>:SYST:SYNC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sib.Syst.Sync.SyncCls
	:members:
	:undoc-members:
	:noindex: