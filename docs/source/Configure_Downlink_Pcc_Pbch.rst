Pbch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PBCH:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PBCH:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Pbch.PbchCls
	:members:
	:undoc-members:
	:noindex: