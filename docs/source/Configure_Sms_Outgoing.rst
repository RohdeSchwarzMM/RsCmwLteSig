Outgoing
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:MESHandling
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:INTernal
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:BINary
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:PIDentifier
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:LHANdling

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:UDHeader
	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:MESHandling
	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:INTernal
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:BINary
	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:PIDentifier
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:DCODing
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:CGRoup
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:MCLass
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:OSADdress
	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:OADDress
	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:LHANdling



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Outgoing.OutgoingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.outgoing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing_File.rst
	Configure_Sms_Outgoing_SctStamp.rst