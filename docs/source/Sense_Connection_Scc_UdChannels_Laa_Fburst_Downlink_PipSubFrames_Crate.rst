Crate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:FBURst:DL<Stream>:PIPSubframes:CRATe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:LAA:FBURst:DL<Stream>:PIPSubframes:CRATe



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.Laa.Fburst.Downlink.PipSubFrames.Crate.CrateCls
	:members:
	:undoc-members:
	:noindex: