Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:EVDO:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Evdo.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: