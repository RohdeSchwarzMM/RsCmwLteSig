Connection
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Ethroughput.rst
	Sense_Connection_Pcc.rst
	Sense_Connection_Scc.rst