Fcpri
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcpri.FcpriCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.fcpri.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Fcpri_Downlink.rst
	Configure_Connection_Pcc_Fcpri_Mcluster.rst