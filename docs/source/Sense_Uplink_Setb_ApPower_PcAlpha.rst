PcAlpha
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PCALpha:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PCALpha:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex: