Fl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FL[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FL[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fl.FlCls
	:members:
	:undoc-members:
	:noindex: