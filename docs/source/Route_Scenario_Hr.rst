Hr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:HR[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:HR[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Hr.HrCls
	:members:
	:undoc-members:
	:noindex: