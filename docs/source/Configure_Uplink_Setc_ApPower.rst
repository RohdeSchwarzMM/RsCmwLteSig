ApPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:EASettings

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:EASettings



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.ApPower.ApPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.setc.apPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Setc_ApPower_PcAlpha.rst
	Configure_Uplink_Setc_ApPower_PirPower.rst
	Configure_Uplink_Setc_ApPower_Pnpusch.rst
	Configure_Uplink_Setc_ApPower_RsPower.rst
	Configure_Uplink_Setc_ApPower_TprrcSetup.rst