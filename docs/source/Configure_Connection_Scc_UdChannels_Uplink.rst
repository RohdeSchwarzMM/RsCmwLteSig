Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDCHannels:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: