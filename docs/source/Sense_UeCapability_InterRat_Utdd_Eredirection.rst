Eredirection
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Utdd.Eredirection.EredirectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.interRat.utdd.eredirection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_InterRat_Utdd_Eredirection_Utdd.rst