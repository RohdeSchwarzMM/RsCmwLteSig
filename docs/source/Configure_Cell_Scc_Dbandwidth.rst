Dbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:DBANdwidth

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:DBANdwidth



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Dbandwidth.DbandwidthCls
	:members:
	:undoc-members:
	:noindex: