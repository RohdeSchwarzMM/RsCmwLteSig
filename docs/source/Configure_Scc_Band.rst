Band
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:BAND

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:BAND



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Band.BandCls
	:members:
	:undoc-members:
	:noindex: