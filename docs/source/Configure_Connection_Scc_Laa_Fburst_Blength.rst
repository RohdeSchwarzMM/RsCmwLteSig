Blength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:BLENgth

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:FBURst:BLENgth



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Fburst.Blength.BlengthCls
	:members:
	:undoc-members:
	:noindex: