Roffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:ROFFset

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:ROFFset



.. autoclass:: RsCmwLteSig.Implementations.Sense.CqiReporting.Scc.Roffset.RoffsetCls
	:members:
	:undoc-members:
	:noindex: