DedBearer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:DEDBearer:SEParate
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:DEDBearer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:DEDBearer:SEParate
	SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:DEDBearer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UesInfo.UeAddress.DedBearer.DedBearerCls
	:members:
	:undoc-members:
	:noindex: