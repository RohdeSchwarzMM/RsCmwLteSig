Update
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SIB<n>:UPDate

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SIB<n>:UPDate



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sib.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: