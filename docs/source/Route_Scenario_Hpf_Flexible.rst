Flexible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:HPF[:FLEXible]:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:HPF[:FLEXible]:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Hpf.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: