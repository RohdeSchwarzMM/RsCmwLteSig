Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCSTable:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCSTable:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcri.Downlink.McsTable.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: