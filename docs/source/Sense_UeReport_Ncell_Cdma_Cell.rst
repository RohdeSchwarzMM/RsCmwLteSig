Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:CDMA:CELL<nr>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:CDMA:CELL<nr>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Cdma.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: