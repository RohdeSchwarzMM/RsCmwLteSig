Resource
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:RESource

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:RESource



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Csirs.Resource.ResourceCls
	:members:
	:undoc-members:
	:noindex: