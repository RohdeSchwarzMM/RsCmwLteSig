Last
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:ELOG:LAST

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:ELOG:LAST



.. autoclass:: RsCmwLteSig.Implementations.Sense.Elog.Last.LastCls
	:members:
	:undoc-members:
	:noindex: