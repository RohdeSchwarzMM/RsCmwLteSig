Single
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:PUSCh:TPC:SINGle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:PUSCh:TPC:SINGle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.Pusch.Tpc.Single.SingleCls
	:members:
	:undoc-members:
	:noindex: