Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:CAGGregation:SET

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:CAGGregation:SET



.. autoclass:: RsCmwLteSig.Implementations.Configure.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: