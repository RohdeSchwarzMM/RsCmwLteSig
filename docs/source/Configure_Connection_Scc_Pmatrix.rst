Pmatrix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PMATrix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PMATrix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Pmatrix.PmatrixCls
	:members:
	:undoc-members:
	:noindex: