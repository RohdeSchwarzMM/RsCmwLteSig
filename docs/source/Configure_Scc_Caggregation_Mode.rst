Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:SCC<Carrier>:CAGGregation:MODE

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:SCC<Carrier>:CAGGregation:MODE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Caggregation.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: