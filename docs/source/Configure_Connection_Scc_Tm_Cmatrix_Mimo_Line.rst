Line<MatrixLine>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Line1 .. Line4
	rc = driver.configure.connection.scc.tm.cmatrix.mimo.line.repcap_matrixLine_get()
	driver.configure.connection.scc.tm.cmatrix.mimo.line.repcap_matrixLine_set(repcap.MatrixLine.Line1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:MIMO<Mimo>:LINE<line>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CMATrix:MIMO<Mimo>:LINE<line>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Cmatrix.Mimo.Line.LineCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.tm.cmatrix.mimo.line.clone()