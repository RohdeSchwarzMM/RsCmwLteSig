LrMessage
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:SMS:INFO:LRMessage:RFLag

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:SMS:INFO:LRMessage:RFLag



.. autoclass:: RsCmwLteSig.Implementations.Sense.Sms.Info.LrMessage.LrMessageCls
	:members:
	:undoc-members:
	:noindex: