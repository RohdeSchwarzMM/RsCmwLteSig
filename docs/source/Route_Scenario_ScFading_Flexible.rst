Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:SCFading:FLEXible[:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:SCFading:FLEXible:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:SCFading:FLEXible[:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:SCFading:FLEXible:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.ScFading.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: