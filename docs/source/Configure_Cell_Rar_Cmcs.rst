Cmcs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:RAR:CMCS:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CELL:RAR:CMCS

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:RAR:CMCS:ENABle
	CONFigure:LTE:SIGNaling<instance>:CELL:RAR:CMCS



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Rar.Cmcs.CmcsCls
	:members:
	:undoc-members:
	:noindex: