Pmatrix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TM<nr>:PMATrix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TM<nr>:PMATrix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Pmatrix.PmatrixCls
	:members:
	:undoc-members:
	:noindex: