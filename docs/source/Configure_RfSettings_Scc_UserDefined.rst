UserDefined
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.scc.userDefined.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Scc_UserDefined_Bindicator.rst
	Configure_RfSettings_Scc_UserDefined_Channel.rst
	Configure_RfSettings_Scc_UserDefined_Frequency.rst
	Configure_RfSettings_Scc_UserDefined_UdSeparation.rst