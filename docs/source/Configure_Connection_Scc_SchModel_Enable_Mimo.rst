Mimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:ENABle:MIMO<Mimo>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:SCHModel:ENABle:MIMO<Mimo>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SchModel.Enable.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex: