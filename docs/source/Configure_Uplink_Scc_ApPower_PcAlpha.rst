PcAlpha
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.scc.apPower.pcAlpha.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Scc_ApPower_PcAlpha_Advanced.rst