Ad
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:AD[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:AD[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ad.AdCls
	:members:
	:undoc-members:
	:noindex: