DciFormat
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:DCIFormat

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:DCIFormat



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.DciFormat.DciFormatCls
	:members:
	:undoc-members:
	:noindex: