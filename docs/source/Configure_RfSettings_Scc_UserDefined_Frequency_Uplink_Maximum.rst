Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:FREQuency:UL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:FREQuency:UL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Frequency.Uplink.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: