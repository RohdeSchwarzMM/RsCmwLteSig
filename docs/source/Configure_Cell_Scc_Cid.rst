Cid
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Cid.CidCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.scc.cid.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Scc_Cid_Eutran.rst