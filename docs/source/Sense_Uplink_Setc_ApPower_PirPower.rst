PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PIRPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PIRPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: