Transmission
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Transmission.TransmissionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.scc.harq.stream.transmission.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Scc_Harq_Stream_Transmission_Absolute.rst
	Ebler_Scc_Harq_Stream_Transmission_Relative.rst