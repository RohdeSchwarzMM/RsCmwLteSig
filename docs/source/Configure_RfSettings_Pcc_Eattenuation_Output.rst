Output<Output>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Out1 .. Out4
	rc = driver.configure.rfSettings.pcc.eattenuation.output.repcap_output_get()
	driver.configure.rfSettings.pcc.eattenuation.output.repcap_output_set(repcap.Output.Out1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:EATTenuation:OUTPut<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:EATTenuation:OUTPut<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Eattenuation.Output.OutputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.eattenuation.output.clone()