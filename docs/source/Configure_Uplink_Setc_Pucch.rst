Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUCCh:CLTPower

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:PUCCh:CLTPower



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex: