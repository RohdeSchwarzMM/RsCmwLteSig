Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:SUBFrame:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:SUBFrame:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Harq.Stream.Subframe.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: