Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:PUCCh:CLTPower

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:PUCCh:CLTPower



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex: