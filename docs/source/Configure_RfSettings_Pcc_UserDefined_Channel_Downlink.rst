Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:DL:MINimum
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:DL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:DL:MINimum
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:DL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.Channel.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: