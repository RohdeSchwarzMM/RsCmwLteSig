Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex:TDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex:TDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.Scc.Cindex.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: