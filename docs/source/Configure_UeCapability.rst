UeCapability
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UECapability:RUTRa
	single: CONFigure:LTE:SIGNaling<instance>:UECapability:RGCS
	single: CONFigure:LTE:SIGNaling<instance>:UECapability:RGPS
	single: CONFigure:LTE:SIGNaling<instance>:UECapability:RRFormat
	single: CONFigure:LTE:SIGNaling<instance>:UECapability:SFC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UECapability:RUTRa
	CONFigure:LTE:SIGNaling<instance>:UECapability:RGCS
	CONFigure:LTE:SIGNaling<instance>:UECapability:RGPS
	CONFigure:LTE:SIGNaling<instance>:UECapability:RRFormat
	CONFigure:LTE:SIGNaling<instance>:UECapability:SFC



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeCapability_RfBands.rst