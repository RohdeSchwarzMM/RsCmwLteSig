Bh
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:BH[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:BH[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Bh.BhCls
	:members:
	:undoc-members:
	:noindex: