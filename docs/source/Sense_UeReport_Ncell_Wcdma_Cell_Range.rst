Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:WCDMa:CELL<nr>:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:WCDMa:CELL<nr>:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Wcdma.Cell.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: