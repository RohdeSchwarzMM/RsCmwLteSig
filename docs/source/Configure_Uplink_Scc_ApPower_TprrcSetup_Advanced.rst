Advanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:TPRRcsetup:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:TPRRcsetup:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.TprrcSetup.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex: