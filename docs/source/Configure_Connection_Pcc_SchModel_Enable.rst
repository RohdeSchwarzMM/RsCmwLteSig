Enable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:ENABle:MIMO<Mimo>
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:ENABle:MIMO<Mimo>
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.SchModel.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: