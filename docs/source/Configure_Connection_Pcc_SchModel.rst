SchModel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SCHModel



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.SchModel.SchModelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.schModel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_SchModel_Enable.rst
	Configure_Connection_Pcc_SchModel_Mimo.rst
	Configure_Connection_Pcc_SchModel_Mselection.rst