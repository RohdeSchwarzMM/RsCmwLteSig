Geran
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:GERan:SUPPorted
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:GERan:PHGeran

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:GERan:SUPPorted
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:GERan:PHGeran



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.InterRat.Geran.GeranCls
	:members:
	:undoc-members:
	:noindex: