Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.sense.ueReport.scc.repcap_secondaryCompCarrier_get()
	driver.sense.ueReport.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Scc_Cocc.rst
	Sense_UeReport_Scc_Rresult.rst
	Sense_UeReport_Scc_Rsrp.rst
	Sense_UeReport_Scc_Rsrq.rst
	Sense_UeReport_Scc_Scell.rst