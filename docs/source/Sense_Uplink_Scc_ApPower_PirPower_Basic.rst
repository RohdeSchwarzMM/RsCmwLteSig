Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PIRPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PIRPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.PirPower.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: