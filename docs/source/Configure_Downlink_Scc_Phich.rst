Phich
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Phich.PhichCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.phich.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Phich_Poffset.rst