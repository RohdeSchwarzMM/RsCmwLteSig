PepSubFrames
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Laa.Rburst.PepSubFrames.PepSubFramesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udChannels.laa.rburst.pepSubFrames.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdChannels_Laa_Rburst_PepSubFrames_Downlink.rst
	Configure_Connection_Scc_UdChannels_Laa_Rburst_PepSubFrames_Mcluster.rst