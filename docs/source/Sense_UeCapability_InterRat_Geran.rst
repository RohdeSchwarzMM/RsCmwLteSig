Geran
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:SUPPorted
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:PHGeran
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:EREDirection
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:DTM

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:SUPPorted
	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:PHGeran
	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:EREDirection
	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:GERan:DTM



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Geran.GeranCls
	:members:
	:undoc-members:
	:noindex: