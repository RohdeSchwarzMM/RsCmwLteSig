Csirs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fwbcqi.Downlink.McsTable.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.pcc.fwbcqi.downlink.mcsTable.csirs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Pcc_Fwbcqi_Downlink_McsTable_Csirs_Determined.rst