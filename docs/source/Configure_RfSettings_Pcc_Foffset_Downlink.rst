Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:DL:UCSPecific
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:DL:UCSPecific
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:FOFFset:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Foffset.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: