Qam
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:UDTTibased:QAM<256>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:UDTTibased:QAM<256>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdttiBased.Qam.QamCls
	:members:
	:undoc-members:
	:noindex: