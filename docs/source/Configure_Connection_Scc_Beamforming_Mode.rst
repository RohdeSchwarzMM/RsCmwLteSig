Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:BEAMforming:MODE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:BEAMforming:MODE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Beamforming.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: