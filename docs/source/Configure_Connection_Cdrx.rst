Cdrx
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ODTimer
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ITIMer
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:RTIMer
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:LDCYcle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SOFFset
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SCENable
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SDCYcle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SCTimer

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ENABle
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ODTimer
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:ITIMer
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:RTIMer
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:LDCYcle
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SOFFset
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SCENable
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SDCYcle
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:SCTimer



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Cdrx.CdrxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cdrx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cdrx_Imode.rst