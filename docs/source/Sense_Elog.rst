Elog
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Elog.ElogCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.elog.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Elog_All.rst
	Sense_Elog_Last.rst