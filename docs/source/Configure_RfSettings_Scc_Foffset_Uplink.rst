Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:FOFFset:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:FOFFset:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Foffset.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: