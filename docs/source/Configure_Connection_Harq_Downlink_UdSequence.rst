UdSequence
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:UDSequence:LENGth
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:UDSequence

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:UDSequence:LENGth
	CONFigure:LTE:SIGNaling<instance>:CONNection:HARQ:DL:UDSequence



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Harq.Downlink.UdSequence.UdSequenceCls
	:members:
	:undoc-members:
	:noindex: