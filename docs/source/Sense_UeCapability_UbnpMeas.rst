UbnpMeas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:UBNPmeas:LMIDle
	single: SENSe:LTE:SIGNaling<instance>:UECapability:UBNPmeas:SGLocation

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:UBNPmeas:LMIDle
	SENSe:LTE:SIGNaling<instance>:UECapability:UBNPmeas:SGLocation



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.UbnpMeas.UbnpMeasCls
	:members:
	:undoc-members:
	:noindex: