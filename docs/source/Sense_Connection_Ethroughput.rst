Ethroughput
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.EthroughputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Ethroughput_Downlink.rst
	Sense_Connection_Ethroughput_Uplink.rst