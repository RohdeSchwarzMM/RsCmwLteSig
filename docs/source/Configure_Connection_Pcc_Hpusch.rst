Hpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:HPUSch:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:HPUSch:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Hpusch.HpuschCls
	:members:
	:undoc-members:
	:noindex: