Advanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:RSPower:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:RSPower:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.RsPower.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex: