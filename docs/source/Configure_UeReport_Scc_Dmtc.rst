Dmtc
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Dmtc.DmtcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ueReport.scc.dmtc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UeReport_Scc_Dmtc_Period.rst
	Configure_UeReport_Scc_Dmtc_Poffset.rst