Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Pcc.Srs.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: