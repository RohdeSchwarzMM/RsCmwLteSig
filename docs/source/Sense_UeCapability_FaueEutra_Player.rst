Player
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:UTASupported
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:USRSsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:TAPPsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:TWEFsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:PDSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:CCSSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:SPPSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:MCPCsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:NURClist

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:UTASupported
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:USRSsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:TAPPsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:TWEFsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:PDSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:CCSSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:SPPSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:MCPCsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:PLAYer:NURClist



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.Player.PlayerCls
	:members:
	:undoc-members:
	:noindex: