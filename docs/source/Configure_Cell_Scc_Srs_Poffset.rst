Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: