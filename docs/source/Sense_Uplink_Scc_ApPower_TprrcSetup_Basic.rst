Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:TPRRcsetup:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:TPRRcsetup:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.TprrcSetup.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: