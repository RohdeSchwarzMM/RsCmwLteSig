Cj
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CJ[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CJ[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cj.CjCls
	:members:
	:undoc-members:
	:noindex: