Ports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<carrier>:POWer:PORTs

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<carrier>:POWer:PORTs



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Power.Ports.PortsCls
	:members:
	:undoc-members:
	:noindex: