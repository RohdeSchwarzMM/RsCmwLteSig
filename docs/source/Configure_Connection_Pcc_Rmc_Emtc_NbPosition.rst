NbPosition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:NBPosition:UL
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:NBPosition:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:NBPosition:UL
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:EMTC:NBPosition:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Rmc.Emtc.NbPosition.NbPositionCls
	:members:
	:undoc-members:
	:noindex: