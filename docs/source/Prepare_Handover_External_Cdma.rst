Cdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:CDMA

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:CDMA



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Cdma.CdmaCls
	:members:
	:undoc-members:
	:noindex: