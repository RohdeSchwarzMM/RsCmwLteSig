IrnGaps
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.IrnGapsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.meas.irnGaps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Meas_IrnGaps_Chrpd.rst
	Sense_UeCapability_Meas_IrnGaps_Cxrtt.rst
	Sense_UeCapability_Meas_IrnGaps_Geran.rst
	Sense_UeCapability_Meas_IrnGaps_Ufdd.rst
	Sense_UeCapability_Meas_IrnGaps_Utdd.rst
	Sense_UeCapability_Meas_IrnGaps_V.rst