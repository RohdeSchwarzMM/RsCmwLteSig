Channel
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:CHANnel:DL
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:CHANnel:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:CHANnel:DL
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:CHANnel:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: