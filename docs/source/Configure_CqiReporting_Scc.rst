Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.cqiReporting.scc.repcap_secondaryCompCarrier_get()
	driver.configure.cqiReporting.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cqiReporting.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_CqiReporting_Scc_Cindex.rst