Dmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:DMODe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:DMODe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Dmode.DmodeCls
	:members:
	:undoc-members:
	:noindex: