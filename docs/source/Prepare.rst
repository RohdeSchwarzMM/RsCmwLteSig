Prepare
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Prepare.PrepareCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prepare.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prepare_Connection.rst
	Prepare_Handover.rst