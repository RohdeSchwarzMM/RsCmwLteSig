Rperiod
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:RPERiod

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:RPERiod



.. autoclass:: RsCmwLteSig.Implementations.Sense.CqiReporting.Scc.Rperiod.RperiodCls
	:members:
	:undoc-members:
	:noindex: