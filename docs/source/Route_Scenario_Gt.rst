Gt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GT[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GT[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Gt.GtCls
	:members:
	:undoc-members:
	:noindex: