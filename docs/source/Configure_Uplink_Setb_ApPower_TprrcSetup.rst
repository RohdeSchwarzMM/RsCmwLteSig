TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:TPRRcsetup:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:APPower:TPRRcsetup:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: