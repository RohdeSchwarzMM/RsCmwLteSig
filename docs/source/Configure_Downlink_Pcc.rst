Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:OCNG
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:AWGN

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:OCNG
	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:AWGN



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.pcc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Pcc_Csirs.rst
	Configure_Downlink_Pcc_Pbch.rst
	Configure_Downlink_Pcc_Pcfich.rst
	Configure_Downlink_Pcc_Pdcch.rst
	Configure_Downlink_Pcc_Pdsch.rst
	Configure_Downlink_Pcc_Phich.rst
	Configure_Downlink_Pcc_Power.rst
	Configure_Downlink_Pcc_Pss.rst
	Configure_Downlink_Pcc_Rsepre.rst
	Configure_Downlink_Pcc_Sss.rst