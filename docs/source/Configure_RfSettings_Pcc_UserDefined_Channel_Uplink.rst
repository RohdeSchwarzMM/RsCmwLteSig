Uplink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:UL:MINimum
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:UL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:UL:MINimum
	CONFigure:LTE:SIGNaling<instance>:RFSettings[:PCC]:UDEFined:CHANnel:UL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.UserDefined.Channel.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: