BwConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:BWConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<carrier>:SRS:BWConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Srs.BwConfig.BwConfigCls
	:members:
	:undoc-members:
	:noindex: