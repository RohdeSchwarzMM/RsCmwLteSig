Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.uplink.scc.repcap_secondaryCompCarrier_get()
	driver.configure.uplink.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Scc_ApPower.rst
	Configure_Uplink_Scc_Pmax.rst
	Configure_Uplink_Scc_Pmcc.rst
	Configure_Uplink_Scc_Pucch.rst
	Configure_Uplink_Scc_Pusch.rst