Csirs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:CSIRs:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:CSIRs:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fwbcqi.Downlink.McsTable.Csirs.CsirsCls
	:members:
	:undoc-members:
	:noindex: