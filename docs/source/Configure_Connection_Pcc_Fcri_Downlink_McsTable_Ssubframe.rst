Ssubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCSTable:SSUBframe:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:DL:MCSTable:SSUBframe:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcri.Downlink.McsTable.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex: