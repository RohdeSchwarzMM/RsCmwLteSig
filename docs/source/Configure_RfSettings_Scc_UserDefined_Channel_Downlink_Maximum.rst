Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:DL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:DL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Channel.Downlink.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: