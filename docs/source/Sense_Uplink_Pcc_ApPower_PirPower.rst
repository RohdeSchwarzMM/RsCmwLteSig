PirPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PIRPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PIRPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Pcc.ApPower.PirPower.PirPowerCls
	:members:
	:undoc-members:
	:noindex: