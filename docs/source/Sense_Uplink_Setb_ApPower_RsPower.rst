RsPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:RSPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:RSPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.RsPower.RsPowerCls
	:members:
	:undoc-members:
	:noindex: