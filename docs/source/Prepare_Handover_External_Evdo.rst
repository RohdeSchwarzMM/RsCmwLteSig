Evdo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:EVDO

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:EVDO



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Evdo.EvdoCls
	:members:
	:undoc-members:
	:noindex: