FadingSimulator
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:KCONstant
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:PROFile

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:KCONstant
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ENABle
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:PROFile



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.FadingSimulatorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.fadingSimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_FadingSimulator_Bypass.rst
	Configure_Fading_Pcc_FadingSimulator_Dshift.rst
	Configure_Fading_Pcc_FadingSimulator_Globale.rst
	Configure_Fading_Pcc_FadingSimulator_Hmat.rst
	Configure_Fading_Pcc_FadingSimulator_Iloss.rst
	Configure_Fading_Pcc_FadingSimulator_Matrix.rst
	Configure_Fading_Pcc_FadingSimulator_Restart.rst
	Configure_Fading_Pcc_FadingSimulator_Standard.rst