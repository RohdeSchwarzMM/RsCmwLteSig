NbPosition
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:NBPosition:DL
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:NBPosition:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:NBPosition:DL
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:NBPosition:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Emtc.NbPosition.NbPositionCls
	:members:
	:undoc-members:
	:noindex: