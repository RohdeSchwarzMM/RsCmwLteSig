Fp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FP[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FP[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fp.FpCls
	:members:
	:undoc-members:
	:noindex: