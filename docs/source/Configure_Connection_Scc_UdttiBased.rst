UdttiBased
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdttiBased.UdttiBasedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udttiBased.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdttiBased_Downlink.rst
	Configure_Connection_Scc_UdttiBased_Qam.rst
	Configure_Connection_Scc_UdttiBased_Uplink.rst