Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:TRANsmission:ABSolute

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:TRANsmission:ABSolute



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Harq.Stream.Transmission.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: