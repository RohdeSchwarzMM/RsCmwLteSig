Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:BANDwidth:SCC<Carrier>:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:BANDwidth:SCC<Carrier>:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Bandwidth.Scc.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: