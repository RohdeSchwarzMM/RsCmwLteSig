Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PCALpha:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PCALpha:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Scc.ApPower.PcAlpha.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: