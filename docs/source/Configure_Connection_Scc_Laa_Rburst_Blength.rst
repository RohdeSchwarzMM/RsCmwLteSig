Blength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:BLENgth

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:BLENgth



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.Blength.BlengthCls
	:members:
	:undoc-members:
	:noindex: