B
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Emtc.B.BCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.udChannels.emtc.b.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_UdChannels_Emtc_B_Downlink.rst
	Configure_Connection_Pcc_UdChannels_Emtc_B_Uplink.rst