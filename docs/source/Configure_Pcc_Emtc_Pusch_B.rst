B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:B:CERepetition
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:B:MRCE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:B:CERepetition
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:B:MRCE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pusch.B.BCls
	:members:
	:undoc-members:
	:noindex: