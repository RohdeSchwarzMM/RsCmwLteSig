Sps
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:TIConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:SPS:TIConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Sps.SpsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.sps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Sps_Downlink.rst
	Configure_Connection_Pcc_Sps_Sinterval.rst
	Configure_Connection_Pcc_Sps_Uplink.rst