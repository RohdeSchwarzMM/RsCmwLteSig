A
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:A:INTerval

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:A:INTerval



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Uplink.A.ACls
	:members:
	:undoc-members:
	:noindex: