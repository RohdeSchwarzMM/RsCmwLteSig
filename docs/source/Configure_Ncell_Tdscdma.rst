Tdscdma
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Tdscdma.TdscdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ncell.tdscdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ncell_Tdscdma_Cell.rst
	Configure_Ncell_Tdscdma_Thresholds.rst