Downlink<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.sense.connection.scc.udChannels.laa.rburst.downlink.repcap_stream_get()
	driver.sense.connection.scc.udChannels.laa.rburst.downlink.repcap_stream_set(repcap.Stream.S1)





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.Laa.Rburst.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.udChannels.laa.rburst.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_UdChannels_Laa_Rburst_Downlink_FullSubFrames.rst
	Sense_Connection_Scc_UdChannels_Laa_Rburst_Downlink_PepSubFrames.rst
	Sense_Connection_Scc_UdChannels_Laa_Rburst_Downlink_PipSubFrames.rst