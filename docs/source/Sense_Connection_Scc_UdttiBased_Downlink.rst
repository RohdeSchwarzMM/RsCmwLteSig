Downlink<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.sense.connection.scc.udttiBased.downlink.repcap_stream_get()
	driver.sense.connection.scc.udttiBased.downlink.repcap_stream_set(repcap.Stream.S1)





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdttiBased.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.udttiBased.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_UdttiBased_Downlink_Crate.rst