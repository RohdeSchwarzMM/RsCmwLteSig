OnsDuration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:ONSDuration

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:ONSDuration



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.ScMuting.OnsDuration.OnsDurationCls
	:members:
	:undoc-members:
	:noindex: