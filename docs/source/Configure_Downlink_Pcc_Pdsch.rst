Pdsch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDSCh:PA
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDSCh:RINDex

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDSCh:PA
	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDSCh:RINDex



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Pdsch.PdschCls
	:members:
	:undoc-members:
	:noindex: