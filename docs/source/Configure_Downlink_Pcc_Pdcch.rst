Pdcch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDCCh:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL[:PCC]:PDCCh:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Pcc.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex: