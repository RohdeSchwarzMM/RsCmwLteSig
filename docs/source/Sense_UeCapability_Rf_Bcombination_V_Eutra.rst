Eutra<EutraBand>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Band1 .. Band4
	rc = driver.sense.ueCapability.rf.bcombination.v.eutra.repcap_eutraBand_get()
	driver.sense.ueCapability.rf.bcombination.v.eutra.repcap_eutraBand_set(repcap.EutraBand.Band1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rf.bcombination.v.eutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Rf_Bcombination_V_Eutra_Bclass.rst
	Sense_UeCapability_Rf_Bcombination_V_Eutra_Mcapability.rst
	Sense_UeCapability_Rf_Bcombination_V_Eutra_Scproc.rst