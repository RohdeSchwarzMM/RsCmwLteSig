Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:HBANdwidth
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:DBANdwidth
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:BWConfig
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:ENABle
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:MCENable
	single: CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:SFConfig
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:DCONfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:HBANdwidth
	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:DBANdwidth
	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:BWConfig
	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:ENABle
	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:MCENable
	CONFigure:LTE:SIGNaling<Instance>:CELL[:PCC]:SRS:SFConfig
	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SRS:DCONfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Pcc.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.pcc.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Pcc_Srs_Poffset.rst
	Configure_Cell_Pcc_Srs_ScIndex.rst