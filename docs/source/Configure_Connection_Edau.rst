Edau
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:NSEGment
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:NID

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:ENABle
	CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:NSEGment
	CONFigure:LTE:SIGNaling<instance>:CONNection:EDAU:NID



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Edau.EdauCls
	:members:
	:undoc-members:
	:noindex: