Security
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:AUTHenticat
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:NAS
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:AS
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:IALGorithm
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:NCALgorithm
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:MILenage
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:SKEY
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:OPC
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:RVALue

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:AUTHenticat
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:NAS
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:AS
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:IALGorithm
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:NCALgorithm
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:MILenage
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:SKEY
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:OPC
	CONFigure:LTE:SIGNaling<instance>:CELL:SECurity:RVALue



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Security.SecurityCls
	:members:
	:undoc-members:
	:noindex: