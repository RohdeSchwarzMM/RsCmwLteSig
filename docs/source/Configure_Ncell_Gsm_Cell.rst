Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:CELL<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:GSM:CELL<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Gsm.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: