Wiw
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:WIW:WIAPolicies
	single: SENSe:LTE:SIGNaling<instance>:UECapability:WIW:WIRRules

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:WIW:WIAPolicies
	SENSe:LTE:SIGNaling<instance>:UECapability:WIW:WIRRules



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Wiw.WiwCls
	:members:
	:undoc-members:
	:noindex: