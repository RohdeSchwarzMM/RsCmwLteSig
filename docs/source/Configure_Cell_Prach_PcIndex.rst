PcIndex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PCINdex:FDD
	single: CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PCINdex:TDD

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PCINdex:FDD
	CONFigure:LTE:SIGNaling<instance>:CELL:PRACh:PCINdex:TDD



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Prach.PcIndex.PcIndexCls
	:members:
	:undoc-members:
	:noindex: