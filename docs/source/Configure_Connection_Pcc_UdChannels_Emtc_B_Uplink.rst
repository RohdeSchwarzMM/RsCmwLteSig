Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:B:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:B:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Emtc.B.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: