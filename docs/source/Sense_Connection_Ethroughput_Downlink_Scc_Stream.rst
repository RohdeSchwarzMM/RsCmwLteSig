Stream<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.sense.connection.ethroughput.downlink.scc.stream.repcap_stream_get()
	driver.sense.connection.ethroughput.downlink.scc.stream.repcap_stream_set(repcap.Stream.S1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:SCC<Carrier>:STReam<Stream>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:SCC<Carrier>:STReam<Stream>



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.Downlink.Scc.Stream.StreamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.downlink.scc.stream.clone()