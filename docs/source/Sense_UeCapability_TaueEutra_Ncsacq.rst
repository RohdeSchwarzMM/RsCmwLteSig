Ncsacq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:UTRan

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:TAUeeutra:NCSacq:UTRan



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.Ncsacq.NcsacqCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.taueEutra.ncsacq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_TaueEutra_Ncsacq_Frequency.rst