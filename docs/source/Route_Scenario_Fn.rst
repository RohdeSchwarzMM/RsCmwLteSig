Fn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FN[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FN[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fn.FnCls
	:members:
	:undoc-members:
	:noindex: