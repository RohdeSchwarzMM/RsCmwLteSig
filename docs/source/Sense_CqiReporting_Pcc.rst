Pcc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CQIReporting[:PCC]:RPERiod
	single: SENSe:LTE:SIGNaling<instance>:CQIReporting[:PCC]:ROFFset

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CQIReporting[:PCC]:RPERiod
	SENSe:LTE:SIGNaling<instance>:CQIReporting[:PCC]:ROFFset



.. autoclass:: RsCmwLteSig.Implementations.Sense.CqiReporting.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: