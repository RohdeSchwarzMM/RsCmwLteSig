Downlink<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.configure.connection.pcc.rmc.version.downlink.repcap_stream_get()
	driver.configure.connection.pcc.rmc.version.downlink.repcap_stream_set(repcap.Stream.S1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:VERSion:DL<Stream>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:RMC:VERSion:DL<Stream>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Rmc.Version.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.rmc.version.downlink.clone()