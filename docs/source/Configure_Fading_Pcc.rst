Pcc
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_Awgn.rst
	Configure_Fading_Pcc_FadingSimulator.rst
	Configure_Fading_Pcc_Power.rst