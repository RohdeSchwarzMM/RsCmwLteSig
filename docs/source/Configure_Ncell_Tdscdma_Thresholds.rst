Thresholds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:THResholds
	single: CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:THResholds:LOW

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:THResholds
	CONFigure:LTE:SIGNaling<instance>:NCELl:TDSCdma:THResholds:LOW



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ncell.Tdscdma.Thresholds.ThresholdsCls
	:members:
	:undoc-members:
	:noindex: