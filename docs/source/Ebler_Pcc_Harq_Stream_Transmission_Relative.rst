Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:TRANsmission:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer[:PCC]:HARQ:STReam<Stream>:TRANsmission:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Pcc.Harq.Stream.Transmission.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: