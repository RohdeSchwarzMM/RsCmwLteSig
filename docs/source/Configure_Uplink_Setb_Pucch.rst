Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETB:PUCCh:CLTPower

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETB:PUCCh:CLTPower



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setb.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex: