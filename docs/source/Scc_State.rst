State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:SCC<Carrier>:STATe

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:SCC<Carrier>:STATe



.. autoclass:: RsCmwLteSig.Implementations.Scc.State.StateCls
	:members:
	:undoc-members:
	:noindex: