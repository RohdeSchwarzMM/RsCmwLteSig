Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:SSUBframe:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:SSUBframe:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fwbcqi.Downlink.McsTable.Ssubframe.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: