Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:AVERage
	single: READ:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:AVERage

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:AVERage
	READ:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:AVERage



.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Uplink.Pdu.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: