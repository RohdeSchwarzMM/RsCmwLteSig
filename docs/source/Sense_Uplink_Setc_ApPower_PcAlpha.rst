PcAlpha
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PCALpha:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PCALpha:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex: