Tdscdma
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Tdscdma.TdscdmaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.ncell.tdscdma.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell_Tdscdma_Cell.rst