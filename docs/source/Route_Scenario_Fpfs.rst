Fpfs
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fpfs.FpfsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.fpfs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Fpfs_Flexible.rst