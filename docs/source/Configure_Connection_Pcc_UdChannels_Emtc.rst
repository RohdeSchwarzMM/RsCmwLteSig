Emtc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:SFPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:SFPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Emtc.EmtcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.udChannels.emtc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_UdChannels_Emtc_A.rst
	Configure_Connection_Pcc_UdChannels_Emtc_B.rst
	Configure_Connection_Pcc_UdChannels_Emtc_NbPosition.rst