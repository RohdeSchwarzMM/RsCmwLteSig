Profile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:STANdard:PROFile

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:STANdard:PROFile



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Standard.Profile.ProfileCls
	:members:
	:undoc-members:
	:noindex: