Hxsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:HXSM<MIMO4x4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:HXSM<MIMO4x4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Hxsm.HxsmCls
	:members:
	:undoc-members:
	:noindex: