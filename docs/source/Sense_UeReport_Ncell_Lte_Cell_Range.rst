Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:LTE:CELL<nr>:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:NCELl:LTE:CELL<nr>:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Ncell.Lte.Cell.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: