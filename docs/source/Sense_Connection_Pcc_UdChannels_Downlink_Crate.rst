Crate
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.UdChannels.Downlink.Crate.CrateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.pcc.udChannels.downlink.crate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Pcc_UdChannels_Downlink_Crate_All.rst