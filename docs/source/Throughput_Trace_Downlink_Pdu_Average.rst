Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:AVERage
	single: READ:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:AVERage

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:AVERage
	READ:LTE:SIGNaling<instance>:THRoughput:TRACe:DL:PDU:AVERage



.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Downlink.Pdu.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: