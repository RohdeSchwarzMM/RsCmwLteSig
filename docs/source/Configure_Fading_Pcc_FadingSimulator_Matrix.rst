Matrix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:MATRix:MODE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:MATRix:MODE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Matrix.MatrixCls
	:members:
	:undoc-members:
	:noindex: