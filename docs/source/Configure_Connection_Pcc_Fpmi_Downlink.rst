Downlink
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:DL:STTI
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:DL:STTI
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fpmi.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: