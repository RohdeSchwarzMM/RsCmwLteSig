Fdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex[:FDD]

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting:SCC<Carrier>:CINDex[:FDD]



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.Scc.Cindex.Fdd.FddCls
	:members:
	:undoc-members:
	:noindex: