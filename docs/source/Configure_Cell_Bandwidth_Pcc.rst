Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:BANDwidth[:PCC]:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:BANDwidth[:PCC]:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Bandwidth.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: