MmrRepetition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:MMRRepetitio

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:MMRRepetitio



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.Level.Prach.MmrRepetition.MmrRepetitionCls
	:members:
	:undoc-members:
	:noindex: