Tpc
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pusch.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.uplink.scc.pusch.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Uplink_Scc_Pusch_Tpc_CltPower.rst
	Configure_Uplink_Scc_Pusch_Tpc_Pexecute.rst
	Configure_Uplink_Scc_Pusch_Tpc_RpControl.rst
	Configure_Uplink_Scc_Pusch_Tpc_Set.rst
	Configure_Uplink_Scc_Pusch_Tpc_Single.rst
	Configure_Uplink_Scc_Pusch_Tpc_Tpower.rst
	Configure_Uplink_Scc_Pusch_Tpc_UdPattern.rst