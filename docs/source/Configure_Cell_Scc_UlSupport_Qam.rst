Qam<QAMmodulationOrder>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: QAM64 .. QAM256
	rc = driver.configure.cell.scc.ulSupport.qam.repcap_qAMmodulationOrder_get()
	driver.configure.cell.scc.ulSupport.qam.repcap_qAMmodulationOrder_set(repcap.QAMmodulationOrder.QAM64)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.UlSupport.Qam.QamCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.scc.ulSupport.qam.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Scc_UlSupport_Qam_Enable.rst