Sl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSLSs
	single: SENSe:LTE:SIGNaling<instance>:UECapability:SL:CSTX
	single: SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSRalloc
	single: SENSe:LTE:SIGNaling<instance>:UECapability:SL:DUSRalloc
	single: SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSPRoc

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSLSs
	SENSe:LTE:SIGNaling<instance>:UECapability:SL:CSTX
	SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSRalloc
	SENSe:LTE:SIGNaling<instance>:UECapability:SL:DUSRalloc
	SENSe:LTE:SIGNaling<instance>:UECapability:SL:DSPRoc



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Sl.SlCls
	:members:
	:undoc-members:
	:noindex: