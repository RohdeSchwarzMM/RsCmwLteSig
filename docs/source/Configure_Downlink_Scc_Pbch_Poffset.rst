Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PBCH:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PBCH:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pbch.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: