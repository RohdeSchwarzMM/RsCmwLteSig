A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:A:CERepetition
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:A:MRCE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:A:CERepetition
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:PUSCh:A:MRCE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Pusch.A.ACls
	:members:
	:undoc-members:
	:noindex: