Stti
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:STTI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCPRi:DL:STTI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcpri.Downlink.Stti.SttiCls
	:members:
	:undoc-members:
	:noindex: