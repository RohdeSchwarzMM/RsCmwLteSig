Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.rfSettings.scc.repcap_secondaryCompCarrier_get()
	driver.configure.rfSettings.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Scc_Channel.rst
	Configure_RfSettings_Scc_Eattenuation.rst
	Configure_RfSettings_Scc_EnpMode.rst
	Configure_RfSettings_Scc_EnvelopePower.rst
	Configure_RfSettings_Scc_Foffset.rst
	Configure_RfSettings_Scc_MixerLevelOffset.rst
	Configure_RfSettings_Scc_UdSeparation.rst
	Configure_RfSettings_Scc_Umargin.rst
	Configure_RfSettings_Scc_UserDefined.rst