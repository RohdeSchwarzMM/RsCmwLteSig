McsTable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fwbcqi.Downlink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.fwbcqi.downlink.mcsTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Fwbcqi_Downlink_McsTable_Csirs.rst
	Configure_Connection_Pcc_Fwbcqi_Downlink_McsTable_Ssubframe.rst