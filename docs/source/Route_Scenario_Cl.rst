Cl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CL[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CL[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cl.ClCls
	:members:
	:undoc-members:
	:noindex: