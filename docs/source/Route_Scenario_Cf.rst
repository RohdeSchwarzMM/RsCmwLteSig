Cf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CF[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CF[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cf.CfCls
	:members:
	:undoc-members:
	:noindex: