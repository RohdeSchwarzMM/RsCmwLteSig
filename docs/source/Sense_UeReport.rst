UeReport
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.UeReportCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueReport.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeReport_Ncell.rst
	Sense_UeReport_Pcc.rst
	Sense_UeReport_Scc.rst