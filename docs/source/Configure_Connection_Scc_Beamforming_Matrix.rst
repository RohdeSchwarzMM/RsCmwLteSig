Matrix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:BEAMforming:MATRix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:BEAMforming:MATRix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Beamforming.Matrix.MatrixCls
	:members:
	:undoc-members:
	:noindex: