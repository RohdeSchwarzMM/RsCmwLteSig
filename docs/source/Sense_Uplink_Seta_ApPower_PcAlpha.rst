PcAlpha
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PCALpha:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PCALpha:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.PcAlpha.PcAlphaCls
	:members:
	:undoc-members:
	:noindex: