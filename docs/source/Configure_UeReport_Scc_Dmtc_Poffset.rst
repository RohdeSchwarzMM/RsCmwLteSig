Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:DMTC:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:DMTC:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Scc.Dmtc.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: