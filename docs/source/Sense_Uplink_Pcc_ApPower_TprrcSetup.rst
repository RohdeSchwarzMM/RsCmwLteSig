TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:TPRRcsetup:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:TPRRcsetup:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Pcc.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: