Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:MCSTable:SSUBframe:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:MCSTable:SSUBframe:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fcri.Downlink.McsTable.Ssubframe.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: