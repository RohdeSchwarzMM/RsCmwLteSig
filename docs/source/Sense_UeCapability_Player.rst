Player
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:UTASupported
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:USRSsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EDLFsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EDLTsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TAPPsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TWEFsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PDSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CCSSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:SPPSupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:MCPCsupport
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:NURClist
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CIHandl
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EPDCch
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:MACReporting
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:SCIHandl
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TSSubframe
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TDPChselect
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:ULComp
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:ITCWithdiff
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EHPFdd
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EFTCodebook
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TFCPcelldplx
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TRCTddpcell
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TRCFddpcell
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PFMode
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PSPSfset
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CSFSet
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:NRRT
	single: SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:DSDCell

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:UTASupported
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:USRSsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EDLFsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EDLTsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TAPPsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TWEFsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PDSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CCSSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:SPPSupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:MCPCsupport
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:NURClist
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CIHandl
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EPDCch
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:MACReporting
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:SCIHandl
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TSSubframe
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TDPChselect
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:ULComp
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:ITCWithdiff
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EHPFdd
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:EFTCodebook
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TFCPcelldplx
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TRCTddpcell
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:TRCFddpcell
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PFMode
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:PSPSfset
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:CSFSet
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:NRRT
	SENSe:LTE:SIGNaling<instance>:UECapability:PLAYer:DSDCell



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Player.PlayerCls
	:members:
	:undoc-members:
	:noindex: