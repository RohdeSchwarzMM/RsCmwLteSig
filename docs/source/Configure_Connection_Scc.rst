Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.connection.scc.repcap_secondaryCompCarrier_get()
	driver.configure.connection.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_AsEmission.rst
	Configure_Connection_Scc_Beamforming.rst
	Configure_Connection_Scc_Cexecute.rst
	Configure_Connection_Scc_DciFormat.rst
	Configure_Connection_Scc_DlEqual.rst
	Configure_Connection_Scc_Fcpri.rst
	Configure_Connection_Scc_Fcri.rst
	Configure_Connection_Scc_FcttiBased.rst
	Configure_Connection_Scc_Fpmi.rst
	Configure_Connection_Scc_Fpri.rst
	Configure_Connection_Scc_Fwbcqi.rst
	Configure_Connection_Scc_Hpusch.rst
	Configure_Connection_Scc_Laa.rst
	Configure_Connection_Scc_Mcluster.rst
	Configure_Connection_Scc_NenbAntennas.rst
	Configure_Connection_Scc_NoLayers.rst
	Configure_Connection_Scc_Pdcch.rst
	Configure_Connection_Scc_Pmatrix.rst
	Configure_Connection_Scc_Pzero.rst
	Configure_Connection_Scc_Qam.rst
	Configure_Connection_Scc_Rmc.rst
	Configure_Connection_Scc_SchModel.rst
	Configure_Connection_Scc_Sexecute.rst
	Configure_Connection_Scc_Stype.rst
	Configure_Connection_Scc_Tia.rst
	Configure_Connection_Scc_Tm.rst
	Configure_Connection_Scc_Transmission.rst
	Configure_Connection_Scc_UdChannels.rst
	Configure_Connection_Scc_UdttiBased.rst