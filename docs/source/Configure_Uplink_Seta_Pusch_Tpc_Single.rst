Single
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:PUSCh:TPC:SINGle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:PUSCh:TPC:SINGle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.Pusch.Tpc.Single.SingleCls
	:members:
	:undoc-members:
	:noindex: