Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:UL:MAXimum

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UDEFined:CHANnel:UL:MAXimum



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.UserDefined.Channel.Uplink.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: