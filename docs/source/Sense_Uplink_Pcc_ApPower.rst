ApPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PATHloss
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:EPPPower
	single: SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:EOPower

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:PATHloss
	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:EPPPower
	SENSe:LTE:SIGNaling<instance>:UL[:PCC]:APPower:EOPower



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Pcc.ApPower.ApPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.pcc.apPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Pcc_ApPower_PcAlpha.rst
	Sense_Uplink_Pcc_ApPower_PirPower.rst
	Sense_Uplink_Pcc_ApPower_Pnpusch.rst
	Sense_Uplink_Pcc_ApPower_RsPower.rst
	Sense_Uplink_Pcc_ApPower_TprrcSetup.rst