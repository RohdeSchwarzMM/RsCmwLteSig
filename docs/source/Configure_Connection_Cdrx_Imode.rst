Imode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:CLENgth
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:PTWindow
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:CLENgth
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:PTWindow
	CONFigure:LTE:SIGNaling<instance>:CONNection:CDRX:IMODe:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Cdrx.Imode.ImodeCls
	:members:
	:undoc-members:
	:noindex: