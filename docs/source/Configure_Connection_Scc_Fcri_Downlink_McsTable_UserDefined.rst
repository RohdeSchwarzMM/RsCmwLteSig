UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:MCSTable:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FCRI:DL:MCSTable:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcri.Downlink.McsTable.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: