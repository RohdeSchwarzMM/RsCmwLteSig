Sum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:SUM

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:SUM



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Power.Sum.SumCls
	:members:
	:undoc-members:
	:noindex: