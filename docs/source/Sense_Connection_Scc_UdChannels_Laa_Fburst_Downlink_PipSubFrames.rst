PipSubFrames
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.Laa.Fburst.Downlink.PipSubFrames.PipSubFramesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.udChannels.laa.fburst.downlink.pipSubFrames.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_UdChannels_Laa_Fburst_Downlink_PipSubFrames_Crate.rst