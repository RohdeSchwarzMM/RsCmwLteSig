Mcqi
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.Throughput.Scc.Mcqi.McqiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.throughput.scc.mcqi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Trace_Throughput_Scc_Mcqi_Stream.rst