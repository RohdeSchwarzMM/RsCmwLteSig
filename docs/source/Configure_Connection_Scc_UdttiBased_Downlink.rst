Downlink<Stream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: S1 .. S2
	rc = driver.configure.connection.scc.udttiBased.downlink.repcap_stream_get()
	driver.configure.connection.scc.udttiBased.downlink.repcap_stream_set(repcap.Stream.S1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDTTibased:DL<Stream>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:UDTTibased:DL<Stream>



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdttiBased.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udttiBased.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdttiBased_Downlink_All.rst