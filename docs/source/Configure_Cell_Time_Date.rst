Date
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TIME:DATE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TIME:DATE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Time.Date.DateCls
	:members:
	:undoc-members:
	:noindex: