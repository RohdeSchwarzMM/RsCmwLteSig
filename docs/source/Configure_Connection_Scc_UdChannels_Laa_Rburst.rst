Rburst
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.Laa.Rburst.RburstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udChannels.laa.rburst.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdChannels_Laa_Rburst_FullSubFrames.rst
	Configure_Connection_Scc_UdChannels_Laa_Rburst_PepSubFrames.rst
	Configure_Connection_Scc_UdChannels_Laa_Rburst_PipSubFrames.rst