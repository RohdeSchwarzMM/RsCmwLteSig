Pswitched
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:LTE:SIGNaling<instance>:PSWitched:ACTion

.. code-block:: python

	CALL:LTE:SIGNaling<instance>:PSWitched:ACTion



.. autoclass:: RsCmwLteSig.Implementations.Call.Pswitched.PswitchedCls
	:members:
	:undoc-members:
	:noindex: