TdBearer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:TDBearer:RLCMode

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:TDBearer:RLCMode



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.TdBearer.TdBearerCls
	:members:
	:undoc-members:
	:noindex: