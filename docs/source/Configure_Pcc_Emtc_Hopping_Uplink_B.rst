B
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:B:INTerval

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:HOPPing:UL:B:INTerval



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Hopping.Uplink.B.BCls
	:members:
	:undoc-members:
	:noindex: