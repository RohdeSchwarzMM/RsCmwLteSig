Mbms
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MBMS:NSCell
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MBMS:SCELl

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MBMS:NSCell
	SENSe:LTE:SIGNaling<instance>:UECapability:MBMS:SCELl



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Mbms.MbmsCls
	:members:
	:undoc-members:
	:noindex: