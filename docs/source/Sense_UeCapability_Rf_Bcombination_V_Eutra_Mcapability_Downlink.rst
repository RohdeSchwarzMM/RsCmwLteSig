Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:MCAPability:DL

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:MCAPability:DL



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.Mcapability.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: