RsPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:RSPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:RSPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.RsPower.RsPowerCls
	:members:
	:undoc-members:
	:noindex: