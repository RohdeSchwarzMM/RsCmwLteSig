SctStamp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce

.. code-block:: python

	CONFigure:LTE:SIGNaling<Instance>:SMS:OUTGoing:SCTStamp:TSOurce



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Outgoing.SctStamp.SctStampCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sms.outgoing.sctStamp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sms_Outgoing_SctStamp_Date.rst
	Configure_Sms_Outgoing_SctStamp_Time.rst