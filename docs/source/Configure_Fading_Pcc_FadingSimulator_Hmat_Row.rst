Row<HMatrixRow>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Row1 .. Row8
	rc = driver.configure.fading.pcc.fadingSimulator.hmat.row.repcap_hMatrixRow_get()
	driver.configure.fading.pcc.fadingSimulator.hmat.row.repcap_hMatrixRow_set(repcap.HMatrixRow.Row1)





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Hmat.Row.RowCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.pcc.fadingSimulator.hmat.row.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Pcc_FadingSimulator_Hmat_Row_Col.rst