Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:SSS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:SSS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Sss.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: