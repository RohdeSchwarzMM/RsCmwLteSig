RfSettings
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_All.rst
	Configure_RfSettings_Edc.rst
	Configure_RfSettings_Pcc.rst
	Configure_RfSettings_Scc.rst