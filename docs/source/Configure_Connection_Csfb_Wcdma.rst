Wcdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:WCDMa

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:CSFB:WCDMa



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Csfb.Wcdma.WcdmaCls
	:members:
	:undoc-members:
	:noindex: