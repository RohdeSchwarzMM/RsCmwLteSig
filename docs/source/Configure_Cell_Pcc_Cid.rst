Cid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:CID:EUTRan

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:CID:EUTRan



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Pcc.Cid.CidCls
	:members:
	:undoc-members:
	:noindex: