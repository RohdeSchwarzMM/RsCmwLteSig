UeCategory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:UECategory

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:UECategory



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.UeCategory.UeCategoryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.ueCategory.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_UeCategory_Downlink.rst
	Sense_UeCapability_UeCategory_Uplink.rst