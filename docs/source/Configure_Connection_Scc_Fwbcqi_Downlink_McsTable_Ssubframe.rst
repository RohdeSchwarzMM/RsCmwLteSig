Ssubframe
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Downlink.McsTable.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.fwbcqi.downlink.mcsTable.ssubframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Fwbcqi_Downlink_McsTable_Ssubframe_UserDefined.rst