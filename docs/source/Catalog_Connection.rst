Connection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CATalog:LTE:SIGNaling<instance>:CONNection:DEFBearer
	single: CATalog:LTE:SIGNaling<instance>:CONNection:DEDBearer

.. code-block:: python

	CATalog:LTE:SIGNaling<instance>:CONNection:DEFBearer
	CATalog:LTE:SIGNaling<instance>:CONNection:DEDBearer



.. autoclass:: RsCmwLteSig.Implementations.Catalog.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex: