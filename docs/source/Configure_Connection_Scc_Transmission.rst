Transmission
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TRANsmission

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TRANsmission



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Transmission.TransmissionCls
	:members:
	:undoc-members:
	:noindex: