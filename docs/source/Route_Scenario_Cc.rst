Cc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CC:FLEXible

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CC:FLEXible



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cc.CcCls
	:members:
	:undoc-members:
	:noindex: