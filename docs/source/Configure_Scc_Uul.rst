Uul
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:UUL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SCC<Carrier>:UUL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Uul.UulCls
	:members:
	:undoc-members:
	:noindex: