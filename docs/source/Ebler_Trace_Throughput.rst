Throughput
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.Throughput.ThroughputCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.throughput.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Trace_Throughput_All.rst
	Ebler_Trace_Throughput_Pcc.rst
	Ebler_Trace_Throughput_Scc.rst