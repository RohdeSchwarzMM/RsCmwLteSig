Stype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:STYPe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:STYPe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Stype.StypeCls
	:members:
	:undoc-members:
	:noindex: