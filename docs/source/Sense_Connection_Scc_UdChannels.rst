UdChannels
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.UdChannels.UdChannelsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.scc.udChannels.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Scc_UdChannels_Downlink.rst
	Sense_Connection_Scc_UdChannels_Laa.rst
	Sense_Connection_Scc_UdChannels_Uplink.rst