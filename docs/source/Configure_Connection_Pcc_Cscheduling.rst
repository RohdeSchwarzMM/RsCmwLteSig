Cscheduling
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:CSCHeduling:SFPattern

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:CSCHeduling:SFPattern



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Cscheduling.CschedulingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.cscheduling.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Cscheduling_A.rst
	Configure_Connection_Pcc_Cscheduling_B.rst