Configure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:ETOE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:ETOE



.. autoclass:: RsCmwLteSig.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_A.rst
	Configure_B.rst
	Configure_Caggregation.rst
	Configure_Cbs.rst
	Configure_Cell.rst
	Configure_Connection.rst
	Configure_CqiReporting.rst
	Configure_Downlink.rst
	Configure_Ebler.rst
	Configure_EeLog.rst
	Configure_Fading.rst
	Configure_IqIn.rst
	Configure_Mmonitor.rst
	Configure_Ncell.rst
	Configure_Pcc.rst
	Configure_RfSettings.rst
	Configure_Scc.rst
	Configure_Sib.rst
	Configure_Sms.rst
	Configure_Throughput.rst
	Configure_UeCapability.rst
	Configure_UeReport.rst
	Configure_Uplink.rst