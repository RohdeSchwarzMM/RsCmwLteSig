Ccms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CCMS<Carrier>:FLEXible

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CCMS<Carrier>:FLEXible



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ccms.CcmsCls
	:members:
	:undoc-members:
	:noindex: