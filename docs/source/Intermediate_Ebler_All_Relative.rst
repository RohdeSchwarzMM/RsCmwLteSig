Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:ALL:RELative

.. code-block:: python

	FETCh:INTermediate:LTE:SIGNaling<instance>:EBLer:ALL:RELative



.. autoclass:: RsCmwLteSig.Implementations.Intermediate.Ebler.All.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: