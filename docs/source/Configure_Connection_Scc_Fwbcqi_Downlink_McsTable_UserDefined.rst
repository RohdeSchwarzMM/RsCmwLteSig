UserDefined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Downlink.McsTable.UserDefined.UserDefinedCls
	:members:
	:undoc-members:
	:noindex: