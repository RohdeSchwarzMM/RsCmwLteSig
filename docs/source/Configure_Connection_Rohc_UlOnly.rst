UlOnly
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ULONly:PROFiles
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ULONly:ENABle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ULONly:PROFiles
	CONFigure:LTE:SIGNaling<instance>:CONNection:ROHC:ULONly:ENABle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Rohc.UlOnly.UlOnlyCls
	:members:
	:undoc-members:
	:noindex: