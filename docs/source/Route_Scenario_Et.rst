Et
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:ET[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:ET[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Et.EtCls
	:members:
	:undoc-members:
	:noindex: