Pdcch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PDCCh:SYMBol
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PDCCh:ALEVel

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PDCCh:SYMBol
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:PDCCh:ALEVel



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Pdcch.PdcchCls
	:members:
	:undoc-members:
	:noindex: