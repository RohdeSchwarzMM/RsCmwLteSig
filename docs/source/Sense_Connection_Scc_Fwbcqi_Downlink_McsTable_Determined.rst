Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:DL:MCSTable:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Fwbcqi.Downlink.McsTable.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: