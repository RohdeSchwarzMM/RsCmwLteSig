State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:THRoughput:STATe

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:THRoughput:STATe



.. autoclass:: RsCmwLteSig.Implementations.Throughput.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_State_All.rst