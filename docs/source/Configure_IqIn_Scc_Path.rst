Path<Path>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Path1 .. Path2
	rc = driver.configure.iqIn.scc.path.repcap_path_get()
	driver.configure.iqIn.scc.path.repcap_path_set(repcap.Path.Path1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:IQIN:SCC<Carrier>:PATH<n>

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:IQIN:SCC<Carrier>:PATH<n>



.. autoclass:: RsCmwLteSig.Implementations.Configure.IqIn.Scc.Path.PathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.iqIn.scc.path.clone()