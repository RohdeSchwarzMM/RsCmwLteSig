Mtext
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CLEan:LTE:SIGNaling<instance>:SMS:INComing:INFO:MTEXt

.. code-block:: python

	CLEan:LTE:SIGNaling<instance>:SMS:INComing:INFO:MTEXt



.. autoclass:: RsCmwLteSig.Implementations.Clean.Sms.Incoming.Info.Mtext.MtextCls
	:members:
	:undoc-members:
	:noindex: