Iloss
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:MODE
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:LOSS

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:MODE
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:LOSS



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.FadingSimulator.Iloss.IlossCls
	:members:
	:undoc-members:
	:noindex: