Downlink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Cscheduling.A.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.cscheduling.a.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Cscheduling_A_Downlink_All.rst
	Configure_Connection_Pcc_Cscheduling_A_Downlink_Anb.rst