Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PNPusch:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:PNPusch:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: