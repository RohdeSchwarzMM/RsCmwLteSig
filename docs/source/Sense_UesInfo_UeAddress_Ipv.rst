Ipv<IPversion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: IPv4 .. IPv6
	rc = driver.sense.uesInfo.ueAddress.ipv.repcap_iPversion_get()
	driver.sense.uesInfo.ueAddress.ipv.repcap_iPversion_set(repcap.IPversion.IPv4)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:IPV<n>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UESinfo:UEADdress:IPV<n>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UesInfo.UeAddress.Ipv.IpvCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.ueAddress.ipv.clone()