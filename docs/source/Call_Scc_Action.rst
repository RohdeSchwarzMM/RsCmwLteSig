Action
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:LTE:SIGNaling<instance>:SCC<Carrier>:ACTion

.. code-block:: python

	CALL:LTE:SIGNaling<instance>:SCC<Carrier>:ACTion



.. autoclass:: RsCmwLteSig.Implementations.Call.Scc.Action.ActionCls
	:members:
	:undoc-members:
	:noindex: