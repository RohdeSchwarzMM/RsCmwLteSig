UdChannels
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.UdChannels.UdChannelsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.udChannels.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_UdChannels_Downlink.rst
	Configure_Connection_Scc_UdChannels_Laa.rst
	Configure_Connection_Scc_UdChannels_Mcluster.rst
	Configure_Connection_Scc_UdChannels_Uplink.rst