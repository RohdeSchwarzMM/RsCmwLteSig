Rf
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:MTADvance
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:SUPPorted
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:HDUPlex
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:DL<qam>
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:FBRetrieval
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:RBANds
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:FBPadjust
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:MMPRbehavior
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:SRTX
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:SNCap

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:MTADvance
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:SUPPorted
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:HDUPlex
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:DL<qam>
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:FBRetrieval
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:RBANds
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:FBPadjust
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:MMPRbehavior
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:SRTX
	SENSe:LTE:SIGNaling<instance>:UECapability:RF:SNCap



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.RfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.rf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Rf_Bcombination.rst
	Sense_UeCapability_Rf_DcSupport.rst
	Sense_UeCapability_Rf_Uplink.rst