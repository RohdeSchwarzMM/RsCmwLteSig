Mac
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MAC:LDRXcommand
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MAC:LCSPtimer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MAC:LDRXcommand
	SENSe:LTE:SIGNaling<instance>:UECapability:MAC:LCSPtimer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Mac.MacCls
	:members:
	:undoc-members:
	:noindex: