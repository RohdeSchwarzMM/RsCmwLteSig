PsfConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:PSFConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:PSFConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.PsfConfig.PsfConfigCls
	:members:
	:undoc-members:
	:noindex: