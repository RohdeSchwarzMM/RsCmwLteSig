Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:SUBFrame:ABSolute

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:SUBFrame:ABSolute



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Subframe.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: