Chf
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Chf.ChfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.chf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Chf_Flexible.rst