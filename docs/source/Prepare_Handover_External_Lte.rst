Lte
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:LTE

.. code-block:: python

	PREPare:LTE:SIGNaling<instance>:HANDover:EXTernal:LTE



.. autoclass:: RsCmwLteSig.Implementations.Prepare.Handover.External.Lte.LteCls
	:members:
	:undoc-members:
	:noindex: