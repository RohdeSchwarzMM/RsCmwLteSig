NenbAntennas
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:NENBantennas

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:NENBantennas



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.NenbAntennas.NenbAntennasCls
	:members:
	:undoc-members:
	:noindex: