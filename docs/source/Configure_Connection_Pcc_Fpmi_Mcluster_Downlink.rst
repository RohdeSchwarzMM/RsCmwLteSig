Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FPMI:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fpmi.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: