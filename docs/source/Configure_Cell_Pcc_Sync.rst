Sync
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SYNC:ZONE
	single: CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SYNC:OFFSet

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SYNC:ZONE
	CONFigure:LTE:SIGNaling<instance>:CELL[:PCC]:SYNC:OFFSet



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Pcc.Sync.SyncCls
	:members:
	:undoc-members:
	:noindex: