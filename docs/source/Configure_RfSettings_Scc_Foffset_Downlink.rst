Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:FOFFset:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:FOFFset:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Foffset.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: