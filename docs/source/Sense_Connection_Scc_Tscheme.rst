Tscheme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TSCHeme

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:TSCHeme



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Scc.Tscheme.TschemeCls
	:members:
	:undoc-members:
	:noindex: