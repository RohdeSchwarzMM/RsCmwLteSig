Noise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:BWIDth:NOISe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:AWGN:BWIDth:NOISe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Awgn.Bandwidth.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex: