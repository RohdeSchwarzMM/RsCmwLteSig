Rresult
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RRESult

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RRESult



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.Rresult.RresultCls
	:members:
	:undoc-members:
	:noindex: