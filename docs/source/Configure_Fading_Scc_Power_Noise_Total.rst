Total
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:NOISe:TOTal

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:POWer:NOISe:TOTal



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.Power.Noise.Total.TotalCls
	:members:
	:undoc-members:
	:noindex: