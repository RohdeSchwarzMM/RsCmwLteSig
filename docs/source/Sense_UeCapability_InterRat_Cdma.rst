Cdma
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:CDMA<2000>:NWSHaring

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:IRAT:CDMA<2000>:NWSHaring



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.InterRat.Cdma.CdmaCls
	:members:
	:undoc-members:
	:noindex: