V
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IFNGaps:V<number>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IFNGaps:V<number>



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.InterFreqNgaps.V.VCls
	:members:
	:undoc-members:
	:noindex: