Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:QAM<ModOrder>:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:QAM<ModOrder>:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Qam.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: