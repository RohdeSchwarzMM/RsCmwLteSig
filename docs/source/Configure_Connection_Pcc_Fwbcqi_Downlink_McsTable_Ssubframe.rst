Ssubframe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:SSUBframe:UDEFined

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FWBCqi:DL:MCSTable:SSUBframe:UDEFined



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fwbcqi.Downlink.McsTable.Ssubframe.SsubframeCls
	:members:
	:undoc-members:
	:noindex: