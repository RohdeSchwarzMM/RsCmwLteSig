Sib
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:SIB<n>:TTIMing

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:SIB<n>:TTIMing



.. autoclass:: RsCmwLteSig.Implementations.Sense.Sib.SibCls
	:members:
	:undoc-members:
	:noindex: