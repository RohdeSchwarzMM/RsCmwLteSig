Pdu
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Uplink.Pdu.PduCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.throughput.trace.uplink.pdu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Throughput_Trace_Uplink_Pdu_Average.rst
	Throughput_Trace_Uplink_Pdu_Current.rst