Ri
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RI

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:RI



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Ri.RiCls
	:members:
	:undoc-members:
	:noindex: