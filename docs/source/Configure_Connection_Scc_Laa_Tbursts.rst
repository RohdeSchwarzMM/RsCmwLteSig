Tbursts
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:TBURsts

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:TBURsts



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Tbursts.TburstsCls
	:members:
	:undoc-members:
	:noindex: