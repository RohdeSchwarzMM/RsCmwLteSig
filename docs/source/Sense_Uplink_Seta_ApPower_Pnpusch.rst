Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PNPusch:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETA:APPower:PNPusch:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Seta.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: