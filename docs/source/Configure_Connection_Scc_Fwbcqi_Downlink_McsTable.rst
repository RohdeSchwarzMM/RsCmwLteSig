McsTable
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Downlink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.fwbcqi.downlink.mcsTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Fwbcqi_Downlink_McsTable_Csirs.rst
	Configure_Connection_Scc_Fwbcqi_Downlink_McsTable_Ssubframe.rst
	Configure_Connection_Scc_Fwbcqi_Downlink_McsTable_UserDefined.rst