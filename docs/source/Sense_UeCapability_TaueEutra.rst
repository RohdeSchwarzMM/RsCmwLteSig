TaueEutra
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.TaueEutra.TaueEutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.taueEutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_TaueEutra_FgIndicators.rst
	Sense_UeCapability_TaueEutra_InterRat.rst
	Sense_UeCapability_TaueEutra_Ncsacq.rst
	Sense_UeCapability_TaueEutra_Player.rst