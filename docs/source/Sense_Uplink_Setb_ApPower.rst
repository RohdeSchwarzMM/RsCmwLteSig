ApPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PATHloss
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:EPPPower
	single: SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:EOPower

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:PATHloss
	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:EPPPower
	SENSe:LTE:SIGNaling<instance>:UL:SETB:APPower:EOPower



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setb.ApPower.ApPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uplink.setb.apPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Uplink_Setb_ApPower_PcAlpha.rst
	Sense_Uplink_Setb_ApPower_PirPower.rst
	Sense_Uplink_Setb_ApPower_Pnpusch.rst
	Sense_Uplink_Setb_ApPower_RsPower.rst
	Sense_Uplink_Setb_ApPower_TprrcSetup.rst