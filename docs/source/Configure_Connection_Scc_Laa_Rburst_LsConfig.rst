LsConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:LSConfig

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:LAA:RBURst:LSConfig



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.LsConfig.LsConfigCls
	:members:
	:undoc-members:
	:noindex: