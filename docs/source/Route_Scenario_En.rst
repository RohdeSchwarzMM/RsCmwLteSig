En
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:EN[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:EN[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.En.EnCls
	:members:
	:undoc-members:
	:noindex: