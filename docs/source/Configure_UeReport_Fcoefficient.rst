Fcoefficient
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:FCOefficient:RSRP
	single: CONFigure:LTE:SIGNaling<instance>:UEReport:FCOefficient:RSRQ

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UEReport:FCOefficient:RSRP
	CONFigure:LTE:SIGNaling<instance>:UEReport:FCOefficient:RSRQ



.. autoclass:: RsCmwLteSig.Implementations.Configure.UeReport.Fcoefficient.FcoefficientCls
	:members:
	:undoc-members:
	:noindex: