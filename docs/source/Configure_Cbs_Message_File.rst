File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:FILE:INFO
	single: CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:FILE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:FILE:INFO
	CONFigure:LTE:SIGNaling<instance>:CBS:MESSage:FILE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cbs.Message.File.FileCls
	:members:
	:undoc-members:
	:noindex: