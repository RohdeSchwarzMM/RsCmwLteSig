Umargin
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UMARgin

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:RFSettings:SCC<Carrier>:UMARgin



.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Scc.Umargin.UmarginCls
	:members:
	:undoc-members:
	:noindex: