Mapping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:PZERo:MAPPing

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:PZERo:MAPPing



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Pzero.Mapping.MappingCls
	:members:
	:undoc-members:
	:noindex: