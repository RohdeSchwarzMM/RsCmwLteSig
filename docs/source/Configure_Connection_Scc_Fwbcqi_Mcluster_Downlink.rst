Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FWBCqi:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fwbcqi.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: