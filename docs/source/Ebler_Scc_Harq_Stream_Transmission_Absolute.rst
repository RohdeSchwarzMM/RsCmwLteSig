Absolute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:TRANsmission:ABSolute

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:HARQ:STReam<Stream>:TRANsmission:ABSolute



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Harq.Stream.Transmission.Absolute.AbsoluteCls
	:members:
	:undoc-members:
	:noindex: