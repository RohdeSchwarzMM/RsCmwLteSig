Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PSS:POFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PSS:POFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pss.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: