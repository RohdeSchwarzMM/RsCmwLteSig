Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:FREQuency:INTRa
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:FREQuency:INTer

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:FREQuency:INTRa
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:NCSacq:FREQuency:INTer



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.Ncsacq.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: