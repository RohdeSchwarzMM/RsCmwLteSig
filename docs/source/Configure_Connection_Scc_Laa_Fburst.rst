Fburst
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Fburst.FburstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.laa.fburst.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Laa_Fburst_Blength.rst
	Configure_Connection_Scc_Laa_Fburst_OslSubframe.rst
	Configure_Connection_Scc_Laa_Fburst_Pbtr.rst
	Configure_Connection_Scc_Laa_Fburst_SpfSubframe.rst