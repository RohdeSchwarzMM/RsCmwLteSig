Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSRP:RANGe

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport:SCC<Carrier>:RSRP:RANGe



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Scc.Rsrp.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: