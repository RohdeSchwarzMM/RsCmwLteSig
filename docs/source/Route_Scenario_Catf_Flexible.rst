Flexible
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CATF:FLEXible[:EXTernal]
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CATF:FLEXible:INTernal

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CATF:FLEXible[:EXTernal]
	ROUTe:LTE:SIGNaling<instance>:SCENario:CATF:FLEXible:INTernal



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Catf.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: