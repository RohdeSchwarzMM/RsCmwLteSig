McsTable
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fcri.Downlink.McsTable.McsTableCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.fcri.downlink.mcsTable.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Fcri_Downlink_McsTable_Ssubframe.rst
	Configure_Connection_Scc_Fcri_Downlink_McsTable_UserDefined.rst