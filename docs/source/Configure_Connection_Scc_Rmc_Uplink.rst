Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:RMC:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:RMC:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Rmc.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: