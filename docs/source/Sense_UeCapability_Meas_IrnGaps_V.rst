V
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.V.VCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.meas.irnGaps.v.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_Meas_IrnGaps_V_Chrpd.rst
	Sense_UeCapability_Meas_IrnGaps_V_Cxrtt.rst
	Sense_UeCapability_Meas_IrnGaps_V_Geran.rst
	Sense_UeCapability_Meas_IrnGaps_V_Ufdd.rst
	Sense_UeCapability_Meas_IrnGaps_V_Utdd.rst