Rburst
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Laa.Rburst.RburstCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.scc.laa.rburst.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Scc_Laa_Rburst_Blength.rst
	Configure_Connection_Scc_Laa_Rburst_IpSubframe.rst
	Configure_Connection_Scc_Laa_Rburst_LsConfig.rst
	Configure_Connection_Scc_Laa_Rburst_PsfConfig.rst
	Configure_Connection_Scc_Laa_Rburst_Tprobability.rst