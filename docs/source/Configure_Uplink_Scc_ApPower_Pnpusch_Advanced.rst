Advanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PNPusch:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:APPower:PNPusch:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.ApPower.Pnpusch.Advanced.AdvancedCls
	:members:
	:undoc-members:
	:noindex: