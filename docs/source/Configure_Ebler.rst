Ebler
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:TOUT
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:SFRames
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:ERCalc
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:REPetition
	single: CONFigure:LTE:SIGNaling<instance>:EBLer:SCONdition

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:EBLer:TOUT
	CONFigure:LTE:SIGNaling<instance>:EBLer:SFRames
	CONFigure:LTE:SIGNaling<instance>:EBLer:ERCalc
	CONFigure:LTE:SIGNaling<instance>:EBLer:REPetition
	CONFigure:LTE:SIGNaling<instance>:EBLer:SCONdition



.. autoclass:: RsCmwLteSig.Implementations.Configure.Ebler.EblerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ebler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Ebler_Confidence.rst