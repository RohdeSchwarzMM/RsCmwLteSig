File
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:FILE:INFO
	single: CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:FILE

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:FILE:INFO
	CONFigure:LTE:SIGNaling<instance>:SMS:OUTGoing:FILE



.. autoclass:: RsCmwLteSig.Implementations.Configure.Sms.Outgoing.File.FileCls
	:members:
	:undoc-members:
	:noindex: