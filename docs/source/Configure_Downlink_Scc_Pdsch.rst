Pdsch
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pdsch.PdschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.downlink.scc.pdsch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Downlink_Scc_Pdsch_Pa.rst
	Configure_Downlink_Scc_Pdsch_Rindex.rst