Grsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:GRSM<MIMO4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:GRSM<MIMO4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Grsm.GrsmCls
	:members:
	:undoc-members:
	:noindex: