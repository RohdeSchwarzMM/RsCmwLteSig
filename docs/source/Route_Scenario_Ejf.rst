Ejf
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ejf.EjfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.ejf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Ejf_Flexible.rst