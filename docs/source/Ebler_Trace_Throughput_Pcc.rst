Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput[:PCC]

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput[:PCC]



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.Throughput.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.throughput.pcc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Trace_Throughput_Pcc_Mcqi.rst
	Ebler_Trace_Throughput_Pcc_Stream.rst