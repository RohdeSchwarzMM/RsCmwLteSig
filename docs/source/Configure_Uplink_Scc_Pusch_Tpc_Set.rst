Set
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:SET

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PUSCh:TPC:SET



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pusch.Tpc.Set.SetCls
	:members:
	:undoc-members:
	:noindex: