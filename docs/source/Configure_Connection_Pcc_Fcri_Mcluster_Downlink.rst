Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:MCLuster:DL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:FCRI:MCLuster:DL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Fcri.Mcluster.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: