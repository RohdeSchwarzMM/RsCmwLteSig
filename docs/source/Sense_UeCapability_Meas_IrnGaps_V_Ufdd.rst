Ufdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:V<number>:UFDD

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:MEAS:IRNGaps:V<number>:UFDD



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Meas.IrnGaps.V.Ufdd.UfddCls
	:members:
	:undoc-members:
	:noindex: