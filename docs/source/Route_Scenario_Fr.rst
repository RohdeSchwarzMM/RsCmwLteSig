Fr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:FR[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:FR[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Fr.FrCls
	:members:
	:undoc-members:
	:noindex: