Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:CSIRs:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:CSIRs:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcpri.Downlink.McsTable.Csirs.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: