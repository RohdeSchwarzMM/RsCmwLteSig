Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDTTibased:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDTTibased:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdttiBased.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.udttiBased.uplink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_UdttiBased_Uplink_All.rst