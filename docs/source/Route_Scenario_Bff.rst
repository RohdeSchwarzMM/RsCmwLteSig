Bff
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Bff.BffCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.bff.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Bff_Flexible.rst