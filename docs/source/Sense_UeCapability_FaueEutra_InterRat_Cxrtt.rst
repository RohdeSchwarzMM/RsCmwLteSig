Cxrtt
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECSFb
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECCMob
	single: SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECDual

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECSFb
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECCMob
	SENSe:LTE:SIGNaling<instance>:UECapability:FAUeeutra:IRAT:CXRTt:ECDual



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.FaueEutra.InterRat.Cxrtt.CxrttCls
	:members:
	:undoc-members:
	:noindex: