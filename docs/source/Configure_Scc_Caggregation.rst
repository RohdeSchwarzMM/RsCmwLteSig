Caggregation
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Scc.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.scc.caggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Scc_Caggregation_Mode.rst