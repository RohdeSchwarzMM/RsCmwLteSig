Scc<SecondaryCompCarrier>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.ebler.trace.throughput.scc.repcap_secondaryCompCarrier_get()
	driver.ebler.trace.throughput.scc.repcap_secondaryCompCarrier_set(repcap.SecondaryCompCarrier.CC1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput:SCC<Carrier>

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:TRACe:THRoughput:SCC<Carrier>



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Trace.Throughput.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.ebler.trace.throughput.scc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Ebler_Trace_Throughput_Scc_Mcqi.rst
	Ebler_Trace_Throughput_Scc_Stream.rst