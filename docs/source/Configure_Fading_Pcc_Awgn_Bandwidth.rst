Bandwidth
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:BWIDth:RATio
	single: CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:BWIDth:NOISe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:BWIDth:RATio
	CONFigure:LTE:SIGNaling<instance>:FADing[:PCC]:AWGN:BWIDth:NOISe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Pcc.Awgn.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: