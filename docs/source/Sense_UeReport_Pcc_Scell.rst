Scell
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:SCELl:RANGe
	single: SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:SCELl

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:SCELl:RANGe
	SENSe:LTE:SIGNaling<instance>:UEReport[:PCC]:SCELl



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeReport.Pcc.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex: