Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:CURRent
	single: READ:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:CURRent

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:CURRent
	READ:LTE:SIGNaling<instance>:THRoughput:TRACe:UL:PDU:CURRent



.. autoclass:: RsCmwLteSig.Implementations.Throughput.Trace.Uplink.Pdu.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: