Pmac
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:PMAC

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SCMuting:PMAC



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.ScMuting.Pmac.PmacCls
	:members:
	:undoc-members:
	:noindex: