Ensm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:ENSM<MIMO4x4>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:ENSM<MIMO4x4>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Ensm.EnsmCls
	:members:
	:undoc-members:
	:noindex: