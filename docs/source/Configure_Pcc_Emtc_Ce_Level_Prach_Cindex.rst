Cindex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:CINDex

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:CINDex



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.Level.Prach.Cindex.CindexCls
	:members:
	:undoc-members:
	:noindex: