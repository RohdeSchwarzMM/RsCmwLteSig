Iloss
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Iloss.IlossCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.scc.fadingSimulator.iloss.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Scc_FadingSimulator_Iloss_Loss.rst
	Configure_Fading_Scc_FadingSimulator_Iloss_Mode.rst