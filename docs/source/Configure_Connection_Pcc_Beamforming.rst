Beamforming
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:MODE
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:NOLayers
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:MATRix

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:MODE
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:NOLayers
	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:BEAMforming:MATRix



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Beamforming.BeamformingCls
	:members:
	:undoc-members:
	:noindex: