Cjsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:SIGNaling<instance>:SCENario:CJSM<MIMO44>[:FLEXible]

.. code-block:: python

	ROUTe:LTE:SIGNaling<instance>:SCENario:CJSM<MIMO44>[:FLEXible]



.. autoclass:: RsCmwLteSig.Implementations.Route.Scenario.Cjsm.CjsmCls
	:members:
	:undoc-members:
	:noindex: