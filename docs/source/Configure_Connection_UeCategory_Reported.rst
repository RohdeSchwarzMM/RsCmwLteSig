Reported
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:REPorted

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:UECategory:REPorted



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.UeCategory.Reported.ReportedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.ueCategory.reported.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_UeCategory_Reported_Enhanced.rst