CqiReporting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:ENABle
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:CSIRmode
	single: CONFigure:LTE:SIGNaling<instance>:CQIReporting:SANCqi

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CQIReporting:ENABle
	CONFigure:LTE:SIGNaling<instance>:CQIReporting:CSIRmode
	CONFigure:LTE:SIGNaling<instance>:CQIReporting:SANCqi



.. autoclass:: RsCmwLteSig.Implementations.Configure.CqiReporting.CqiReportingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cqiReporting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_CqiReporting_Pcc.rst
	Configure_CqiReporting_PriReporting.rst
	Configure_CqiReporting_Scc.rst