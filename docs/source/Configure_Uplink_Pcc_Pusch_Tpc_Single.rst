Single
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:SINGle

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL[:PCC]:PUSCh:TPC:SINGle



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Pcc.Pusch.Tpc.Single.SingleCls
	:members:
	:undoc-members:
	:noindex: