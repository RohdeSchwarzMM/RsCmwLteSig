Aports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:APORts

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<carrier>:TM<nr>:CSIRs:APORts



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Tm.Csirs.Aports.AportsCls
	:members:
	:undoc-members:
	:noindex: