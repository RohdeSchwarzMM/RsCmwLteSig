Pmax
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PMAX

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SCC<Carrier>:PMAX



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Scc.Pmax.PmaxCls
	:members:
	:undoc-members:
	:noindex: