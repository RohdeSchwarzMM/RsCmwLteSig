Real
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT:ROW<row>:COL<col>:REAL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:FADing:SCC<Carrier>:FSIMulator:HMAT:ROW<row>:COL<col>:REAL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Fading.Scc.FadingSimulator.Hmat.Row.Col.Real.RealCls
	:members:
	:undoc-members:
	:noindex: