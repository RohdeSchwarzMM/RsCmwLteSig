RsPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:RSPower:BASic

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UL:SETC:APPower:RSPower:BASic



.. autoclass:: RsCmwLteSig.Implementations.Sense.Uplink.Setc.ApPower.RsPower.RsPowerCls
	:members:
	:undoc-members:
	:noindex: