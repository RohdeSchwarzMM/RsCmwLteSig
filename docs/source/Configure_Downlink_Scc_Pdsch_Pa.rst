Pa
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PDSCh:PA

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:DL:SCC<Carrier>:PDSCh:PA



.. autoclass:: RsCmwLteSig.Implementations.Configure.Downlink.Scc.Pdsch.Pa.PaCls
	:members:
	:undoc-members:
	:noindex: