Ce
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:MODE
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:ILEVel

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:MODE
	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:ILEVel



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.CeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pcc.emtc.ce.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pcc_Emtc_Ce_Level.rst