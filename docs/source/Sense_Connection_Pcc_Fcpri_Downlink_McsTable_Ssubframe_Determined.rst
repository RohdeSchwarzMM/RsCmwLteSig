Determined
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:SSUBframe:DETermined

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection[:PCC]:FCPRi:DL:MCSTable:SSUBframe:DETermined



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Pcc.Fcpri.Downlink.McsTable.Ssubframe.Determined.DeterminedCls
	:members:
	:undoc-members:
	:noindex: