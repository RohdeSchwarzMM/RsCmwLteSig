UesInfo
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:UEUSage
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:VDPReference
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:IMEI
	single: SENSe:LTE:SIGNaling<instance>:UESinfo:IMSI

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UESinfo:UEUSage
	SENSe:LTE:SIGNaling<instance>:UESinfo:VDPReference
	SENSe:LTE:SIGNaling<instance>:UESinfo:IMEI
	SENSe:LTE:SIGNaling<instance>:UESinfo:IMSI



.. autoclass:: RsCmwLteSig.Implementations.Sense.UesInfo.UesInfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_UeAddress.rst