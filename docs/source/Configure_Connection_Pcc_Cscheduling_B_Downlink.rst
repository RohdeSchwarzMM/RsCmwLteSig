Downlink
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Cscheduling.B.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.pcc.cscheduling.b.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Pcc_Cscheduling_B_Downlink_All.rst