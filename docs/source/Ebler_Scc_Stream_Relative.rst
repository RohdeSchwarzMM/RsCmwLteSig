Relative
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:STReam<Stream>:RELative

.. code-block:: python

	FETCh:LTE:SIGNaling<instance>:EBLer:SCC<Carrier>:STReam<Stream>:RELative



.. autoclass:: RsCmwLteSig.Implementations.Ebler.Scc.Stream.Relative.RelativeCls
	:members:
	:undoc-members:
	:noindex: