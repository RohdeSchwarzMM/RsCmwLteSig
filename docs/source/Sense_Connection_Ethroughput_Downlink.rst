Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:ALL

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:CONNection:ETHRoughput:DL:ALL



.. autoclass:: RsCmwLteSig.Implementations.Sense.Connection.Ethroughput.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.connection.ethroughput.downlink.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Connection_Ethroughput_Downlink_Pcc.rst
	Sense_Connection_Ethroughput_Downlink_Scc.rst