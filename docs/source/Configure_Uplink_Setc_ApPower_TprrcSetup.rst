TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:TPRRcsetup:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETC:APPower:TPRRcsetup:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Setc.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: