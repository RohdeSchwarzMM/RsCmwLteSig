Foffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:FOFFset

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>[:PCC]:EMTC:CE:LEVel:PRACh:FOFFset



.. autoclass:: RsCmwLteSig.Implementations.Configure.Pcc.Emtc.Ce.Level.Prach.Foffset.FoffsetCls
	:members:
	:undoc-members:
	:noindex: