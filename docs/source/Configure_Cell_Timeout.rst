Timeout
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:OSYNch

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:TOUT:OSYNch



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Timeout.TimeoutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.cell.timeout.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Cell_Timeout_T.rst
	Configure_Cell_Timeout_Text.rst