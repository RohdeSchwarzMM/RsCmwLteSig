Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: