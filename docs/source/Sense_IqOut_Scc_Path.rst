Path<Path>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Path1 .. Path2
	rc = driver.sense.iqOut.scc.path.repcap_path_get()
	driver.sense.iqOut.scc.path.repcap_path_set(repcap.Path.Path1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:IQOut:SCC<Carrier>:PATH<n>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:IQOut:SCC<Carrier>:PATH<n>



.. autoclass:: RsCmwLteSig.Implementations.Sense.IqOut.Scc.Path.PathCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.iqOut.scc.path.clone()