Throughput
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:THRoughput:TOUT
	single: CONFigure:LTE:SIGNaling<instance>:THRoughput:UPDate
	single: CONFigure:LTE:SIGNaling<instance>:THRoughput:WINDow
	single: CONFigure:LTE:SIGNaling<instance>:THRoughput:REPetition

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:THRoughput:TOUT
	CONFigure:LTE:SIGNaling<instance>:THRoughput:UPDate
	CONFigure:LTE:SIGNaling<instance>:THRoughput:WINDow
	CONFigure:LTE:SIGNaling<instance>:THRoughput:REPetition



.. autoclass:: RsCmwLteSig.Implementations.Configure.Throughput.ThroughputCls
	:members:
	:undoc-members:
	:noindex: