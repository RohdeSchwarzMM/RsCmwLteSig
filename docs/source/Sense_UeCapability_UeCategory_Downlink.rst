Downlink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:UECategory:DL:ENHanced

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:UECategory:DL:ENHanced



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.UeCategory.Downlink.DownlinkCls
	:members:
	:undoc-members:
	:noindex: