Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SYNC:OFFSet

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CELL:SCC<Carrier>:SYNC:OFFSet



.. autoclass:: RsCmwLteSig.Implementations.Configure.Cell.Scc.Sync.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: