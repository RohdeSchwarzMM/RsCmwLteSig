CeParameters
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.CeParameters.CeParametersCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.ueCapability.ceParameters.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UeCapability_CeParameters_Mode.rst