Stti
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPMI:DL:STTI

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:FPMI:DL:STTI



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Fpmi.Downlink.Stti.SttiCls
	:members:
	:undoc-members:
	:noindex: