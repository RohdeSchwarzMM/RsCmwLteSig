Uplink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:A:UL

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:UDCHannels:EMTC:A:UL



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.UdChannels.Emtc.A.Uplink.UplinkCls
	:members:
	:undoc-members:
	:noindex: