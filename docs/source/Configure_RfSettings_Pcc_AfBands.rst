AfBands
----------------------------------------





.. autoclass:: RsCmwLteSig.Implementations.Configure.RfSettings.Pcc.AfBands.AfBandsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.pcc.afBands.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Pcc_AfBands_All.rst