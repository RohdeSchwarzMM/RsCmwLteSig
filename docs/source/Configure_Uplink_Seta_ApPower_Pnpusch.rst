Pnpusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:PNPusch:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:PNPusch:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.ApPower.Pnpusch.PnpuschCls
	:members:
	:undoc-members:
	:noindex: