Csamples<ClippingCounter>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.sense.fading.pcc.fadingSimulator.iloss.csamples.repcap_clippingCounter_get()
	driver.sense.fading.pcc.fadingSimulator.iloss.csamples.repcap_clippingCounter_set(repcap.ClippingCounter.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:CSAMples<ClippingCounter>

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:FADing[:PCC]:FSIMulator:ILOSs:CSAMples<ClippingCounter>



.. autoclass:: RsCmwLteSig.Implementations.Sense.Fading.Pcc.FadingSimulator.Iloss.Csamples.CsamplesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.fading.pcc.fadingSimulator.iloss.csamples.clone()