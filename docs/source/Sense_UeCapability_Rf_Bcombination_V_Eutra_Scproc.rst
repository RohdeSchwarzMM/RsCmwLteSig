Scproc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:SCPRoc

.. code-block:: python

	SENSe:LTE:SIGNaling<instance>:UECapability:RF:BCOMbination:V<Number>:EUTRa<BandNr>:SCPRoc



.. autoclass:: RsCmwLteSig.Implementations.Sense.UeCapability.Rf.Bcombination.V.Eutra.Scproc.ScprocCls
	:members:
	:undoc-members:
	:noindex: