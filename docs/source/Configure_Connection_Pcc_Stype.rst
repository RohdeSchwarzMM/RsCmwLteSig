Stype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:STYPe

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection[:PCC]:STYPe



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Pcc.Stype.StypeCls
	:members:
	:undoc-members:
	:noindex: