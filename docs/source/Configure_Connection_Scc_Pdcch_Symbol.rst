Symbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PDCCh:SYMBol

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:CONNection:SCC<Carrier>:PDCCh:SYMBol



.. autoclass:: RsCmwLteSig.Implementations.Configure.Connection.Scc.Pdcch.Symbol.SymbolCls
	:members:
	:undoc-members:
	:noindex: