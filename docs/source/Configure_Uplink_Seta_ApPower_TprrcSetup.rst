TprrcSetup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:TPRRcsetup:ADVanced

.. code-block:: python

	CONFigure:LTE:SIGNaling<instance>:UL:SETA:APPower:TPRRcsetup:ADVanced



.. autoclass:: RsCmwLteSig.Implementations.Configure.Uplink.Seta.ApPower.TprrcSetup.TprrcSetupCls
	:members:
	:undoc-members:
	:noindex: